<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'dbteprotejo');

/** MySQL database username */
define('DB_USER', 'userteprotejo');

/** MySQL database password */
define('DB_PASSWORD', 'passteprotejo');

/** MySQL hostname */
define('DB_HOST', 'mysql');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '04/3a6a`~;BD6; T?i-KYUXgT!lb(~PT.U/gU}VHwLTdT{:g+kJFgt |<-?qrBeT');
define('SECURE_AUTH_KEY',  'thE?!5wcN(Ieh}c`ZBnV3p&0%o(:$Dcfk/^H-#<yj7&vSF$*#J72(E}p<D5c%VgP');
define('LOGGED_IN_KEY',    'k1V%lh;eLqFxKpGtw8dBw=Tk(9wZ2^W30!#)&tcfsvt`:7l.],TzM8d6NyA~lMNL');
define('NONCE_KEY',        ';[<Pc`fBW*.JO[?lZsjQ^E8::(NKv*U<K8|;MBq=!j!w2DAI a3R@W>|Q9O^=ckL');
define('AUTH_SALT',        '^V%B5?c|7B8w.r#N#%%jDN|!k*V;Hra;%;Y=>j}2vo[_6^)pw(Bs00VD5F00]2@T');
define('SECURE_AUTH_SALT', 'o.<:h8iqfv9y0{O]28Iy~br,>>x^BIAUT`8+VkEdgQFE]b(S83d jq(8RDNpErl#');
define('LOGGED_IN_SALT',   'rFZ3I>s?_JcBU.63O37Mk{bf+FpluH%[E7I(SQ<T^4M*yL6}46*?[?BS=gO/Z,_%');
define('NONCE_SALT',       '.MhIMVp!!4Ekv#u`)Lpu/}+9EXM:Czb1AZ&i2lFECH;]E6gAX _5ec!9c^eTtuC?');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'teproxx_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
