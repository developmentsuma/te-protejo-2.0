<?php

/*Template Name: Quienes Somos*/ 

get_header(); 

$count= 0;
$blog_id = get_current_blog_id();
$site_name = get_blog_details($blog_id)->blogname;


?>

<style>
    
    .img-limits{
        display: table;
        margin: auto;
        border: 1px dashed;
        overflow: hidden;
        border-radius: 50%;
        position: relative;
        /* height: 130px; */
        margin-bottom: 20px;
        border: 3px solid #fff;
        z-index: 2;
        width: 109px;
    }

    .s-carrusel .p-15{
        margin: 15px;
    }

    .download-section{
        border-top: 2px solid #d2d1cb;
        margin-top: 30px;
        padding-top: 30px;
    }

    .sponsor-title {

        font-size: 18px;
        text-align: center;
        color: #ABABA7;
        text-transform: uppercase;
        padding-bottom: 10px;

    }

    .donwload-link{
        color: #555555;
    }

    .s-carrusel .slick-track{
        padding-bottom: 30px;
    }

    .sponsor-box{
        padding: 15px 0px;
        background-color: white;
    }

    .picture-egg{
        border: none !important;
        height: 100% !important;
        max-width: 100% !important;
        width: 100% !important;

    }

    .a-objetives{
        color: #53BD96 !important;
        font-size: 20px !important;
    }

    .slick-track{
        margin: 0 !important;
    }
    h4#equipo{
        font-size: 30px;
        color:#53BD96;
    }

</style>

    <div class="main" role="main">
        <div class="c-about">
            <div class="c-about__bg" style="background-image:url('<?php the_field('imagen_header'); ?>');"> </div>
            <div class="container">
                <div class="row"> 
                    <div class="col-md-12">
                        <!--titulos-->
                        <div class="c-about__top">
                            <div class="row">
                                <div class="col-12">
                                    <h1><?php  tradgb('tenemos_una_mision','Tenemos una misión');?></h1>
                                    <h2><?php  tradgb('conoce_personas_te_protejo','Conoce a las personas que trabajan en Te Protejo');?></h2>
                                   
                                </div>
                            </div>
                        </div>

                        <!--information-->
                        <div class="c-about__information">
                            <div class="row">
                                <div class="col-lg-5">
                                    <h3><?php  tradgb('quienes_somos','Quiénes Somos');?></h3>
                                    <?php $texto_link=get_field('texto_link_youtube');?>
                                </div>
                                <div class="col-lg-7">
                                    <p><?php the_field('texto_quienes_somos'); ?></p>
                                </div>
                            </div>
                        </div>
                        <div class="c-about__objetives">
                            <h4><?php  tradgb('objetivos_principales','Objetivos principales');?></h4>
                            <div class="c-about__objetives-cnt">
                            <div class="c-about__objetives-subitem">
                                    <div class="c-about__objetives-item">
                                        <div class="c-about__objetives-img">
                                            
                                            <?php 
                                            
                                            $url_img_objetivo_1 = get_field('objetivo_1_fotografia');
                                            
                                            ?>

                                            <img src="<?php echo $url_img_objetivo_1['sizes']['img_objetivo_secundario']; ?>">
                                        </div>
                                        <div class="c-about__objetives-text">
                                            <h5><?php the_field('objetivo_1_titulo') ?></h5>
                                            <?php the_field('objetivo_1_descripcion') ?>
                                            <a class="a-objetives" href="<?php the_field('link_objetivos_principales'); ?>"><?php the_field('texto_objetivos_principales'); ?></a>
                                        <!-- <a class="a-objetives" href="<?php echo get_home_url(); ?>/archive-posts/?tag=<?php the_field('objetivo_1_tag') ?>">Ver post relacionados</a> -->
                                        </div>
                                    </div> 
                                    <div class="c-about__objetives-item">
                                        <div class="c-about__objetives-img">
                                            
                                            <?php 
                                            
                                            $url_img_objetivo_1 = get_field('objetivo_2_fotografia');
                                            
                                            ?>

                                            <img src="<?php echo $url_img_objetivo_1['sizes']['img_objetivo_secundario']; ?>">
                                        </div>
                                        <div class="c-about__objetives-text">
                                            <h5><?php the_field('objetivo_2_titulo') ?></h5>
                                            <?php the_field('objetivo_2_descripcion') ?>
                                            <a class="a-objetives" href="<?php the_field('link_objetivos_principales_2'); ?>"><?php the_field('texto_objetivos_principales_2'); ?></a>
                                        <!-- <a class="a-objetives" href="<?php echo get_home_url(); ?>/archive-posts/?tag=<?php the_field('objetivo_1_tag') ?>">Ver post relacionados</a> -->
                                        </div>
                                    </div>   
                                </div>
                                <div class="c-about__objetives-subitem">
                                    <div class="c-about__objetives-item">
                                        <div class="c-about__objetives-img">
                                            
                                            <?php 
                                            
                                            $url_img_objetivo_1 = get_field('objetivo_3_fotografia');
                                            
                                            ?>

                                            <img src="<?php echo $url_img_objetivo_1['sizes']['img_objetivo_secundario']; ?>">
                                        </div>
                                        <div class="c-about__objetives-text">
                                            <h5><?php the_field('objetivo_3_titulo') ?></h5>
                                            <?php the_field('objetivo_3_descripcion') ?>
                                            <a class="a-objetives" href="<?php the_field('link_objetivos_principales_3'); ?>"><?php the_field('texto_objetivos_principales_3'); ?></a>
                                       <!-- <a class="a-objetives" href="<?php echo get_home_url(); ?>/archive-posts/?tag=<?php the_field('objetivo_1_tag') ?>">Ver post relacionados</a> -->
                                        </div>
                                    </div>
                                    <div class="c-about__objetives-item">
                                        <div class="c-about__objetives-img">
                                            
                                            <?php 
                                            
                                            $url_img_objetivo_1 = get_field('objetivo_4_fotografia');
                                            
                                            ?>

                                            <img src="<?php echo $url_img_objetivo_1['sizes']['img_objetivo_secundario']; ?>">
                                        </div>
                                        <div class="c-about__objetives-text">
                                            <h5><?php the_field('objetivo_4_titulo') ?></h5>
                                            <?php the_field('objetivo_4_descripcion') ?>
                                            <a class="a-objetives" href="<?php the_field('link_objetivos_principales_4'); ?>"><?php the_field('texto_objetivos_principales_4'); ?></a>
                                       <!-- <a class="a-objetives" href="<?php echo get_home_url(); ?>/archive-posts/?tag=<?php the_field('objetivo_1_tag') ?>">Ver post relacionados</a> -->
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="c-about__staff">
                    <h4 id="equipo">Equipo</h4>
                </div>
                
            <?php

            $count=0; //reset variable
            $count2=0; //reset variable

            $arg_equipos = array(
                'posts_per_page' => -1,
                'post_type' => 'equipos',
                'order' => 'DESC', 
                'orderby' => 'date',
            );

            $the_query = new WP_Query( $arg_equipos );

            // The Loop
            while ( $the_query->have_posts() ) : $the_query->the_post();
                
                global $post;
             
                ?>

                <div class="c-about__staff">
                    <h4><?php the_title(); ?></h4>
                    <div class=" <?php switch ($count2) {
                                                                    case 0:
                                                                        echo 'd-carrusel';
                                                                        break;
                                                                    default: 
                                                                    
                                                                    echo 'c-carrusel';
                                                                    break;
                                                                }; ?>">

                   
                        <?php
                        
                        // check if the repeater field has rows of data
                        if( have_rows('equipo', $post->ID) ):

                            // loop through the rows of data
                            while ( have_rows('equipo', $post->ID) ) : the_row();

                                $fotografia_miembro = get_sub_field('fotografia');
                                $canal_youtube = get_sub_field('youtube');
                                //Siguientes líneas se obtendra objeto user de wp y la url de todos los post del user
                                $name_user = get_sub_field('nombre');
                                $user_wp = get_sub_field('vincular_usuario');
                                $instagram = get_sub_field('instagram');
                                $link_instagram = "https://www.instagram.com/".str_replace("@","", $instagram);

                                ?>

                                <div class="c-carrusel__item">
                                    <div class="c-carrusel__single <?php switch ($count) {
                                                                                            case 0:
                                                                                                # vacio sin clase color
                                                                                                break;
                                                                                            case 1:
                                                                                                echo '--pink';
                                                                                                break;
                                                                                            case 2:
                                                                                                echo '--purple';
                                                                                                break;
                                                                                        }; ?>">
                                        <div class="img-limits">
                                            <img class="picture-egg" src="<?php echo $fotografia_miembro['sizes']['thumbnail']; ?>">
                                        </div>
                                        <div class="w-100 d-table picture-name">
                                            <div class="d-table-cell align-middle">
                                                <h5><?php the_sub_field('nombre'); ?></h5>
                                            </div>
                                        </div>
                                        <p class="picture-job"><?php the_sub_field('cargo'); ?></p>

                                        <div class="description-container">
                                            <?php the_sub_field('descripcion'); ?>
                                        </div>

                                        <div class="contact-box-footer">

                                            <?php if($instagram){  ?>
                                                <a class="link-youtube" href="<?php echo $link_instagram;?>" tabindex="-1"><?php echo $instagram; ?></a>
                                            <?php  }  ?>
                                            
                                            <?php if($canal_youtube){  ?>
                                                <a class="link-youtube" href="<?php echo $canal_youtube;?>" tabindex="-1"><?php tradgb('td_canal_de_youtube',$texto_link); ?></a>
                                            <?php } ?>

                                            <?php if($canal_linkedin){  ?>
                                                <a class="link-youtube" href="<?php echo $canal_linkedin;?>" tabindex="-1"><?php tradgb('td_canal_de_linkedin','Linkedin'); ?></a>
                                            <?php } ?>

                                            <?php if($user_wp['ID'] != '1' && $user_wp['ID'] != null){  ?>

                                                <a class="btn-see" href="<?php echo get_home_url(); ?>/archive-posts/?autor=<?php echo $user_wp['ID']; ?>"><?php tradgb('ver_todo_sus','Ver todos sus'); ?> posts</a>

                                            <?php } ?>

                                        </div>
                                        
                                    </div>
                                </div>

                                <?php
                            
                            endwhile;

                        else :

                        // no rows found

                        endif;

                        ?>

                    </div>
                </div>

                <?php

                $count++;
                $count2++;

                if($count == 3){

                    $count = 0; //reset var

                }

            endwhile;

            wp_reset_query();

            ?>

            <div class="c-textcontact">
                <p><?php  tradgb('td_si_quieres_participar','Si quieres participar en nuestra área de voluntariado, escribe a');?> <a href=""><?php  tradgb('td_correo_para_voluntarios','info@teprotejo.cl');?></a></p>
            </div>

            
            <h5 class="sponsor-title d-table m-auto"><?php  tradgb('td_nuestras_alianzas','Nuestras alianzas');?></h5>
         

            <div class="d-block container s-carrusel text-center">

                     <?php

                        // check if the repeater field has rows of data
                        if( have_rows('nuestras_alianzas') ):

                            // loop through the rows of data
                            while ( have_rows('nuestras_alianzas') ) : the_row();
                       ?>
                            <div class="d--inline-block">
                                <div class="sponsor-box p-15 align-middle">
                                    <a href="<?php the_sub_field('link_alianza'); ?>">
                                        <img class="d-table m-auto" src="<?php the_sub_field('imagen_alianza'); ?>" alt="sponsor">
                                    </a>
                                </div>
                            </div>   

                      <?php         
                            endwhile;

                        else :

                            // no rows found

                        endif;

                      ?>
                  
            </div>


            <div class="c-newsletter">
                <div class="container">
                    <h4><?php  tradgb('suscribete_al_newsletter','Suscríbete al newsletter');?></h4>
                    <form>
                        <div class="row">
                            <div class="col-md-6">
                                <p><?php  tradgb('quieres_recibir_nuestras_noticias','¿Quieres recibir nuestras noticias?');?></p>
                            </div>
                            <div class="col-md-6">
                                <div class="c-newsletter__inputs">
                                    <input id="email_newsletter" placeholder="<?php  tradgb('escribe_tu_email','Escribe tu email');?>" type="text">
                                    <input type="hidden" id="blog_origen_newsletter" value="<?php echo $site_name; ?>">
                                    <button id="newsletter"><?php  tradgb('suscribete','Suscríbete');?></button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>

                <div class="d-block download-section">

                    <div class="container">
                        <div class="row">
                             <?php
                               $estado_financiero=get_field('estado_financiero');
                               $memorias_anuales=get_field('memorias_anuales');
                               ?>
                            <a href="<?php echo $estado_financiero['url']; ?>" class="col-6 col-md-3 col-xl-2 text-capitalize donwload-link">
                                <i class="fa fa-download"></i>
                                <?php  tradgb('td_estado_financiero','Estado financiero');?>
                            </a>
                            <a href="<?php echo $memorias_anuales['url']; ?>" class="col-6 col-md-3 col-xl-2 text-capitalize donwload-link">
                                <i class="fa fa-download"></i>
                                <?php  tradgb('td_memorias_anuales','memorias anuales');?> 
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    
<?php 

get_footer(); 

?>