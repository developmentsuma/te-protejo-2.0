<?php

/*Template Name: Template Secciones*/ 

get_header('nuevo'); 
session_start();
$count= 0;
$blog_id = get_current_blog_id();
$site_name = get_blog_details($blog_id)->blogname;
?>

<style>
    
    .img-limits{
        display: table;
        margin: auto;
        border: 1px dashed;
        overflow: hidden;
        border-radius: 50%;
        position: relative;
        /* height: 130px; */
        margin-bottom: 20px;
        border: 3px solid #fff;
        z-index: 2;
        width: 109px;
    }

    .s-carrusel .p-15{
        margin: 15px;
    }

    .download-section{
        border-top: 2px solid #d2d1cb;
        margin-top: 30px;
        padding-top: 30px;
    }

    .sponsor-title {

        font-size: 18px;
        text-align: center;
        color: #ABABA7;
        text-transform: uppercase;
        padding-bottom: 10px;

    }

    .donwload-link{
        color: #555555;
    }

    .s-carrusel .slick-track{
        padding-bottom: 30px;
    }

    .sponsor-box{
        padding: 15px 0px;
        background-color: white;
    }

    .picture-egg{
        border: none !important;
        height: 100% !important;
        max-width: 100% !important;
        width: 100% !important;

    }

    .a-objetives{
        color: #53BD96 !important;
        font-size: 20px !important;
    }

    .slick-track{
        margin: 0 !important;
    }
    h4#equipo{
        font-size: 30px;
        color:#53BD96;
    }

    /* Nuevo CSS */
.template-title{
 font-size: 64px !important;
line-height: 79px !important;
text-align: center;
letter-spacing: -0.008em;
color: #898989 !important;
margin-bottom:40px;
}

@media only screen and (max-width: 768px){

.template-title{
font-size: 40px !important;
line-height: 40px !important;
}

}
 

.template-img{
    margin: auto;
    display: block;
}
.template-content{
    margin-top:50px;
}

.template-content strong{
font-weight: 600;
font-size: 24px;
line-height: 28px;
text-align: center;
letter-spacing: 0.016em;
color: #339966;
}

.template-content img{
    margin: auto;
    display: block;
}



</style>

<?php


$titulo = get_field('titulo');
$subtitulo = get_field('subtitulo_superior');
$titulo_contenido = get_field('titulo_contenido');
$contenido = get_field('contenido');
$imagen = get_field('imagen');
 
 

?>

    <div class="main" role="main">
        <div class="c-about">
            <div class="c-about__bg" style="background:#62C29E;"></div>
            <div class="container">
                <div class="row"> 
                    <div class="col-md-12">
                        <!--titulos-->
                        <div class="c-about__top">
                            <div class="row">
                                <div class="col-12">
                                    <h1><?php echo $titulo ;?></h1>
                                    <h2><?php echo $subtitulo ;?></h2>
                                   
                                </div>
                            </div>
                        </div>

                        <!--information-->
                        <div class="c-about__information">
                            <div class="row">
                                <div class="col-lg-12">
                                    <h3 class="template-title"><?php echo $titulo_contenido ;?></h3>
                                    <img src="<?php echo $imagen['url'] ;?>" alt="" class="template-img">
                                    <div class="template-content"><?php echo $contenido ;?></div>
                                </div>
                            </div>
                        </div>
             
                    </div>
                </div>
            </div>
         
            <div class="c-newsletter">
                <div class="container">
                    <h4><?php if($_GET['lang']=='en'):  tradgb('suscribete_al_newsletter','Sign up for our Newsletter'); else: tradgb('suscribete_al_newsletter','Suscríbete al newsletter'); endif; ?></h4>
                    <form>
                        <div class="row">
                            <div class="col-md-6">
                                <p><?php if($_GET['lang']=='en'):  tradgb('quieres_recibir_nuestras_noticias','¿Want to recieve our information?'); else: tradgb('quieres_recibir_nuestras_noticias','¿Quieres recibir nuestras noticias?'); endif;?></p>
                            </div>
                            <div class="col-md-6">
                                <div class="c-newsletter__inputs">
                                    <input id="email_newsletter" placeholder="<?php if($_GET['lang']=='en'):  tradgb('escribe_tu_email','Write your email'); else :  tradgb('escribe_tu_email','Escribe tu email'); endif;?>" type="text">
                                    <input type="hidden" id="blog_origen_newsletter" value="<?php echo $site_name; ?>">
                                    <button id="newsletter"><?php if($_GET['lang']=='en'):  tradgb('suscribete','SUBSCRIBE'); else :  tradgb('suscribete','Suscríbete'); endif; ?></button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
                <?php if($_GET['lang']!='en') { ?>
                    <div class="d-block download-section">

                        <div class="container">
                            
                            <div class="row">
                                <?php
                                $estado_financiero=get_field('estado_financiero');
                                $memorias_anuales=get_field('memorias_anuales');
                                ?>
                                <a href="<?php echo $estado_financiero['url']; ?>" class="col-6 col-md-3 col-xl-2 text-capitalize donwload-link">
                                    <i class="fa fa-download"></i>
                                    <?php  tradgb('td_estado_financiero','Estado financiero');?>
                                </a>
                                <a href="<?php echo $memorias_anuales['url']; ?>" class="col-6 col-md-3 col-xl-2 text-capitalize donwload-link">
                                    <i class="fa fa-download"></i>
                                    <?php  tradgb('td_memorias_anuales','memorias anuales');?> 
                                </a>
                            </div>
                            
                        </div>
                    </div>
                <?php } ?>
            </div>
        </div>
    </div>
    
<?php 

get_footer(); 

?>