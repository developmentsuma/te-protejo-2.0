<?php

require "db.php";
ini_set('display_errors', 'Off');

$db = new DB();

function save($nombre = '', $email = '', $motivo = '',$pais = '', $mensaje = '', $sitio = '')
{
    global $db;

    $nombre = limpiarvariable($nombre, $db->connection());
    $email = limpiarvariable($email, $db->connection());
    $motivo = limpiarvariable($motivo, $db->connection());
	$pais = limpiarvariable($pais, $db->connection());
    $mensaje = limpiarvariable($mensaje, $db->connection());
    $sitio = limpiarvariable($sitio, $db->connection());

    $insert = $db->insert("INSERT INTO `email_contactos`(`id`, `nombre`, `email`, `motivo`, `pais`,`mensaje`,`sitio_origen`)
						VALUES (NULL, '" . $nombre . "', '" . $email . "', '" . $motivo . "', '" . $pais . "', '" . $mensaje . "', '" . $sitio . "')");

    return $insert;
}

switch ($_POST['motive']) {
    case 'informacion_general':
        $para = 'info@teprotejo.cl';
        $motivo_text = 'Información General';
        break;
    case 'feria_ecobelleza':
        $para = 'feria@ecobelleza.cl';
        $motivo_text = 'Feria Ecobelleza';
        break;
    case 'tienda_ecobelleza':
        $para = 'info@tiendaecobelleza.cl';
        $motivo_text = 'Tienda Ecobelleza';
        break;
    case 'certificaciones':
        $para = 'info@teprotejo.cl';
        $motivo_text = 'Certificaciones';
        break;
    case 'comunicaciones_prensa':
        $para = 'comunicaciones@teprotejo.cl';
        $motivo_text = 'Comunicaciones y Prensa';
        break;
    case 'activismo':
        $para = 'activismo@teprotejo.cl';
        $motivo_text = 'Activismo';
        break;
}

$html_defecto = '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
	<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
	<meta http-equiv="Content-Type" content="text/html; " />
	<meta charset="utf-8" />
	<title>Nuevo Contacto - Te Protejo </title>
	</head>

	<body>
	<table style="margin: 0 auto; font-family: Arial, Helvetica, sans-serif;" width="600" border="0" cellpadding="0" cellspacing="0">
		<tr>
			<td><span style="color:#333333;font-size: 18px;font-weight:bold;">Notificacion ' . $motivo_text . '</span></td>
		</tr>
		<tr>
			<td style="font-size:12px;">Contacto desde web ongteprotejo.com: </td>
		</tr>
		<tr>
			<td>
				<table width="600" cellpadding="0" cellspacing="0" style="margin-top: 10px;">
					<tr>
						<td valign="top" style="font-size:12px; font-weight: bold; padding: 10px 0px 10px 0px; border-top: 1px solid #d7d7d7;">Nombre:</td>
						<td style="font-size:12px; padding: 10px 0px 10px 0px; border-top: 1px solid #d7d7d7;">{NOMBRE}</td>
					</tr>
					<tr>
						<td valign="top" style="font-size:12px; font-weight: bold; padding: 10px 0px 10px 0px; border-top: 1px solid #d7d7d7;">E-mail:</td>
						<td style="font-size:12px; padding: 10px 0px 10px 0px; border-top: 1px solid #d7d7d7;">{EMAIL}</td>
					</tr>
					<tr>
						<td valign="top" style="font-size:12px; font-weight: bold; padding: 10px 0px 10px 0px; border-top: 1px solid #d7d7d7;">País de origen:</td>
						<td style="font-size:12px; padding: 10px 0px 10px 0px; border-top: 1px solid #d7d7d7;">{PAIS}</td>
					</tr>
					<tr>
						<td valign="top" style="font-size:12px; font-weight: bold; padding: 10px 0px 10px 0px; border-top: 1px solid #d7d7d7;">Mensaje:</td>
						<td style="font-size:12px; padding: 10px 0px 10px 0px; border-top: 1px solid #d7d7d7;">{MENSAJE}</td>
					</tr>
				</table>
			</td>
		</tr>
		<tr>
			<td style="padding:20px 0px 20px 0px; font-size: 10px; color: #666;"></td>
		</tr>
	</table>
	</body>
	</html>';

$full_name = $_POST['name'];
$affected_rows_db = save($full_name, $_POST['email'], $_POST['motive'], $_POST['country'], $_POST['message'], $_POST['blog_origen']);

$html_defecto = str_replace('{NOMBRE}', $full_name, $html_defecto);
$html_defecto = str_replace('{EMAIL}', $_POST['email'], $html_defecto);
$html_defecto = str_replace('{MOTIVO}', $_POST['motive'], $html_defecto);
$html_defecto = str_replace('{PAIS}', $_POST['country'], $html_defecto);
$html_defecto = str_replace('{MENSAJE}', $_POST['message'], $html_defecto);

$tipo = $_POST['name'] . ' Te Protejo';

$headers = 'MIME-Version: 1.0' . "\r\n";
$headers .= 'Content-type: text/html; charset=utf-8' . "\r\n";
$headers .= 'From: Te Protejo <no-responder@teprotejo.cl>' . "\r\n";
$headers .= "Cc: \r\n";

$mail_enviado = mail($para, $tipo, $html_defecto, $headers);

/*$para*/
echo 'ok';
