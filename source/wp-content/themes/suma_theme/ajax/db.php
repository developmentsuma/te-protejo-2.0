<?php
class DB
{
	private $_server;
	private $_user;
	private $_pass;
	private $_bd;
	/*Construct*/

	public function __construct()
	{
		$this->_server = 'localhost';
		$this->_user = 'teprotej_app_101';
		$this->_pass = 'aQ.)Z?cU7oS2';
		$this->_bd = 'teprotej_ong';
	}
	
	/*Methods*/
	private function _connect()
	{
		if (!$con = mysqli_connect($this->_server, $this->_user, $this->_pass)) {
			echo "No se puede conectar a la base de datos";
			exit();
		}

		if (!mysqli_select_db($con, $this->_bd)) {
			echo "No se pudo seleccionar la base de datos";
			exit();
		}
		mysqli_set_charset($con, 'utf8');
		return $con;
	}

	public function connection()
	{
		return $this->_connect();
	}

	private function _insert($query)
	{
		$con = $this->connection();
		$res = mysqli_query($con, $query);
		$rows = mysqli_insert_id($con);
		mysqli_close($con);
		return $rows;

	}

	public function insert($query)
	{
		return $this->_insert($query);
	}

	private function _select($query)
	{
		$con = $this->connection();
		$res = mysql_query($query, $con);
		mysql_close($con);
		return $res;
	}

	public function query($query)
	{
		$res = $this->select($query);
		if ($res == 1) {
			return true;
		}

		if (count($res) > 0) {
			while ($resp = mysql_fetch_assoc($res)) {

				$response[] = $resp;

			}
			return $response;
		}


	}

	public function select($query)
	{
		return $this->_select($query);
	}

	private function _insertarAjax($query)
	{
		$con = $this->connection();
		$res = mysql_query($query, $con);
		mysql_close($con);
	}

	public function insertarAjax($query)
	{
		$this->_insertarAjax($query);
	}

	private function _update($query)
	{
		$con = $this->connection();
		$res = mysql_query($query, $con);
		mysql_close($con);
	}

	public function update($query)
	{
		$this->_update($query);
	}

	public function getStructure($nombre_tabla)
	{
		$con = $this->connection();
		$resultado = mysql_query("SHOW COLUMNS FROM " . $nombre_tabla, $con);
		$tabla = array();
		if (!$resultado) {
			echo 'No se pudo ejecutar la consulta: ' . mysql_error();
			exit;
		}
		if (mysql_num_rows($resultado) > 0) {
			while ($fila = mysql_fetch_assoc($resultado)) {
				$tabla[] = $fila;
			}
		}
		return $tabla;
	}


}

function url_exists($url = NULL)
{

	if ( ($url == '') || ($url == NULL)) {
		return false;
	}

	$headers = @get_headers($url);
	sscanf($headers[0], 'HTTP/%*d.%*d %d', $httpcode);

    //Aceptar solo respuesta 200 (Ok), 301 (redirección permanente) o 302 (redirección temporal)
	$accepted_response = array(200, 301, 302);
	if (in_array($httpcode, $accepted_response)) {
		return true;
	} else {
		return false;
	}
}

function limpiarCadena($valor)
{
	$valor = str_ireplace("SELECT", "", $valor);
	$valor = str_ireplace("COPY", "", $valor);
	$valor = str_ireplace("DELETE", "", $valor);
	$valor = str_ireplace("DROP", "", $valor);
	$valor = str_ireplace("DUMP", "", $valor);
	$valor = str_ireplace(" OR ", "", $valor);
	$valor = str_ireplace("%", "", $valor);
	$valor = str_ireplace("LIKE", "", $valor);
	$valor = str_ireplace("--", "", $valor);
	$valor = str_ireplace("^", "", $valor);
	$valor = str_ireplace("[", "", $valor);
	$valor = str_ireplace("]", "", $valor);
	$valor = str_ireplace("\\", "", $valor);
	$valor = str_ireplace("!", "", $valor);
	$valor = str_ireplace("¡", "", $valor);
	$valor = str_ireplace("?", "", $valor);
	$valor = str_ireplace("=", "", $valor);
	// $valor = str_ireplace("&","",$valor);
	$valor = str_ireplace("*", "", $valor);
	$valor = str_ireplace("/", "", $valor);
	$valor = str_ireplace("FROM", "", $valor);
	return $valor;
}
function limpiarvariable($valor, $db)
{
	$return = mysqli_real_escape_string($db, addslashes(limpiarCadena(strip_tags(str_replace('"', '-', $valor)))));
	return $return;
}

?>