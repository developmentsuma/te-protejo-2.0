<?php
/**
 * The Template for displaying all single posts.
 *
 * @package WordPress
 * @subpackage Twenty_Ten
 * @since Twenty Ten 1.0
 */

get_header(); 

?>

<style>
	
	.container-img-blog{
		width: 100%;
    	text-align: center;
	}

	.container-img-blog img{
		margin: 0 auto;
		margin-botom: 30px;
	}

</style>

    <div class="main" role="main">
        <div class="c-blog">
            <div class="c-blog__bg">
                <div class="c-blog__bg2">
                </div>
            </div>
            <div class="container">
                <div class="row justify-content-md-center"> 
                    <div class="col-xl-10">
                        <div class="c-blog__top">
                            <div class="row">
                                <div class="col-6">
                                    <a href="<?php echo get_home_url(); ?>/blog-cruelty-free/"><i class="fas fa-arrow-left"></i> Volver al blog</a>
                                </div>
                                <div class="col-6 text-right"> 

									<?php

										$next_post_id = get_next_post()->ID;
										$prev_post_id = get_previous_post()->ID;

										if($next_post_id != ''){

											?>

											<a href="<?php the_permalink($next_post_id); ?>">Siguiente post <i class="fas fa-arrow-right"></i></a>

											<?php

										}else{

											?>

											<a href="<?php the_permalink($prev_post_id); ?>"><i class="fas fa-arrow-left"></i> Post anterior</a>

											<?php
										}
									
									?>
                                    
                                </div>
                            </div>
                        </div>
                        <div class="c-blog__single">
                            <div class="c-blog__single-info">

								<?php

								$post_type = $post->post_type;
								$post_type_object = get_post_type_object( $post_type );
								$img_post = get_the_post_thumbnail($id_post, 'full');
								$img_post = preg_replace( '/(height)=\"\d*\"\s/', "", $img_post );
								$tags = get_the_tags($id_post); 
								$id_post = $post->ID;
								$tipo_post = $post_type_object->labels->name;								
								

								?>
								
                                <span><strong><?php echo $tipo_post; ?></strong></span>
                                <span>|</span><small>Publicado el <?php echo get_the_date('d-m-Y'); ?></small>
                            </div>
							<h1><?php the_title(); ?></h1>
							<div class="container-img-blog">
								<?php echo $img_post; ?>
							</div>
                            <!-- <img src="<?php echo $img_post; ?>"> -->
                            <div class="c-blog__tag">

								<?php
								
								if ($tags) {
								
									?>
									
									<i class="fas fa-tag"> </i>
									
									<?php

									foreach ($tags as $tag) {
																	
										?>

										<a href="<?php echo get_home_url(); ?>/archive-posts/?tag=<?php echo $tag->slug; ?>"><?php echo $tag->name;  ?></a>

										<?php

									}
								
								}

								?>
								
                            </div>
                            <div class="c-blog__resume">
                                <p><?php the_field('extracto'); ?></p>
                            </div>
                            <div class="c-blog__main">
                                <p><?php echo $post->post_content; ?></p>
                            </div>
                        </div>
                        <div class="c-blog__share">
                            <p>comparte:</p>
	
							<!-- Your share button code -->
							<div class="fb-share-button" data-href="<?php the_permalink(); ?>" data-layout="button" data-size="large" data-mobile-iframe="true">
							</div>
	
							<a id="copiar_url" class="btn-copy" href="#"> <i class="fas fa-copy"></i>
								Copiar URL
							</a>

							<a class="copiar_url" style="float:right" href="<?php echo get_home_url(); ?>/archive-posts/?autor=<?php echo $post->post_author; ?>">
								Más posts de <?php the_author_meta('display_name', $post->post_author); ?>
							</a>

							<div class="c-blog__main">

								<div class="row w-100 m-auto">
								<div id="comentarios-form" class="col-md-8 offset-md-2">
								
								
								<?php 
									comment_form( array(
									'id_form'           => 'form_comentario',
									'id_submit'         => 'btn-enviar',
									'comment_notes_after' => '',
									'comment_notes_before' => '',
									'comment_field' =>  '<p class="campo comentario-text-area"><label for="comment">' . _x( 'Comment', 'noun' ) .
										'</label><textarea id="comment" name="comment" class="" aria-required="true">' .
										'</textarea></p>',
									'fields' => array(

									'author' =>
										'<p class="campo comentario-text-input"><label for="nombre_comentario">' . __( 'Nombre:', 'domainreference' ) . '</label> ' .
										( $req ? '<span class="required">*</span>' : '' ) .
										'<input id="author" name="author" type="text" value="' . esc_attr( $commenter['comment_author'] ) .
										'" size="30"' . $aria_req . ' /></p>',

									'email' =>
										'<p class="campo comentario-text-input"><label for="email">' . __( 'Email:', 'domainreference' ) . '</label> ' .
										( $req ? '<span class="required">*</span>' : '' ) .
										'<input id="email" name="email" type="text" value="' . esc_attr(  $commenter['comment_author_email'] ) .
										'" size="30"' . $aria_req . ' /></p>',

									)), $post->ID ); 
								?>	
								<!--form id="form_comentario">
									<p class="campo"><label for="nombre_comentario">Nombre</label> <input id="nombre_comentario" /></p>
									<p class="campo"><label for="correo_comentario">Correo</label> <input id="correo_comentario" /></p>
									<p class="campo"><label for="mensaje_comentario">Mensaje</label> <textarea id="mensaje_comentario"></textarea></p>
									<p id="btn-enviar"><a href="" title="Enviar">Enviar</a></p>
								</form-->
								<p id="accedes">Al enviar tus consultas o comentarios estás accediendo a recibir nuestro newsletter de forma mensual.</p>
							</div>
							</div>
								
								<?php

								$comments = get_comments(array ( 'post_id' => $id_post ));

								foreach($comments as $comment) : 
									?>
									
									<div class="comentario">
										<div class="globo"></div>
										<p class="datos-comentario"><span><?php echo($comment->comment_author);?></span> <?php $comment->comment_date; ?> dice:</p>
										<p><?php echo($comment->comment_content);?></p>
									</div>
									
									<?php 

								endforeach;
								
								?>

                            </div>

                        </div>
                    </div>
                </div>
                <div class="row c-blog__related">
                    <div class="col-md-12">
                        <h2><?php echo $tipo_post; ?></h2>
                        <a class="btn-all" href="<?php echo get_home_url(); ?>/archive-posts/?post_type=<?php echo $tipo_post; ?>">Ver todas</a>
					</div>
					
					<?php

					$arg_post = array(
						'posts_per_page' => 2,
						'post_type' => $tipo_post,
						'post__not_in' => array($id_post),
						'order' => 'DESC',
						'orderby' => 'date',
					);
					
					// The Query
					$the_query = new WP_Query( $arg_post );

					// The Loop
					while ( $the_query->have_posts() ) : $the_query->the_post();

						$post_type = $post->post_type;
						$post_type_object = get_post_type_object( $post_type );
						$img_post = get_the_post_thumbnail_url( null, 'img_front_blog' );

						?>

						<div class="col-xl-6">
							<div class="c-blog__related-item">
								<div class="row">
									<div class="col-xl-6 col-lg-5 col-md-5">
										<a href="<?php the_permalink(); ?>">
											<img src="<?php echo $img_post; ?>">
										</a>
									</div>
									<div class="col-md-6"> 
										<span><?php echo $post_type_object->name; ?></span>
										<h3><?php the_title(); ?></h3>
										<p><?php echo get_excerpt(80); ?></p>
										<a href="<?php the_permalink(); ?>">Ver <?php echo $post_type_object->labels->singular_name; ?></a>
									</div>
								</div>
							</div>
						</div>

						<?php

					endwhile;

					// Reset Query
					wp_reset_postdata();

					?>
                    
                </div>
            </div>
            <div class="c-newsletter">
                <div class="container">
                    <h4>Suscríbete al newsletter</h4>
                    <form>
                        <div class="row">
                            <div class="col-md-6">
                                <p>¿Quieres recibir nuestras noticias?</p>
                            </div>
                            <div class="col-md-6">
                                <div class="c-newsletter__inputs">
                                    <input id="email_newsletter" placeholder="Escribe tu email" type="text">
                                    <button id="newsletter">Suscríbete</button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

		
<?php 

get_footer(); 

?>
