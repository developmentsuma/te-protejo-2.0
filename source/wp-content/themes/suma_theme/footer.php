<?php
/**
 * @package WordPress
 * @subpackage SUMA
 */
?>

        <?php $lenguaje = $_GET['lang'];?>

        <?php if($lenguaje=='es') :
                $texto_menu = 'ES';
            elseif($lenguaje=='en') :
                $texto_menu = 'EN';
            else:
                $texto_menu = 'Selector de idioma';
            endif;    
        ?>
        <?php if($lenguaje =='en') : ?>

        <div class="c-footer">
            <div class="container">
                <div class="row">
                    <div class="col-xl-1 col-md-2 col-sm-3">
                        <img src="<?php bloginfo( 'template_url' ); ?>/img/cnt-logo_small.jpg">
                    </div>
                    <div class="col-xl-4 col-lg-6 col-md-6 col-sm-8">
                        <ul>
                        <li><a href="<?php echo get_home_url(); ?>/ong-te-protejo-en?lang=en">About Us</a></li>
                        <!-- <li><a href="">Noticias y prensa</a></li> -->
                        <li><a href="<?php echo get_home_url(); ?>/contacto">Contact</li>
                        </ul>
                    </div>
                    <div class="col-md-2">
                        <a href="https://www.instagram.com/teprotejo/"><i class="fab fa-instagram"></i></a>
                        <a href="https://www.facebook.com/TeProtejo/"><i class="fab fa-facebook"></i></a>
                        <a href="https://twitter.com/TeProtejo"><i class="fab fa-twitter"></i></a>
                        <a href="https://www.youtube.com/channel/UCJRZXwBBOAPuzPJkQoMrBMA"><i class="fab fa-youtube"></i></a>
                        <a href="mailto:info@ongteprotejo.org"><small>info@ongteprotejo.org</small></a>
                    </div>
                    <div class="col-md-3  ">
                        <ul>
                          <li><?php tradgb('td_casa_matriz','Headquarters, Santiago. Chile.');?> </li>
                        </ul>
                    </div>
                    <a class="btn-suma" href="https://www.suma.cl/" target="_blank">SUMA - Agencia Digital</a>
                </div>
            </div>
        </div>

        <?php else : ?>

            <div class="c-footer">
            <div class="container">
                <div class="row">
                    <div class="col-xl-1 col-md-2 col-sm-3">
                        <img src="<?php bloginfo( 'template_url' ); ?>/img/cnt-logo_small.jpg">
                    </div>
                    <div class="col-xl-4 col-lg-6 col-md-6 col-sm-8">
                        <ul>
                        <li><a href="<?php echo get_home_url(); ?>/ong-te-protejo">ONG Te Protejo</a></li>
                        <li><a href="<?php echo get_home_url(); ?>/blog-cruelty-free">Blog Cruelty-free</a></li>
                        <li><a href="<?php echo get_home_url(); ?>/marcas-cruelty-free"><?php tradgb('td_marcas','Marcas');?> Cruelty-free</a></li>
                        <!-- <li><a href="<?php echo get_home_url(); ?>/feria-ecobelleza"><?php tradgb('feria_ecobelleza','Feria Ecobelleza');?></a></li> -->
                        <!-- <li><a href="">Noticias y prensa</a></li> -->
                        <li><a href="<?php echo get_home_url(); ?>/contacto"><?php tradgb('td_contacto','Contacto');?></a></li>
                        </ul>
                    </div>
                    <div class="col-md-2">
                        <a href="https://www.instagram.com/teprotejo/"><i class="fab fa-instagram"></i></a>
                        <a href="https://www.facebook.com/TeProtejo/"><i class="fab fa-facebook"></i></a>
                        <a href="https://twitter.com/TeProtejo"><i class="fab fa-twitter"></i></a>
                        <a href="https://www.youtube.com/channel/UCJRZXwBBOAPuzPJkQoMrBMA"><i class="fab fa-youtube"></i></a>
                        <a href="mailto:info@ongteprotejo.org"><small>info@ongteprotejo.org</small></a>
                    </div>
                    <div class="col-md-3  ">
                        <ul>
                          <li><?php tradgb('td_casa_matriz','Casa Matriz: Santiago, Chile.');?> </li>
                        </ul>
                    </div>
                    <a class="btn-suma" href="https://www.suma.cl/" target="_blank">SUMA - Agencia Digital</a>
                </div>
            </div>
        </div>

        <?php endif; ?>
	
        <?php

            wp_footer();

            $pagina_actual = basename(get_permalink()); //se usa para mostrar JS cuando corresponda según página
            $v_assets = "8.2";
        ?>

        <script src="<?php bloginfo( 'template_url' ); ?>/js/libs/bundle.js">   </script>
        <script src="<?php bloginfo( 'template_url' ); ?>/js/bootstrap.js"> </script>
        <script src="<?php bloginfo( 'template_url' ); ?>/js/slick.min.js"> </script>
        <script src="https://rawgit.com/utatti/perfect-scrollbar/master/dist/perfect-scrollbar.js"> </script>
        <script src="<?php bloginfo( 'template_url' ); ?>/js/main.min.js?v=10.1"></script>
        <script src="<?php bloginfo( 'template_url' ); ?>/js/contact-form.js?v=3.6"></script>
        <script src="<?php bloginfo( 'template_url' ); ?>/js/jquery-ui.js"></script>
        <script src="<?php bloginfo( 'template_url' ); ?>/js/paginador-plugin/jquery.bootpag.min.js" type="text/javascript"></script>
        <script src="<?php bloginfo( 'template_url' ); ?>/js/newsletter.js?v=<?php echo $v_assets; ?>"></script>
        <script src="<?php bloginfo( 'template_url' ); ?>/js/copiarUrl.js?v=<?php echo $v_assets; ?>"></script>
        <script src="<?php bloginfo( 'template_url' ); ?>/js/buscarPosts.js?v=<?php echo $v_assets; ?>"></script>
        <script src="<?php bloginfo( 'template_url' ); ?>/js/infinite_scroll.js?v=<?php echo $v_assets; ?>"></script>
        <script src="<?php bloginfo( 'template_url' ); ?>/js/infinite_scroll_archive.js?v=<?php echo $v_assets; ?>"></script>
        <script src="https://cdn.jsdelivr.net/npm/js-cookie@3.0.1/dist/js.cookie.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/selectize.js/0.12.6/js/standalone/selectize.min.js" integrity="sha256-+C0A5Ilqmu4QcSPxrlGpaZxJ04VjsRjKu+G82kl5UJk=" crossorigin="anonymous"></script>

        <script>
            var lenguaje = '<?php echo $_GET['lang']; ?>';
            if(lenguaje!=''){
            Cookies.set('lenguaje', lenguaje); //cookie de sesión
            } 
        </script>

    </body>
</html>

