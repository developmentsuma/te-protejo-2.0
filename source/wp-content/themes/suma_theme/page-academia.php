<?php

/*Template Name: academia */

get_header('nuevo');

$count = 0;
$blog_id = get_current_blog_id();
$site_name = get_blog_details($blog_id)->blogname;


?>
<style>
    .academy .bg {
        background: #E07E99;
        height: 350px;
        position: absolute;
        left: 0px;
        top: -100px;
        width: 100%;
        z-index: 0;
    }

    .academy_title {
        font-family: 'Signika';
        font-style: normal;
        font-weight: 400;
        font-size: 40px;
        line-height: 49px;
        text-align: center;
        color: #282828;
    }

    .academy_sub_title {
        font-family: 'Signika';
        font-style: normal;
        font-weight: 400;
        font-size: 20px;
        line-height: 25px;
        text-align: center;
        letter-spacing: 0.01em;
        color: white;
    }

    @media screen and (max-width: 769px) {
        .academy .bg {
            background: #E07E99;
            height: 395px;
            position: absolute;
            left: 0px;
            top: -100px;
            width: 100%;
            z-index: 0;
        }
    }
</style>
<div class="main" role="main">
    <div class="academy">
        <div class="bg" style="background-image:url('<?php the_field('imagen_header_izquierda'); ?>');">
            <div class="c-blog__bg2" style="background-image:url('<?php the_field('imagen_header_derecha'); ?>');">
            </div>
        </div>
        <div class="container">
            <div class="row">
                <div class="col-md-1">
                </div>
                <div class="col-md-10">
                    <div class="c-blog__top">
                        <div class="row">
                            <div class="col-12">
                                <h1 class="academy_title"><?php tradgb('academy_title', 'ACADEMIA'); ?></h1>
                                <h2 class="academy_sub_title"><?php tradgb('academy_sub_title', 'Todo el material que necesitas para entender en profundidad la experimentación en animales para cosmética y sus opciones de reemplazo.'); ?></h2>
                            </div>
                        </div>
                    </div>
                    <div class="c-blog__important">
                        <div class="carousel slide" id="carouselExampleSlidesOnly" data-ride="carousel">
                            <div class="carousel-inner">

                                <?php

                                $posts = get_field('slider_de_posts');
                                $total_slides = count($posts);
                                $id_posts_slider = array();

                                if ($posts) :

                                    foreach ($posts as $post) : // variable must be called $post (IMPORTANT) 

                                        setup_postdata($post);

                                        $post_type = $post->post_type;
                                        $post_type_object = get_post_type_object($post_type);
                                        $img_post = get_the_post_thumbnail_url();
                                        /* $img_post = get_the_post_thumbnail_url( null, 'img_slider_posts' ); */
                                        $id_post = $post->ID;

                                ?>

                                        <div class="carousel-item <?php if ($count == 0) {
                                                                        echo 'active';
                                                                    } ?>">
                                            <div class="row">
                                                <div class="col-lg-6">
                                                    <a href="<?php the_permalink(); ?>">
                                                        <img src="<?php echo $img_post; ?>">
                                                    </a>
                                                </div>
                                                <div class="col-lg-6">
                                                    <div class="c-blog__important-txt">
                                                        <span><?php tradgb($post_type_object->labels->singular_name, $post_type_object->labels->singular_name); ?></span>
                                                        <h3><?php the_title(); ?></h3>
                                                        <p><?php echo get_excerpt(80); ?></p>
                                                        <a href="<?php the_permalink(); ?>"><?php tradgb('ver', 'Ver');
                                                                                            echo ' ';
                                                                                            tradgb($post_type_object->labels->singular_name, $post_type_object->labels->singular_name); ?></a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                <?php

                                        $count++;
                                        array_push($id_posts_slider, $id_post); //se agregan ids de posts ya mostrados

                                    endforeach;

                                    wp_reset_postdata(); // IMPORTANT - reset the $post object so the rest of the page works correctly 

                                endif;

                                ?>
                            </div>
                            <ol class="carousel-indicators-new ol carousel-indicators">

                                <?php

                                for ($i = 0; $i < $total_slides; $i++) {

                                ?>

                                    <li class="<?php if ($i == 0) {
                                                    echo 'active';
                                                } ?>" data-target="#carouselExampleSlidesOnly" data-slide-to="<?php echo $i; ?>"><?php echo $i + 1; ?></li>

                                <?php

                                }

                                ?>

                            </ol>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row c-blog__articles pt-5">
                <!-- <div class="col-md-12">
                    <h2><?php tradgb('lo_ultimo_del_blog', 'Lo último del blog'); ?></h2>
                </div> -->

                <?php

                $arg_ultimos_blog = array(
                    'posts_per_page' => 16,
                    'post_type' => array('articulos'),
                    'order' => 'DESC',
                    'orderby' => 'date',
                    'taxonomy' => 'articulos_category',
                    'tax_query' => array(
                        'relation' => 'AND',
                        array(
                            'taxonomy' => 'articulos_category',
                            'field'    => 'slug',
                            'terms'    => array('academia'),
                        )
                    ),
                );

                // The Query
                query_posts($arg_ultimos_blog);

                // The Loop
                while (have_posts()) : the_post();

                    $post_type = $post->post_type;
                    $post_type_object = get_post_type_object($post_type);
                    /* $img_post = get_the_post_thumbnail_url( null, 'img_front_blog' ); */
                    $img_post = get_the_post_thumbnail_url();
                    $id_post = $post->ID;

                ?>

                    <div class="col-lg-3 col-md-6">
                        <div class="c-blog__articles-item">
                            <a class="picture-container" href="<?php the_permalink(); ?>">
                                <img src="<?php echo $img_post; ?>">
                            </a>
                            <div class="c-blog__articles-item-text">
                                <span><?php tradgb($post_type_object->name, $post_type_object->name); ?></span><!-- traducir -->
                                <h3><?php the_title(); ?></h3>
                                <div class="c-blog__articles-item-text-description">
                                    <p><?php echo get_excerpt(80); ?></p>
                                </div>
                                <a href="<?php the_permalink(); ?>"><?php tradgb('ver', 'Ver'); ?> <?php echo ' ' . tradgb('más', 'más'); ?></a>
                            </div>
                        </div>
                    </div>

                <?php

                    array_push($id_posts_slider, $id_post); //se agregan más ids de posts ya mostrados

                endwhile;

                // Reset Query
                wp_reset_query();

                ?>

            </div>


            <div id="hook-scroll" class="c-newsletter">
                <div class="container">
                    <h4><?php tradgb('suscribete_al_newsletter', 'Suscríbete al newsletter');    ?></h4>
                    <form>
                        <div class="row">
                            <div class="col-md-6">
                                <p><?php tradgb('quieres_recibir_nuestras_noticias', '¿Quieres recibir nuestras noticias?'); ?></p>
                            </div>
                            <div class="col-md-6">
                                <div class="c-newsletter__inputs">
                                    <input id="email_newsletter" type="text" placeholder="<?php tradgb('escribe_tu_email', 'Escribe tu email'); ?>">
                                    <input type="hidden" id="blog_origen_newsletter" value="<?php echo $site_name; ?>">
                                    <button id="newsletter"><?php tradgb('suscribete', 'Suscríbete'); ?></button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <?php

    get_footer();

    ?>