<?php

get_header(); 

$curauth = (isset($_GET['author_name'])) ? get_user_by('slug', $author_name) : get_userdata(intval($author));

?>

<div class="author-info">
	<h2 class="author-heading"><?php echo $curauth->nickname; ?></h2>
	<div class="author-description">
	</div><!-- .author-description -->
</div><!-- .author-info -->

<?php 

get_footer(); 

?>