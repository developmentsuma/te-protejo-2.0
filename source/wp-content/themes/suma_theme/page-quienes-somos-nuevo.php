<?php



/*Template Name: Quienes Somos nuevo*/ 



get_header('nuevo'); 



$count= 0;

$blog_id = get_current_blog_id();

$site_name = get_blog_details($blog_id)->blogname;





?>



<style>

    

    .img-limits{

        display: table;

        margin: auto;

        border: 1px dashed;

        overflow: hidden;

        border-radius: 50%;

        position: relative;

        /* height: 130px; */

        margin-bottom: 20px;

        border: 3px solid #fff;

        z-index: 2;

        width: 109px;

    }



    .s-carrusel .p-15{

        margin: 15px;

    }



    .download-section{

        border-top: 2px solid #d2d1cb;

        margin-top: 30px;

        padding-top: 30px;

    }



    .sponsor-title {



        font-size: 18px;

        text-align: center;

        color: #ABABA7;

        text-transform: uppercase;

        padding-bottom: 10px;



    }



    .donwload-link{

        color: #555555;

    }



    .s-carrusel .slick-track{

        padding-bottom: 30px;

    }



    .sponsor-box{

        padding: 15px 0px;

        background-color: white;

    }



    .picture-egg{

        border: none !important;

        height: 100% !important;

        max-width: 100% !important;

        width: 100% !important;



    }



    .a-objetives{

        color: #53BD96 !important;

        font-size: 20px !important;

    }



    .slick-track{

        margin: 0 !important;

    }

    h4#equipo{

        font-size: 30px;

        color:#53BD96;

    }

 

 



.c-newsletter form a {

    font-size: 25px;

    color: rgb(255 255 255);

    text-align: center;

    padding-right: 20px;

    padding-top: 10px;

    margin: 0px;

    display: block;

}



</style>

<?php



 



    if($_GET['lang']=='en'):

        $equipo = 'equipo_ingles';

    else:

        $equipo = 'equipo';

    endif;

    

    $titulo = '';

    $subtitulo = '';

    $titulo_quienes_somos = '';

    $titulo_mision_y_vision = '';

    $titulo_objetivos_principales = '';

    $titulo_equipo = '';

    $texto_quienes_somos = get_field('texto_quienes_somos');

    $mision_titulo = get_field('mision_titulo');

    $mision_descripcion = get_field('mision_descripcion');

    $vision_titulo = get_field('vision_titulo');

    $vision_descripcion = get_field('vision_descripcion');

    $objetivo_1_titulo = get_field('objetivo_1_titulo');

    $objetivo_1_descripcion = get_field('objetivo_1_descripcion');

    $texto_objetivos_principales = get_field('texto_objetivos_principales');

    $objetivo_2_titulo = get_field('objetivo_2_titulo');

    $objetivo_2_descripcion = get_field('objetivo_2_descripcion');

    $texto_objetivos_principales_2 = get_field('texto_objetivos_principales_2');

    $objetivo_3_titulo = get_field('objetivo_3_titulo');

    $objetivo_3_descripcion = get_field('objetivo_3_descripcion');

    $texto_objetivos_principales_3 = get_field('texto_objetivos_principales_3');

    $objetivo_4_titulo = get_field('objetivo_4_titulo');

    $objetivo_4_descripcion = get_field('objetivo_4_descripcion');

    $texto_objetivos_principales_4 = get_field('texto_objetivos_principales_4');

    $texto_link_youtube = get_field('texto_link_youtube');

    $link_memorias_y_estados_financieros = get_field('link_memorias_y_estados_financieros');

 



?>                        



    <div class="main" role="main">

        <div class="c-about">

            <div class="c-about__bg" style="background-image:url('<?php the_field('imagen_header'); ?>');"> </div>

            <div class="container">

                <div class="row"> 

                    <div class="col-md-12">

                        <!--titulos-->

                        <div class="c-about__top">

                            <div class="row">

                                <div class="col-12">

                                    <h1><?php if($titulo=='' && $_GET['lang']!='en'): tradgb('tenemos_una_mision','Tenemos una misión'); elseif($titulo=='' && $_GET['lang']=='en') : tradgb('tenemos_una_mision','We have a mission'); else: echo $titulo; endif; ?></h1>

                                    <h2><?php if($subtitulo=='' && $_GET['lang']!='en'): tradgb('conoce_personas_te_protejo','Conoce a las personas que trabajan en Te Protejo'); elseif($subtitulo=='' && $_GET['lang']=='en'): tradgb('conoce_personas_te_protejo','Get to know us'); else: echo $subtitulo; endif; ?></h2>

                                   

                                </div>

                            </div>

                        </div>



                        <!--information-->

                        <div class="c-about__information">

                            <div class="row">

                                <div class="col-lg-5">

                                    <h3><?php if($titulo_quienes_somos=='' && $_GET['lang']!='en'): tradgb('quienes_somos','Quiénes Somos'); elseif($titulo_quienes_somos=='' && $_GET['lang']=='en'): tradgb('quienes_somos','Who we are');  else: echo $titulo_quienes_somos; endif; ?></h3>

                                    <?php $texto_link=get_field('texto_link_youtube');?>

                                </div>

                                <div class="col-lg-7">

                                    <p><?php echo $texto_quienes_somos; ?></p>

                                </div>

                            </div>

                        </div>

                        <div class="c-about__objetives">

                            <h4><?php if($titulo_mision_y_vision=='' && $_GET['lang']!='en'): tradgb('mision_vision','MISIÓN Y VISIÓN'); elseif($titulo_mision_y_vision=='' && $_GET['lang']=='en'): tradgb('mision_vision','Mission and Vision'); else: echo $titulo_mision_y_vision; endif; ?></h4>

                            <div class="c-about__objetives-cnt">

                            <div class="c-about__objetives-subitem-dos">

                                    <div class="c-about__objetives-item">

                                        <div class="c-about__objetives-text">

                                            <h5 class="text-left"><?php echo $mision_titulo; ?></h5>

                                            <?php echo $mision_descripcion; ?>

                                        </div>

                                    </div> 

                                    <div class="c-about__objetives-item">

                                        <div class="c-about__objetives-text">

                                            <h5 class="text-left"><?php echo $vision_titulo;  ?></h5>

                                            <?php echo $vision_descripcion; ?>

                                        </div>

                                    </div>   

                                </div>

    

                            </div>

                        </div>



 

                    </div>

                </div>

            </div>

            <div class="c-about__staff">

                    <h4 id="equipo"><?php if($_GET['lang']!='en'): echo 'EQUIPO'; else: echo 'TEAM'; endif; ?></h4>

                </div>

                

            <?php



            $count=0; //reset variable

            $count2=0; //reset variable



            $arg_equipos = array(

                'posts_per_page' => -1,

                'post_type' => 'equipos',

                'order' => 'DESC', 

                'orderby' => 'date',

            );



            $the_query = new WP_Query( $arg_equipos );



            // The Loop

            while ( $the_query->have_posts() ) : $the_query->the_post();

                

                global $post;

                $mostrar_carrusel = get_field('mostrar_carrusel');

 

                

                ?>

               <?php if($mostrar_carrusel == 1){ ?>

                <div class="c-about__staff">

                    <!-- <h4><?php if($count2!=2) : if($_GET['lang']!='en'): echo the_title(); else: echo get_field('titulo_equipo_ingles'); endif; endif; ?></h4> -->

                    <h4><?php  if($_GET['lang']!='en'): echo the_title(); else: echo get_field('titulo_equipo_ingles'); endif;  ?></h4>

               

                    <div class="row justify-content-center <?php switch ($count2) {

                                                                    case 0:

                                                                        echo 'pn1-col';

                                                                        break;

                                                                    case 1:

                                                                        echo 'pn2-col';

                                                                        break;

                                                                    case 2:

                                                                        echo 'pn3-col';

                                                                        break;

                                                                }; ?>">



                   

                        <?php



                      

                        

                        // check if the repeater field has rows of data

                        if( have_rows($equipo, $post->ID) ):



                            // loop through the rows of data

                            while ( have_rows($equipo, $post->ID) ) : the_row();



                                $fotografia_miembro = get_sub_field('fotografia');

                                $canal_youtube = get_sub_field('youtube');

                                $canal_linkedin = get_sub_field('linkedin');

                                //Siguientes líneas se obtendra objeto user de wp y la url de todos los post del user

                                $name_user = get_sub_field('nombre');

                                $user_wp = get_sub_field('vincular_usuario');

                                $instagram = get_sub_field('instagram');

                                $link_instagram = "https://www.instagram.com/".str_replace("@","", $instagram);



                                

                                ?>

                                 



                                 

                                <div class="col-md-6 col-xl-3 c-carrusel__item">

                                    <div class="c-carrusel__single">

                                        <div class="img-limits">

                                            <img class="picture-egg" src="<?php echo $fotografia_miembro['sizes']['thumbnail']; ?>">

                                        </div>

                                        <div class="w-100 d-table picture-name">

                                            <div class="d-table-cell align-middle">

                                                <h5><?php the_sub_field('nombre'); ?></h5>

                                            </div>

                                        </div>

                                        <p class="picture-job"><?php the_sub_field('cargo'); ?></p>



                                        <div class="description-container">

                                            <?php the_sub_field('descripcion'); ?>

                                        </div>



                                        <div class="contact-box-footer">



                                            <?php if($instagram){  ?>

                                                <a class="link-youtube" href="<?php echo $link_instagram;?>" tabindex="-1"><?php echo $instagram; ?></a>

                                            <?php  }  ?>

                                            

                                            <?php if($canal_youtube){  ?>

                                                <a class="link-youtube" href="<?php echo $canal_youtube;?>" tabindex="-1"><?php tradgb('td_canal_de_youtube',$texto_link); ?></a>

                                            <?php } ?>



                                            <?php if($canal_linkedin){  ?>

                                                <a class="link-youtube" href="<?php echo $canal_linkedin;?>" tabindex="-1"><?php tradgb('td_canal_de_linkedin','Linkedin'); ?></a>

                                            <?php } ?>



                                            <?php if($user_wp['ID'] != '1' && $user_wp['ID'] != null){  ?>



                                                <a class="btn-see" href="<?php echo get_home_url(); ?>/archive-posts/?autor=<?php echo $user_wp['ID']; ?>"><?php tradgb('ver_todo_sus','Ver todos sus'); ?> posts</a>



                                            <?php } ?>



                                        </div>

                                        

                                    </div>

                                </div>

                               

                                <?php

                            

                            endwhile;



                        else :



                        // no rows found



                        endif;



                        ?>



                    </div>

                    </div>

                </div>

                <?php } ?>

                <?php



                $count++;

                $count2++;



                if($count == 3){



                    $count = 0; //reset var



                }



            endwhile;



            wp_reset_query();



            ?>



          


            <div class="c-newsletter">

                <div class="container">

                    <!-- <h4><?php  tradgb('suscribete_al_newsletter','Suscríbete al newsletter');?></h4> -->

                    <form>

                        <div class="row">

                            <div class="col-md-12">

                                <a href="<?php echo $link_memorias_y_estados_financieros; ?>"><?php if($_GET['lang']=='en'): tradgb('revisa_memorias','Check our work and financial statements here.'); else  : tradgb('revisa_memorias','Revisa nuestras memorias y estados financieros'); endif;?></a>

                            </div>

                            <!-- <div class="col-md-6">

                                <div class="c-newsletter__inputs">

                                   <input id="email_newsletter" placeholder="<?php  tradgb('escribe_tu_email','Escribe tu email');?>" type="text">

                                    <input type="hidden" id="blog_origen_newsletter" value="<?php echo $site_name; ?>"> 

                                    <a href="#"id="newsletter"><?php  tradgb('suscribete','Suscríbete');?></a> 

                                </div>

                            </div> -->

                        </div>

                    </form>

                </div>



                <!-- <div class="d-block download-section">



                    <div class="container">

                        <div class="row">

                             <?php

                               $estado_financiero=get_field('estado_financiero');

                               $memorias_anuales=get_field('memorias_anuales');

                               ?>

                            <a href="<?php echo $estado_financiero['url']; ?>" class="col-6 col-md-3 col-xl-2 text-capitalize donwload-link">

                                <i class="fa fa-download"></i>

                                <?php  tradgb('td_estado_financiero','Estado financiero');?>

                            </a>

                            <a href="<?php echo $memorias_anuales['url']; ?>" class="col-6 col-md-3 col-xl-2 text-capitalize donwload-link">

                                <i class="fa fa-download"></i>

                                <?php  tradgb('td_memorias_anuales','memorias anuales');?> 

                            </a>

                        </div>

                    </div>

                </div> -->

            </div>

        </div>

    </div>

    

<?php 



get_footer(); 



?>