<?php
/**
 * The Template for displaying all single posts.
 *
 * @package WordPress
 * @subpackage Twenty_Ten
 * @since Twenty Ten 1.0
 */

get_header('nuevo'); 

?>

<style>
	
	.container-img-blog{
		width: 100%;
    	text-align: center;
	}

	.container-img-blog img{
		margin: 0 auto;
		margin-botom: 30px;
	}

	p img {
		display: table;
		margin: auto;
	}

</style>

    <div class="main" role="main">
        <div class="c-blog">
            <div class="c-blog__bg">
                <div class="c-blog__bg2">
                </div>
            </div>
            <div class="container">
                <div class="row justify-content-md-center"> 
                    <div class="col-xl-10">
                        <div class="c-blog__top">
                            <div class="row">
                                <div class="col-6">
									<a href="<?php echo get_home_url(); ?>/blog-cruelty-free/"><i class="fas fa-arrow-left"></i> <?php  tradgb('volver_al_blog','Volver al blog');	?></a>
                                </div>
                                <div class="col-6 text-right"> 

									<?php

										$next_post_id = get_next_post()->ID;
										$prev_post_id = get_previous_post()->ID;

										if($next_post_id != ''){

											?>

											<a href="<?php the_permalink($next_post_id); ?>"><?php  tradgb('siguiente_post','Siguiente post');	?><i class="fas fa-arrow-right"></i></a>

											<?php

										}else{

											?>

											<a href="<?php the_permalink($prev_post_id); ?>"><i class="fas fa-arrow-left"></i><?php  tradgb('post_anterior','Post anterior');	?></a>

											<?php
										}
									
									?>
                                    
                                </div>
                            </div>
                        </div>
                        <div class="c-blog__single">
                            <div class="c-blog__single-info">

								<?php

								$post_type = $post->post_type;
								$post_type_object = get_post_type_object( $post_type );
								$img_post = get_the_post_thumbnail($id_post, 'full');
								$img_post = preg_replace( '/(height)=\"\d*\"\s/', "", $img_post );
								$tags = get_the_tags($id_post); 
								$id_post = $post->ID;
								$tipo_post = $post_type_object->labels->name;								
								

								?>
								
                                <span><strong><?php  tradgb($post_type_object->name,$post_type_object->name);	?></strong></span>
                                <span>|</span><small><?php tradgb('publicado_el','Publicado el'); echo ' '.get_the_date('d-m-Y'); ?></small>
                            </div>
							<h1><?php the_title(); ?></h1>
										<div class="container-img-blog">
								<?php echo $img_post; ?>
							</div>
          
                            <!-- <img src="<?php echo $img_post; ?>"> -->
                            <div class="c-blog__tag">

								<?php
								
								if ($tags) {
								
									?>
									
									<i class="fas fa-tag"> </i>
									
									<?php

									foreach ($tags as $tag) {
																	
										?>

										<a href="<?php echo get_home_url(); ?>/archive-posts/?tag=<?php echo $tag->slug; ?>"><?php echo $tag->name;  ?></a>

										<?php

									}
								
								}

								?>
								
                            </div>
                            <div class="c-blog__resume">
                                <p><?php the_field('extracto'); ?></p>
                            </div>
                            <div class="c-blog__main">
                                <p><?php echo do_shortcode(get_post_field('post_content', $postid)); ?></p>
                            </div>
                        </div>
                        <div class="c-blog__share">
                            <p><?php  tradgb('comparte','Comparte');?>:</p>
	
							<!-- Your share button code -->
							<div class="fb-share-button" data-href="<?php the_permalink(); ?>" data-layout="button" data-size="large" data-mobile-iframe="true">
							</div>
	
							<a id="copiar_url" class="btn-copy" href="#"> <i class="fas fa-copy"></i>
							<?php  tradgb('copiar_url','Copiar URL');?>
							</a>

							<a class="copiar_url" style="float:right" href="<?php echo get_home_url(); ?>/archive-posts/?autor=<?php echo $post->post_author; ?>">
								 <?php tradgb('mas_posts_de','Más posts de'); echo' ';the_author_meta('nickname', $post->post_author); ?>
							</a>

							<div class="c-blog__main">

								<div class="row w-100 m-auto">
								<div id="comentarios-form" class="col-md-8 offset-md-2">
								
								
								<?php 
									//$txt_nombre = tradgb("td_nombre","nombre");
									comment_form( array(
									'id_form'           => 'form_comentario',
									'id_submit'         => 'btn-enviar',
									'title_reply' => tradgbp("td_agregar_un_comentario","AGREGAR UN COMENTARIO"),
									'label_submit' => tradgbp("td_publicar_comentario","Publicar comentario"),
									'comment_notes_after' => '',
									'comment_notes_before' => '',
									'comment_field' =>  '<p class="campo comentario-text-area"><label for="comment">' . tradgbp("td_comentario",_x( 'Comment', 'noun' )) .
										'</label><textarea id="comment" name="comment" class="" aria-required="true">' .
										'</textarea></p>',
									'fields' => array(

									'author' =>
										'<p class="campo comentario-text-input"><label for="nombre_comentario">' . __( tradgbp("td_nombre","nombre"), 'domainreference' ) . '</label> ' .
										( $req ? '<span class="required">*</span>' : '' ) .
										'<input id="author" name="author" type="text" value="' . esc_attr( $commenter['comment_author'] ) .
										'" size="30"' . $aria_req . ' /></p>',

									'email' =>
										'<p class="campo comentario-text-input"><label for="email">' . __( 'Email:', 'domainreference' ) . '</label> ' .
										( $req ? '<span class="required">*</span>' : '' ) .
										'<input id="email" name="email" type="text" value="' . esc_attr(  $commenter['comment_author_email'] ) .
										'" size="30"' . $aria_req . ' /></p>',

									)), $post->ID ); 
								?>	
								<!--form id="form_comentario">
									<p class="campo"><label for="nombre_comentario">Nombre</label> <input id="nombre_comentario" /></p>
									<p class="campo"><label for="correo_comentario">Correo</label> <input id="correo_comentario" /></p>
									<p class="campo"><label for="mensaje_comentario">Mensaje</label> <textarea id="mensaje_comentario"></textarea></p>
									<p id="btn-enviar"><a href="" title="Enviar">Enviar</a></p>
								</form-->
								<p id="accedes"><?php  tradgb('texto_newsletter','Al enviar tus consultas o comentarios estás accediendo a recibir nuestro newsletter de forma mensual.');?></p>
							</div>
							</div>
								
								<?php

								$comments = get_comments(array ( 'post_id' => $id_post ));

								foreach($comments as $comment) : 
									?>
									
									<div class="comentario">
										<div class="globo"></div>
										<p class="datos-comentario"><span><?php echo($comment->comment_author);?></span> <?php $comment->comment_date; ?> <?php tradgb('td_dice','Dice'); ?>:</p>
										<p><?php echo($comment->comment_content);?></p>
									</div>
									
									<?php 

								endforeach;
								
								?>

                            </div>

                        </div>
                    </div>
                </div>
                <div class="row c-blog__related">
                    <div class="col-md-12">
                        <h2><?php tradgb($tipo_post,$tipo_post); ?></h2>
                        <a class="btn-all" href="<?php echo get_home_url(); ?>/archive-posts/?post_type=<?php echo $tipo_post; ?>"><?php  tradgb('ver_todos','Ver Todos');?></a>
					</div>
					
					<?php

					$arg_post = array(
						'posts_per_page' => 2,
						'post_type' => $tipo_post,
						'post__not_in' => array($id_post),
						'order' => 'DESC',
						'orderby' => 'date',
					);
					
					// The Query
					$the_query = new WP_Query( $arg_post );

					// The Loop
					while ( $the_query->have_posts() ) : $the_query->the_post();

						$post_type = $post->post_type;
						$post_type_object = get_post_type_object( $post_type );
						$img_post = get_the_post_thumbnail_url( null, 'img_front_blog' );

						?>

						<div class="col-xl-6">
							<div class="c-blog__related-item">
								<div class="row">
									<div class="col-xl-6 col-lg-5 col-md-5">
										<a href="<?php the_permalink(); ?>">
											<img src="<?php echo $img_post; ?>">
										</a>
									</div>
									<div class="col-md-6"> 
										<span><?php tradgb($post_type_object->name,$post_type_object->name); ?></span>
										<h3><?php the_title(); ?></h3>
										<p><?php echo get_excerpt(80); ?></p>
										<a href="<?php the_permalink(); ?>"> <?php tradgb('ver','Ver');?> <?php echo ' ' . tradgb($post_type_object->labels->singular_name,$post_type_object->labels->singular_name); ?></a>
									</div>
								</div>
							</div>
						</div>

						<?php

					endwhile;

					// Reset Query
					wp_reset_postdata();

					?>
                    
                </div>
            </div>
            <div class="c-newsletter">
                <div class="container">
                    <h4><?php  tradgb('suscribete_al_newsletter','Suscríbete al newsletter');	?></h4>
                    <form>
                        <div class="row">
                            <div class="col-md-6">
                                <p><?php  tradgb('quieres_recibir_nuestras_noticias','¿Quieres recibir nuestras noticias?');?></p>
                            </div>
                            <div class="col-md-6">
                                <div class="c-newsletter__inputs">
                                    <input id="email_newsletter" placeholder="<?php  tradgb('escribe_tu_email','Escribe tu email');?>" type="text">
                                    <button id="newsletter"><?php  tradgb('suscribete','Suscríbete');?></button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

		
<?php 

get_footer(); 

?>
