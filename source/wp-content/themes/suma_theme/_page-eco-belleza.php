<?php

/*Template Name: Feria EcoBelleza*/ 

get_header(); 

?>

    <div class="main" role="main">
        <div class="c-blog">
            <div class="c-blog__bg c-blog__bg--eco">
                <div class="c-blog__bg2 c-blog__bg--eco2"></div>
            </div>
            <div class="container">
                <div class="row justify-content-md-center">
                    <div class="col-xl-10">
                        <div class="c-blog__top">
                            <div class="row">
                                <div class="col-12">
                                    <h1>Feria Eco Belleza</h1>
                                </div>
                                <div class="col-6">
                                    <a href="<?php echo get_home_url(); ?>/blog-cruelty-free/"> <i class="fas fa-arrow-left"></i>Volver al blog </a>
                                </div>
                            </div>
                        </div>
                        <div class="c-blog__important">
                            <div class="row">

                                <?php 

                                $posts = get_field('post_destacado');
                                $id_posts_slider = array();

                                if( $posts ):
                                    
                                    foreach( $posts as $post): // variable must be called $post (IMPORTANT) 
                                    
                                        setup_postdata($post); 

                                        $post_type = $post->post_type;
                                        $post_type_object = get_post_type_object( $post_type );
                                        $img_post = get_the_post_thumbnail_url( null, 'img_slider_posts' );
                                        $id_post = $post->ID;

                                        ?>

                                        <div class="col-lg-6">
                                            <img src="<?php echo $img_post; ?>">
                                        </div>
                                        <div class="col-lg-6">
                                            <div class="c-blog__important-txt">
                                                <span><?php echo $post_type_object->labels->name; ?></span>
                                                <h3><?php the_title(); ?></h3>
                                                <p><?php the_excerpt(); ?></p>
                                                <a href="<?php the_permalink(); ?>">Ver <?php echo $post_type_object->labels->singular_name; ?></a>
                                            </div>
                                        </div>

                                        <?php 

                                    endforeach;
                                    
                                    wp_reset_postdata(); // IMPORTANT - reset the $post object so the rest of the page works correctly 
                                    
                                endif; 

                                ?>
                                
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row c-blog__articles">
                    <div id="articulos" class="col-md-12">
                        <h2>Nuestros articulos</h2>
                    </div>

                    <?php

                    $paged = (get_query_var('paged')) ? get_query_var('paged') : 1;

                    $arg_eco_bellezas = array(
                        'posts_per_page' => 8,
                        'post_type' => array('noticias', 'articulos'),
                        'order' => 'DESC', 
                        'orderby' => 'date',
                        'paged' => $paged,
                    );

                    // The Query
                    $the_query = new WP_Query( $arg_eco_bellezas );

                    // The Loop
                    while ( $the_query->have_posts() ) : $the_query->the_post();

                        //pr($post);
                        $post_type = $post->post_type;
                        $post_type_object = get_post_type_object( $post_type );
                        $img_post = get_the_post_thumbnail_url( null, 'img_front_blog' );

                    ?>

                        <div class="col-lg-3 col-md-6">
                            <div class="c-blog__articles-item"> 
                                <img src="<?php echo $img_post; ?>">
                                <div class="c-blog__articles-item-text">
                                    <span><?php echo $post_type_object->name ?></span>
                                    <h3><?php the_title(); ?></h3>
                                    <p><?php the_excerpt(); ?></p>
                                    <a href="<?php the_permalink(); ?>">Ver <?php echo $post_type_object->labels->singular_name; ?></a>
                                </div>
                            </div>
                        </div>

                    <?php

                    endwhile;

                    ?>

                    <div class="col-md-12">
                        
                        <?php kriesi_pagination($the_query->max_num_pages); ?>

                    </div>
                    
                    <?php

                    // Reset Query
                    wp_reset_query();

                    ?>
                    
                </div>
                <div class="c-eco_fair"><img src="<?php bloginfo( 'template_url' ); ?>/img/cnt-logo-eco.png">
                    <div>
                        <a class="btn btn-primary" href="">Ir a feria eco belleza</a>
                    </div>
                </div>
            </div>
            <div class="c-newsletter">
                <div class="container">
                    <h4>Suscríbete al newsletter</h4>
                    <form>
                        <div class="row">
                            <div class="col-md-6">
                                <p>¿Quieres recibir nuestras noticias?</p>
                            </div>
                            <div class="col-md-6">
                                <div class="c-newsletter__inputs">
                                    <input id="email_newsletter" type="text" placeholder="Escribe tu email">
                                    <button id="newsletter">Suscríbete</button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    
<?php 

get_footer(); 

?>