var fin_posts = 0;
var trigger = 1;
var pathname = window.location.pathname;


jQuery(window).scroll(function() {
    var validaroArchive = pathname.indexOf("/archive-posts/");
    var buscarArchive = pathname.indexOf("/buscar-posts/");

    if(pathname == '/archive-posts/' || pathname == '/buscar-posts/' || validaroArchive != -1 || buscarArchive != -1 ){

        var windowFooter = jQuery(window).scrollTop() + jQuery(window).height();
        var positionFooter =  jQuery('#hook-scroll').offset().top - 200;
        console.log('param '+pathname);
        // console.log(windowFooter, ' <<< >>>' ,  jQuery('#posts_originales').offset().top+jQuery('#posts_originales').height());

        if((windowFooter >= positionFooter) && trigger == pagina_actual) {

            if(pagina_actual >= 1){

                pagina_actual++;

            }

            if(pagina_actual < 1){

                pagina_actual = 1;

            }


            if(query_busqueda.length != 0){

                query_paginador = query_busqueda;

            }else{

                query_paginador = query_inicial;

            }

            if(fin_posts != 1){

                jQuery.ajax({

                    url:ajax_wp.ajaxurl,
                    type: "POST",
                    data: {
                        pagina_actual: pagina_actual,
                        query_busqueda: query_paginador,
                        action: "ajax_paginador_archive",
                    },
                    async: true,
                    cache: false,
                    dataType: "json",
                    // beforeSend:
                    //     function () {
                
                    //         jQuery('#posts_ajax').fadeOut(200, 'swing').html('');
                    //         jQuery('#posts_originales').fadeOut(500, 'swing');
                            
                    //     },
                
                    success: function (response) {
                        
                        if(response.posts != 0){
                
                            response.posts.forEach(function(resultado){
                
                                jQuery('#posts_originales').append(resultado).fadeIn(500, 'swing');
                
                            });
                
                            trigger++;
                
                        }
                        else{
                
                            // jQuery('#posts_ajax').append('<div class="row"><div class="col-12 col-md-10"><div class="post-panel"><h5 class="text-primary">No se han encontrado marcas</h5></div></div></div>').fadeIn(500, 'swing');
                            
                            fin_posts = 1;

                            jQuery('#response_message').append('<div class="col-12 text-center"><h5 class="text-primary">No existen más posts</h5></div>').fadeIn(500, 'swing');
                
                        }
                        
                    },
                
                    error: function (error) {
                        
                        console.log(error);
                
                    },
        
                });

            }

            
        }

    }
     
});