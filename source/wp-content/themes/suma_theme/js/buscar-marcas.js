query_busqueda = new Array();

//si presionan enter sobre el input de busqueda
jQuery('#buscador_marcas').on('keyup', function(e) {

    if (e.keyCode == 13) {

        if (jQuery(this).val().trim() != '') {

            ajax_buscar_marcas(jQuery(this).val());

        }

    }
    /*else {

           return false;

       }*/

});

//si presionan sobre el botón lupa del input de busqueda
jQuery('#btn_buscador_marcas').click(function() {

    var tag_busqueda = jQuery('#buscador_marcas').val();

    if (tag_busqueda.trim() != '') {

        ajax_buscar_marcas(tag_busqueda);

    } else {

        return false;

    }

});

function ajax_buscar_marcas(busqueda) {

    //reset de los pool_ids de los filtros
    pool_ids_categorias = [];
    pool_ids_respaldos = [];
    pool_ids_puntos_venta = [];
    pool_ids_vegano = [];

    //reset pagina actual porque al buscar queda de cero
    pagina_actual = 1;

    jQuery('input[type="checkbox"]').prop('checked', false); //reset a los checkbox filtros

    jQuery.ajax({
        url: ajax_wp.ajaxurl,
        type: "POST",
        data: {
            var_busqueda: busqueda,
            total_post: total_posts,
            cantidad_post_mostrados: cantidad_post_mostrados,
            action: "ajax_buscar",
        },
        async: true,
        cache: false,
        dataType: "json",
        // beforeSend:
        //     function () {

        //         jQuery('#posts_ajax').fadeOut(200, 'swing').html('');
        //         jQuery('#posts_originales').fadeOut(500, 'swing');

        //     },

        success: function(response) {

            console.log(response);

            var_busqueda = response.var_busqueda;

            if (response.posts != 0) {

                jQuery('#posts_originales').show();
                jQuery('#not-found-msg').remove();
                query_busqueda = response.query_object.query;

                response.posts.forEach(function(resultado) {

                    jQuery('#posts_originales').html('')
                        .append(resultado)
                        .fadeIn(500, 'swing');

                });

            } else {

                jQuery('#posts_originales').hide();
                jQuery('#not-found-msg').remove()
                jQuery('#response_message').append('<div class="row" id="not-found-msg"><div class="col-12 col-md-10"><div class="post-panel"><h5 class="text-primary ">No se han encontrado marcas</h5></div></div></div>').fadeIn(500, 'swing');

            }

        },

        error: function(error) {

            console.log(error);

        }
    });

}