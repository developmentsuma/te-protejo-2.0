function validarEmail(mail) {
    var exr = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
    return exr.test(mail);
}

$('#newsletter').click(function () {

    event.preventDefault();

    var email_newsletter = $('#email_newsletter').val();
    var blog_origen_newsletter = $('#blog_origen_newsletter').val();
    console.log(blog_origen_newsletter);

    if (!email_newsletter.trim()) {
        $('#email_newsletter').val('');
        $('#email_newsletter').attr('placeholder', 'Ingrese su email por favor');
        $('#email_newsletter').addClass('input_error');
        return false;
    } else if (validarEmail(email_newsletter) == false) {
        $('#email_newsletter').val('');
        $('#email_newsletter').attr('placeholder', 'Ingrese un email válido por favor');
        $('#email_newsletter').addClass('input_error');
        return false;
    } else {
        $('#email_newsletter').removeClass('input_error');
    }

    $.ajax({
        url: $('meta[name=template-url]').attr("content") + "/ajax/guardado-newsletter.php",
        type: "POST",
        data: {
            email_newsletter: email_newsletter,
            blog_origen_newsletter: blog_origen_newsletter,
        },
        async: true,
        cache: false,
        success: function (response) {

            if (response == 'ok') {

                $('#email_newsletter').addClass('input_success')
                    .val('')
                    .attr('placeholder', 'Email registrado de forma exitosa!');

                console.log(response);
            }

        },
        error: function (response) {

            $('#email_newsletter').addClass('input_error')
                .val('')
                .attr('placeholder', 'Tu email no ha sido registrado, favor intenta más tarde');

            console.log(response);
        }
    });
})