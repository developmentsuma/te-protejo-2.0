$('#copiar_url').click(function(){

    event.preventDefault();

    var aux = document.createElement("input");
    aux.setAttribute("value",window.location.href);
    document.body.appendChild(aux);
    aux.select();
    document.execCommand("copy");
    document.body.removeChild(aux);

    var tooltip = document.getElementById("copiar_url");
    tooltip.innerHTML = '<i class="fas fa-copy"></i> URL copiada';

});