jQuery(document).ready(function(){

    total_paginas = Math.ceil(total_posts/cantidad_post_mostrados); //

    // init bootpag
    jQuery('#page-selection').bootpag({
        total: total_paginas,
        maxVisible: 3,
        leaps: false,
        firstLastUse: true,
        first: '<<',
        last: '>>',
        next: '>',
        prev: '<',
    }).on("page", function(event, /* page number here */ num){
        
        pagina_actual = num;

        if(query_busqueda.length != 0){

            query_paginador = query_busqueda;

        }else{

            query_paginador = query_inicial;

        }

        jQuery.ajax({

            url:ajax_wp.ajaxurl,
            type: "POST",
            data: {
                pagina_actual: pagina_actual,
                query_busqueda: query_paginador,
                var_busqueda: var_busqueda,
                cantidad_post_mostrados: cantidad_post_mostrados,
                action: "ajax_paginador",
            },
            async: true,
            cache: false,
            dataType: "json",
            beforeSend:
                function () {
        
                    jQuery('#posts_ajax').fadeOut(200, 'swing').html('');
                    jQuery('#posts_originales').fadeOut(500, 'swing');
                    
                },
        
            success: function (response) {
                
                if(response.posts != 0){
        
                    response.posts.forEach(function(resultado){
        
                        jQuery('#posts_ajax').append(resultado).fadeIn(500, 'swing');
        
                    });
        
                    jQuery('#posts_ajax').append(response.paginador).fadeIn(500, 'swing');
        
                }
                else{
        
                    jQuery('#posts_ajax').append('<div class="row"><div class="col-12 col-md-10"><div class="post-panel"><h5 class="text-primary">No se han encontrado marcas</h5></div></div></div>').fadeIn(500, 'swing');
        
                }
                
            },
        
            error: function (error) {
                
                console.log(error);
        
            },

        });

    });

});