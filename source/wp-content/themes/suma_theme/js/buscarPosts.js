var busqueda;

//si presionan enter sobre el input de busqueda
$('#buscar_posts').on('keyup', function (e) {

    if (e.keyCode == 13) {

        if ($(this).val().trim() != '') {
            window.open($('meta[name=home-url]').attr("content") + "/buscar-posts/?busqueda=" + $(this).val(), "_self");
        }

    }
    else {

        return false;

    }

});

//si presionan sobre el botón lupa del input de busqueda
$('#btn_buscar_posts').click(function () {

    busqueda = $('#buscar_posts').val().trim();

    if (busqueda != '') {

        window.open($('meta[name=home-url]').attr("content") + "/buscar-posts/?busqueda=" + busqueda, "_self");

    }
    else {

        return false;

    }

});

$('#btn_buscar_posts_sec').click(function () {

    busqueda = $('#buscar_posts_sec').val().trim();
    postype  = $('#post_type').val().trim();
    //var postype = document.getElementById("evenEmailing_code").value;

    if (busqueda != '') {

        window.open($('meta[name=home-url]').attr("content") + "/archive-posts/?post_type="+postype+"&busqueda=" + busqueda, "_self");

    }
    else {

        return false;

    }

});