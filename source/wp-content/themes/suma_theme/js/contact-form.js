function validarEmail(mail) {
    var exr = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
    return exr.test(mail);
}

$('#contact_form').click(function() {

    event.preventDefault();

    var name;
    let nametd;
    var email;
    let emailtd;
    let emailvtd;
    var motive;
    var message;
    let messagetd;
    var blog_origen;
    var country;

    name = $('#name').val();
    email = $('#email').val();
    country = $('#country').val();
    motive = $('#motive').val();
    message = $('#message').val();
    blog_origen = $('#blog_origen').val();
    nametd = $('#name').data('rel');
    emailtd = $('#email').data('rel');
    emailvtd = $('#email').data('info');
    messagetd = $('#message').data('rel');


    if (!name.trim()) {
        $('#name').val('');
        var td = trangb(nametd, 'Ingrese su nombre por favor');
        console.log(td);
        $('#name').attr('placeholder', td);
        $('#name').addClass('input_error');
        return false;
    } else {
        $('#name').removeClass('input_error');
    }


    if (!email.trim()) {
        $('#email').val('');
        var td = trangb(emailtd, 'Ingrese su nombre por favor');
        console.log(td);
        $('#email').attr('placeholder', td);
        $('#email').addClass('input_error');
        return false;
    } else if (validarEmail(email) == false) {

        $('#email').val('');
        var td = trangb(emailvtd, 'Ingrese un email válido por favor');
        $('#email').attr('placeholder', td);
        $('#email').addClass('input_error');
        return false;
    } else {
        $('#email').removeClass('input_error');
    }
    if (!country.trim()) {
        $('#country').val('');
        $('.selectize-input').addClass('input_error');
        return false;
    } else {
        $('.selectize-input').removeClass('input_error');
    }

    if (!message.trim()) {
        $('#message').val('');
        var td = trangb(messagetd, 'Ingrese su nombre por favor');
        console.log(messagetd);
        $('#message').attr('placeholder', td);
        $('#message').addClass('input_error');
        return false;
    } else {
        $('#message').removeClass('input_error');
    }

    $.ajax({
        url: $('meta[name=template-url]').attr("content") + "/ajax/envio.php",
        type: "POST",
        data: {
            name: name,
            email: email,
            motive: motive,
            country: country,
            message: message,
            blog_origen: blog_origen,
        },
        async: true,
        cache: false,
        success: function(response) {
            if (response) {

                $('#contact_form').fadeOut('slow', function() {

                    $('#contact_response').append('<h3 class="text-center">Mensaje enviado con éxito</h3><p class="text-center">Serás contactado a la brevedad</p>');
                    $('#contact_response').fadeIn('slow');
                    $('#contact_response').append("<script> gtag('config', 'AW-629692403'); </script>");
                });
                console.log(response);
            }

        },
        error: function(response) {
            $('#contact_form').fadeOut('slow', function() {
                $('#contact_response').append('<h3 class="text-center">¡Error en el envío!</h3><p class="text-center">Favor envíanos un correo a <a href="mailto:info@teprotejo.cl">info@teprotejo.cl</a></p>');
                $('#contact_response').fadeIn('slow');
            });
            console.log(response);
        }
    });
})

function trangb(field, traduccion) {
    if (field) {
        return field;
    } else {
        return traduccion;
    }

}