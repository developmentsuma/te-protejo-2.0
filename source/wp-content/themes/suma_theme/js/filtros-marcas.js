pool_ids_categorias = new Array();
pool_ids_respaldos = new Array();
pool_ids_puntos_venta = new Array();
pool_ids_vegano = new Array();

jQuery('input[type="checkbox"]').click(function() {

    pagina_actual = 1;
    fin_posts = 0;
    trigger = 1

    var id_term = jQuery(this).val(); //obtiene el valor del id del termino de la categoria wp
    var father_term = jQuery(this).data('father'); //indica de que filtro viene el valor

    switch (father_term) {
        case 'empresas_extranjeras_segmento':

            if (jQuery.inArray(id_term, pool_ids_categorias) == -1) { //evaluo si existe en el array el id seleccionado

                pool_ids_categorias.push(id_term); //si no existe le push al array

            } else {

                pool_ids_categorias = jQuery.grep(pool_ids_categorias, function(val) {

                    return val != id_term;

                }); //si existe le quito del array

            }

            break;

        case 'empresas_extranjeras_respaldo':

            if (jQuery.inArray(id_term, pool_ids_respaldos) == -1) {

                pool_ids_respaldos.push(id_term);

            } else {

                pool_ids_respaldos.splice(jQuery.inArray(id_term, pool_ids_respaldos), 1);

            }

            break;

        case 'empresas_extranjeras_venta':

            if (jQuery.inArray(id_term, pool_ids_puntos_venta) == -1) {

                pool_ids_puntos_venta.push(id_term);

            } else {

                pool_ids_puntos_venta.splice(jQuery.inArray(id_term, pool_ids_puntos_venta), 1);

            }

            break;

        case 'empresas_extranjeras_porcentaje':

            if (jQuery.inArray(id_term, pool_ids_vegano) == -1) {

                pool_ids_vegano.push(id_term);

            } else {

                pool_ids_vegano.splice(jQuery.inArray(id_term, pool_ids_vegano), 1);

            }

            break;
    }



    var tag_busqueda = jQuery('#buscador_marcas').val();

    if (tag_busqueda.trim() == '') {

        var_busqueda = '';

    }


    jQuery.ajax({
        url: ajax_wp.ajaxurl,
        type: "POST",
        data: {
            pagina_actual: pagina_actual,
            pool_ids_categorias: pool_ids_categorias,
            pool_ids_puntos_venta: pool_ids_puntos_venta,
            pool_ids_respaldos: pool_ids_respaldos,
            pool_ids_vegano: pool_ids_vegano,
            query_busqueda: query_busqueda, //viene desde el js buscar_marcas donde se setea query inicial para los filtros
            total_post: total_posts,
            cantidad_post_mostrados: cantidad_post_mostrados,
            var_busqueda: var_busqueda,
            action: "ajax_filtros",
        },
        async: true,
        cache: false,
        dataType: "json",
        beforeSend: function() {

            jQuery('#response_message').fadeOut(200, 'swing').html('');
            jQuery('#posts_originales').fadeOut(500, 'swing').html('');
            jQuery('#c-filter').addClass('no-pointer');
            jQuery('.cdx').hide();

        },

        success: function(response) {

            console.log(response);

            jQuery('#c-filter').removeClass('no-pointer');

            query_busqueda = response.query_object.query;

            if (response.posts != 0) {

                response.posts.forEach(function(resultado) {

                    jQuery('#posts_originales').append(resultado)
                        .fadeIn(500, 'swing');

                });

            } else {

                jQuery('#not-found-msg').remove();
                jQuery('#response_message').append('<div class="row" id="not-found-msg"><div class="col-12 col-md-10"><div class="post-panel"><h5 class="text-primary">No se han encontrado marcas</h5></div></div></div>').fadeIn(500, 'swing');

            }

        },

        error: function(error) {

            console.log(error);

        }
    });

})