<?php

/*Template Name: Archive Posts y Tags*/ 

get_header('nuevo'); 

$post_type = $_GET['post_type'];
$tag = $_GET['tag'];
$autor_id = $_GET['autor'];
$busqueda = $_GET['busqueda'];

if($autor_id != ''){

    $nombre_autor = get_the_author_meta('nickname', $autor_id);
    
}

$blog_id = get_current_blog_id();
$site_name = get_blog_details($blog_id)->blogname;


?>

<style>

    .background-archive{
        background-image: url("<?php bloginfo( 'template_url' ); ?>/img/header-archive.png") !important;
        background-position-y: bottom !important;
        background-repeat: no-repeat !important;
        background-size: contain !important;
    }

</style>

    <div class="main" role="main">
        <div class="c-blog">
            <div class="c-blog__bg c-blog__bg--eco background-archive">
                <!-- <div class="c-blog__bg2 c-blog__bg--eco2"></div> -->
            </div>
            <div class="container">
                <div class="row justify-content-md-center">
                    <div class="col-xl-10">
                        <div class="c-blog__top" style="padding-bottom:350px">
                            <div class="row">
                                <div class="col-12">
                                    
                                    <?php

                                    if($busqueda != ''){
                                        ?>
                                            <h1 class = "col-sm-12 col-md-9"><?php  tradgb('busqueda','Búsqueda');?>: <?php echo $busqueda; ?></h1>
                                        <?php
                                    }
                                   else if($post_type != '' && $busqueda == '' ) {

                                        ?>

                                            <h1><?php  tradgb('resultado_de','Resultado de');?> <?php echo $post_type; ?></h1>

                                        <?php

                                    } else if($tag != '') {

                                        ?>

                                        <h1><?php  tradgb('resultado_del_tag','Resultado del tag');?> <?php echo $tag; ?></h1>

                                        <?php

                                    } else if($autor_id != '') {

                                        ?>

                                        <h1><?php  tradgb('posts_de','Posts de');?>  <?php echo $nombre_autor; ?></h1>

                                        <?php
                                    }

                                    ?>

                                    
                                </div>
                                <?php if($post_type != ''){ ?>
                                <div class="c-blog__search">
                                    <input id="buscar_posts_sec" type="text" placeholder="<?php  tradgb('buscas_algo_en_especial','¿Buscas algo en especial?');?>...">
                                    <button id="btn_buscar_posts_sec"> <i class="fas fa-search"></i></button>
                                    <input id="post_type" value = "<?php echo $post_type; ?>" hidden>
                                </div>
                                <?php } ?>
                                <div class="col-6">
                                    <a href="<?php echo get_home_url(); ?>/blog-cruelty-free/"> <i class="fas fa-arrow-left"></i><?php  tradgb('volver_al_blog','Volver al blog');?> </a>
                                </div>
                            </div>
                        </div>
                        <div class="c-blog__important" style="background: ">    
                            <div class="row">
                                <!-- <div class="col-lg-6">
                                    <img src="<?php bloginfo( 'template_url' ); ?>/img/cnt-important.jpg">
                                </div>
                                <div class="col-lg-6">
                                    <div class="c-blog__important-txt">
                                        <span>Feria eco belleza</span>
                                        <h3>¡No te pierdas Feria Ecobelleza Concepción!</h3>
                                        <p>Te Protejo comienza una nueva campaña para llevar la vida cruelty free a toda Latinoamérica a través de Enlightaid.</p><a href="#">Ver artículo</a>
                                    </div>
                                </div> -->
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row c-blog__articles" id="posts_originales">
                    <!-- <div id="articulos" class="col-md-12">
                        <h2>Todos los post de <?php if($post_type != ''){ echo $post_type; }
                                                    if($tag != ''){ echo $tag; }
                                                    if($autor_id != ''){ echo $nombre_autor; } 
                                                    if($busqueda != ''){ echo $busqueda; } ?>?> </h2>
                    </div> -->

                    <?php

                    $paged = 1;
                    if($busqueda != ''){

                        $arg_archive = array(
                            'posts_per_page' => 8,
                            'post_type' => array($post_type),
                            's' => $busqueda,
                            'order' => 'DESC', 
                            'orderby' => 'relevance',
                            'paged' => $paged,
                        );
                    
                    
                    }else if($post_type != '' && $busqueda == ''  ){

                        $arg_archive = array(
                            'posts_per_page' => 4,
                            'post_type' => array($post_type),
                            'post_status' => array('publish'),
                            'order' => 'DESC', 
                            'orderby' => 'date',
                            'paged' => $paged,
                        );

                    } else if($tag != ''){

                        $arg_archive = array(
                            'posts_per_page' => 4,
                            'post_type' => 'any',
                            'post_status' => array('publish'),
                            'tag' => array($tag),
                            'order' => 'DESC', 
                            'orderby' => 'date',
                            'paged' => $paged,
                        );


                    } else if($autor_id != ''){

                        $arg_archive = array(
                            'posts_per_page' => 4,
                            'post_status' => array('publish'),
                            'post_type' => array('post', 'noticias', 'articulos', 'reviews', 'datos', 'tips', 'actividades'),
                            'author' => $autor_id,
                            'order' => 'DESC', 
                            'orderby' => 'date',
                            'paged' => $paged,
                        );

                    }
                    
                    // The Query
                    $the_query = new WP_Query( $arg_archive );

                    if( $the_query->have_posts() ):

                        // The Loop
                        while ( $the_query->have_posts() ) : $the_query->the_post();

                            //pr($post);
                            $post_type = $post->post_type;
                            $post_type_object = get_post_type_object( $post_type );
                            $img_post = get_the_post_thumbnail_url( null, 'img_front_blog' );

                        ?>

                            <div class="col-lg-3 col-md-6">
                                <div class="c-blog__articles-item"> 
                                    <a class="picture-container" href="<?php the_permalink(); ?>">
                                        <img src="<?php echo $img_post; ?>">
                                    </a>
                                    <div class="c-blog__articles-item-text">
                                        <span><?php  tradgb($post_type_object->name ,$post_type_object->name );?></span>
                                        <h3><?php the_title(); ?></h3>
                                        <div class="c-blog__articles-item-text-description">
                                            <p><?php echo get_excerpt(80); ?></p>
                                        </div>
                                        <a href="<?php the_permalink(); ?>"><?php tradgb('ver', 'Ver'); echo ' '; tradgb($post_type_object->labels->singular_name,$post_type_object->labels->singular_name);?></a>
                                    </div>
                                </div>
                            </div>

                        <?php

                        endwhile;

                        ?>

                        <script>

                            var pagina_actual = <?php echo json_encode($paged); ?>;
                            var query_inicial = <?php echo json_encode($arg_archive); ?>;

                        </script>

                        <?php
                    
                    else:

                        ?>

                        <p><?php tradgb('no_se_han_econtrado_posts', 'No se han econtrado posts')?></p>

                        <?php
                    
                    endif

                    ?>

                    <!-- <div class="col-md-12">
                        
                        <?php kriesi_pagination($the_query->max_num_pages); ?>

                    </div>
                     -->
                    <?php

                    // Reset Query
                    wp_reset_query();

                    ?>
                    
                </div>

                <div class="row c-blog__articles" id="response_message">
                </div>
            </div>
            <div id="hook-scroll" class="c-newsletter">
                <div class="container">
                    <h4><?php  tradgb('suscribete_al_newsletter','Suscríbete al newsletter');	?></h4>
                    <form>
                        <div class="row">
                            <div class="col-md-6">
                                <p><?php  tradgb('quieres_recibir_nuestras_noticias','¿Quieres recibir nuestras noticias?');?></p>
                            </div>
                            <div class="col-md-6">
                                <div class="c-newsletter__inputs">
                                    <input id="email_newsletter" type="text" placeholder="<?php  tradgb('escribe_tu_email','Escribe tu email');?>">
                                    <input type="hidden" id="blog_origen_newsletter" value="<?php echo $site_name; ?>">
                                    <button id="newsletter"><?php  tradgb('suscribete','Suscríbete');?></button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    
<?php 

get_footer(); 

?>