<?php

/*Template Name: Feria EcoBelleza*/ 

get_header('nuevo'); 
$blog_id = get_current_blog_id();
$site_name = get_blog_details($blog_id)->blogname;

 

?>

    <div class="main" role="main">
        <div class="c-blog">
            <div class="c-blog__bg c-blog__bg--eco" style="background-image:url('<?php the_field('imagen_header_izquierda'); ?>');">
                <div class="c-blog__bg2 c-blog__bg--eco2" style="background-image:url('<?php the_field('imagen_header_derecha'); ?>');"></div>
            </div>
            <div class="container">
                <div class="row justify-content-md-center">
                    <div class="col-xl-10">
                        <div class="c-blog__top">
                            <div class="row">
                                <div class="col-12">
                                    <h1><?php  tradgb('feria_eco_belleza','Feria Eco Belleza');?></h1>
                                </div>
                                <div class="col-6">
                                    <a href="<?php echo get_home_url(); ?>/blog-cruelty-free/"> <i class="fas fa-arrow-left"></i><?php  tradgb('volver_al_blog','Volver al blog');?> </a>
                                </div>
                            </div>
                        </div>
                        <div class="c-blog__important">
                            <div class="row">

                                <?php 

                                $posts = get_field('post_destacado');
                                $id_posts_slider = array();

                                if( $posts ):
                                    
                                    foreach( $posts as $post): // variable must be called $post (IMPORTANT) 
                                    
                                        setup_postdata($post); 

                                        $post_type = $post->post_type;
                                        $post_type_object = get_post_type_object( $post_type );
                                        $img_post = get_the_post_thumbnail_url( null, 'img_slider_posts' );
                                        $id_post = $post->ID;

                                        ?>

                                        <div class="col-lg-6">
                                            <a href="<?php the_permalink(); ?>">
                                                <img src="<?php echo $img_post; ?>">
                                            </a>
                                        </div>
                                        <div class="col-lg-6">
                                            <div class="c-blog__important-txt">
                                                <span><?php tradgb($post_type_object->labels->name,$post_type_object->labels->name); ?></span>
                                                <h3><?php the_title(); ?></h3>
                                                <p><?php echo get_excerpt(80); ?></p>
                                                <a href="<?php the_permalink(); ?>"><?php tradgb('ver', 'Ver'); echo ' '; tradgb($post_type_object->labels->singular_name,$post_type_object->labels->singular_name);?></a>
                                            </div>
                                        </div>

                                        <?php 

                                    endforeach;
                                    
                                    wp_reset_postdata(); // IMPORTANT - reset the $post object so the rest of the page works correctly 
                                    
                                endif; 

                                ?>
                                
                            </div>
                        </div>
                    </div>
                </div>
                <div class="c-eco_fair"><img src="<?php the_field('banner_feria'); ?>">
                    <div>
                        <a class="btn btn-primary" href="https://www.feriaecobelleza.cl/"><?php tradgb('ir_a', 'Ir a'); echo ' '; tradgb('feria_eco_belleza','feria eco belleza');?> </a>
                    </div>
                </div>
                <div class="row c-blog__articles">
                    <div id="articulos" class="col-md-12">
                        <h2><?php tradgb('nuestros_post_de', 'Nuestros Post de'); echo ' '; tradgb('feria_eco_belleza','feria eco belleza');?></h2>
                    </div>

                    <?php

                    $paged = (get_query_var('paged')) ? get_query_var('paged') : 1;

                    $arg_eco_bellezas = array(
                        'posts_per_page' => 4,
                        'post_type' => array('actividades', 'reviews', 'tips', 'datos', 'articulos', 'noticias'),
                        'order' => 'DESC', 
                        'orderby' => 'date',
                        'paged' => $paged,
                        'tax_query' => array(
                            'relation' => 'AND',
                            array(
                                'taxonomy' => 'feria_eco',
                                'field'    => 'slug',
                                'terms'    => 'si',
                            ),
                        ),
                    );

                    // The Query
                    $the_query = new WP_Query( $arg_eco_bellezas );

                    // The Loop
                    while ( $the_query->have_posts() ) : $the_query->the_post();

                        //pr($post);
                        $post_type = $post->post_type;
                        $post_type_object = get_post_type_object( $post_type );
                        $img_post = get_the_post_thumbnail_url( null, 'img_front_blog' );

                    ?>

                        <div class="col-lg-3 col-md-6">
                            <div class="c-blog__articles-item">
                                <a class="picture-container" href="<?php the_permalink(); ?>">
                                    <img src="<?php echo $img_post; ?>">
                                </a>
                                <div class="c-blog__articles-item-text">
                                    <span><?php tradgb($post_type_object->name,$post_type_object->name); ?></span>
                                    <h3><?php the_title(); ?></h3>
                                    <div class="c-blog__articles-item-text-description">
                                        <p><?php echo get_excerpt(80); ?></p>
                                    </div>
                                    <a href="<?php the_permalink(); ?>"><?php tradgb('ver', 'Ver'); echo ' '; tradgb($post_type_object->labels->singular_name,$post_type_object->labels->singular_name);?></a>
                                </div>
                            </div>
                        </div>

                    <?php

                    endwhile;

                    ?>

                    <div class="col-md-12">
                        
                        <?php kriesi_pagination($the_query->max_num_pages); ?>

                    </div>
                    
                    <?php

                    // Reset Query
                    wp_reset_query();

                    ?>
                    
                </div>
            </div>
            <div class="c-newsletter">
                <div class="container">
                    <h4><?php  tradgb('suscribete_al_newsletter','Suscríbete al newsletter');?></h4>
                    <form>
                        <div class="row">
                            <div class="col-md-6">
                                <p><?php  tradgb('quieres_recibir_nuestras_noticias','¿Quieres recibir nuestras noticias?');?></p>
                            </div>
                            <div class="col-md-6">
                                <div class="c-newsletter__inputs">
                                    <input id="email_newsletter" type="text" placeholder="<?php  tradgb('escribe_tu_email','Escribe tu email');?>">
                                    <input type="hidden" id="blog_origen_newsletter" value="<?php echo $site_name; ?>">
                                    <button id="newsletter"><?php  tradgb('suscribete','Suscríbete');?></button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    
<?php 

get_footer(); 

?>