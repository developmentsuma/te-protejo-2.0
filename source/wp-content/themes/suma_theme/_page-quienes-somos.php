<?php

/*Template Name: Quienes Somos*/ 

get_header(); 

$count= 0;

?>

<style>
    
    .img-limits{
        display: table;
        margin: auto;
        border: 1px dashed;
        overflow: hidden;
        border-radius: 50%;
        position: relative;
        /* height: 130px; */
        margin-bottom: 20px;
        border: 3px solid #fff;
        z-index: 2;
        width: 109px;
    }

    .picture-egg{
        border: none !important;
        height: 100% !important;
        max-width: 100% !important;
        width: 100% !important;

    }

    .a-objetives{
        color: #53BD96 !important;
        font-size: 20px !important;
    }

    .slick-track{
        margin: 0 !important;
    }

</style>

    <div class="main" role="main">
        <div class="c-about">
            <div class="c-about__bg" style="background-image:url('<?php the_field('imagen_header'); ?>');"> </div>
            <div class="container">
                <div class="row"> 
                    <div class="col-md-12">
                        <!--titulos-->
                        <div class="c-about__top">
                            <div class="row">
                                <div class="col-12">
                                    <h1>Tenemos una misión</h1>
                                    <h2>Conoce a las personas que trabajan en Te Protejo</h2>
                                   
                                </div>
                            </div>
                        </div>

                        <!--information-->
                        <div class="c-about__information">
                            <div class="row">
                                <div class="col-lg-5">
                                    <h3>Quiénes Somos</h3>
                                    <?php $texto_link=get_field('texto_link_youtube');?>
                                </div>
                                <div class="col-lg-7">
                                    <p><?php the_field('texto_quienes_somos'); ?></p>
                                </div>
                            </div>
                        </div>
                        <div class="c-about__objetives">
                            <h4>Objetivos principales</h4>
                            <div class="c-about__objetives-cnt">
                                <div class="c-about__objetives-item --main">
                                    <div class="c-about__objetives-img">
                                        
                                        <?php 
                                        
                                        $url_img_objetivo_1 = get_field('objetivo_1_fotografia');
                                        
                                        ?>

                                        <img src="<?php echo $url_img_objetivo_1['sizes']['img_objetivo_principal']; ?>">
                                    </div>
                                    <div class="c-about__objetives-text">
                                        <h5><?php the_field('objetivo_1_titulo') ?></h5>
                                        <?php the_field('objetivo_1_descripcion') ?>
                                        <a class="a-objetives" href="<?php the_field('link_objetivos_principales'); ?>"><?php the_field('texto_objetivos_principales'); ?></a>
                                       <!-- <a class="a-objetives" href="<?php echo get_home_url(); ?>/archive-posts/?tag=<?php the_field('objetivo_1_tag') ?>">Ver post relacionados</a> -->
                                    </div>
                                </div>
                                <div class="c-about__objetives-subitem">
                                    <div class="c-about__objetives-item">
                                        <div class="c-about__objetives-img">
                                            
                                            <?php 
                                            
                                            $url_img_objetivo_1 = get_field('objetivo_2_fotografia');
                                            
                                            ?>

                                            <img src="<?php echo $url_img_objetivo_1['sizes']['img_objetivo_secundario']; ?>">
                                        </div>
                                        <div class="c-about__objetives-text">
                                            <h5><?php the_field('objetivo_2_titulo') ?></h5>
                                            <?php the_field('objetivo_2_descripcion') ?>
                                            <a class="a-objetives" href="<?php the_field('link_objetivos_principales_2'); ?>"><?php the_field('texto_objetivos_principales_2'); ?></a>
                                       <!-- <a class="a-objetives" href="<?php echo get_home_url(); ?>/archive-posts/?tag=<?php the_field('objetivo_1_tag') ?>">Ver post relacionados</a> -->
                                        </div>
                                    </div>
                                    <div class="c-about__objetives-item">
                                        <div class="c-about__objetives-img">
                                            
                                            <?php 
                                            
                                            $url_img_objetivo_1 = get_field('objetivo_3_fotografia');
                                            
                                            ?>

                                            <img src="<?php echo $url_img_objetivo_1['sizes']['img_objetivo_secundario']; ?>">
                                        </div>
                                        <div class="c-about__objetives-text">
                                            <h5><?php the_field('objetivo_3_titulo') ?></h5>
                                            <?php the_field('objetivo_3_descripcion') ?>
                                            <a class="a-objetives" href="<?php the_field('link_objetivos_principales_3'); ?>"><?php the_field('texto_objetivos_principales_3'); ?></a>
                                       <!-- <a class="a-objetives" href="<?php echo get_home_url(); ?>/archive-posts/?tag=<?php the_field('objetivo_1_tag') ?>">Ver post relacionados</a> -->
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <?php

            $count=0; //reset variable

            $arg_equipos = array(
                'posts_per_page' => -1,
                'post_type' => 'equipos',
                'order' => 'DESC', 
                'orderby' => 'date',
            );

            $the_query = new WP_Query( $arg_equipos );

            // The Loop
            while ( $the_query->have_posts() ) : $the_query->the_post();
                
                global $post;

                ?>

                <div class="c-about__staff">
                    <h4><?php the_title(); ?></h4>
                    <div class="c-carrusel">

                   
                        <?php
                        
                        // check if the repeater field has rows of data
                        if( have_rows('equipo', $post->ID) ):

                            // loop through the rows of data
                            while ( have_rows('equipo', $post->ID) ) : the_row();

                                $fotografia_miembro = get_sub_field('fotografia');
                                $canal_youtube = get_sub_field('youtube');
                                //Siguientes líneas se obtendra objeto user de wp y la url de todos los post del user
                                $name_user = get_sub_field('nombre');
                                $user_wp = get_sub_field('vincular_usuario');
                           
                                ?>

                                <div class="c-carrusel__item">
                                    <div class="c-carrusel__single <?php switch ($count) {
                                                                                            case 0:
                                                                                                # vacio sin clase color
                                                                                                break;
                                                                                            case 1:
                                                                                                echo '--pink';
                                                                                                break;
                                                                                            case 2:
                                                                                                echo '--purple';
                                                                                                break;
                                                                                        }; ?>">
                                        <div class="img-limits">
                                            <img class="picture-egg" src="<?php echo $fotografia_miembro['sizes']['thumbnail']; ?>">
                                        </div>
                                        <h5><?php the_sub_field('nombre'); ?></h5>
                                        <p><?php the_sub_field('cargo'); ?></p>
                                        <?php the_sub_field('descripcion');
                                        
                                        if($canal_youtube){  ?>
                                       <a class="link-youtube" href="<?php echo $canal_youtube;?>" tabindex="-1"><?php echo $texto_link; ?></a>
                                       <?php    
                                    }
                                        ?>

                                        <?php

                                        if($user_wp['ID'] != '1' && $user_wp['ID'] != null){

                                            ?>

                                            <a class="btn-see" href="<?php echo get_home_url(); ?>/archive-posts/?autor=<?php echo $user_wp['ID']; ?>">Ver todos sus posts</a>

                                            <?php
                                        }

                                        ?>
                                        
                                    </div>
                                </div>

                                <?php
                            
                            endwhile;

                        else :

                        // no rows found

                        endif;

                        ?>

                    </div>
                </div>

                <?php

                $count++;

                if($count == 3){

                    $count = 0; //reset var

                }

            endwhile;

            wp_reset_query();

            ?>

            <div class="c-textcontact">
                <p>Si quieres participar en nuestra área de voluntariado, escribe a <a href="">info@teprotejo.cl</a></p>
            </div>
            <div class="c-newsletter">
                <div class="container">
                    <h4>Suscríbete al newsletter</h4>
                    <form>
                        <div class="row">
                            <div class="col-md-6">
                                <p>¿Quieres recibir nuestras noticias?</p>
                            </div>
                            <div class="col-md-6">
                                <div class="c-newsletter__inputs">
                                    <input id="email_newsletter" placeholder="Escribe tu email" type="text">
                                    <button id="newsletter">Suscríbete</button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    
<?php 

get_footer(); 

?>