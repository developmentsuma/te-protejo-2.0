<?php

/*Template Name: Marcas Cruelty-Free*/ 

get_header('nuevo'); 
$blog_id = get_current_blog_id();
$site_name = get_blog_details($blog_id)->blogname;

?>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.2.2/jquery.min.js"></script>
    <div class="main" role="main">
        <div class="c-search">
            <div class="c-search__bg" style="background-image:url('<?php the_field('imagen_header'); ?>');">
                <div class="c-search__bg2">
                </div>
            </div>
            <div class="container">
                <div class="row">
                    <div class="col-md-1">
                    </div>
                    <div class="col-md-10">
                        <div class="c-search__top">
                            <div class="row">
                                <div class="col-12">
                                    <h1><?php  tradgb('marcas_cruelty_free','marcas cruelty-free');?></h1>
                                    <h2><?php  tradgb('encuentra_las_marcas','Encuentra las marcas de belleza que no testean en animales');?></h2>
                                </div>
                            </div>
                            <div class="c-search__search">

                                <?php

                                $pool_ids_marcas = array();
                                $pool_autocomplete_marcas = array();
                                $titles_marcas = array();
                                $tags_marcas = array();
                                $get = get_field('otras_certificaciones');
                                echo '<!-- get'.print_r($get).'-->';
    
                                $arg_marcas = array(
                                    'posts_per_page' => -1,
                                    'post_type' => array('marcas'),
                                    'order' => 'DESC', 
                                    'orderby' => 'date',
                                );
    
                                // The Query
                                $the_query = new WP_Query( $arg_marcas );
    
                                // The Loop
                                while ( $the_query->have_posts() ) : $the_query->the_post();
                                    
                                    $id_post = $post->ID;
                                    array_push($pool_ids_marcas, $id_post);
                                    array_push($titles_marcas, $post->post_title);
    
                                endwhile;
    
                                // Reset Query
                                wp_reset_query();

                                $tags_marcas = wp_get_object_terms( $pool_ids_marcas, 'post_tag');
                                
                                foreach ($tags_marcas as $tag_marca) {
                                    
                                    array_push($pool_autocomplete_marcas, $tag_marca->name);

                                }

                                foreach ($titles_marcas as $title_marca) {
                                    
                                    array_push($pool_autocomplete_marcas, $title_marca);
                                
                                }

                                ?>

                                <script> 
                                    
                                    var autocomplete_marcas = <?php echo json_encode($pool_autocomplete_marcas); ?>;
                                    
                                </script>

                                <input id="buscador_marcas" type="text" placeholder="<?php  tradgb('buscac_ruelty_free_placeholder','Busca una marca o punto de venta cruelty-free');?>">
                                <button id="btn_buscador_marcas"> <i class="fas fa-search"></i></button>
                            </div>
                            <div class="c-search__filter">
                                <p><?php  tradgb('usa_nuestros_filtros','En el listado encuentras las marcas cruelty free que cuentan con un sello oficial y están regularizadas ante la entidad sanitaria del país');?></p>
                                <form id="c-filter" class="">

                                    
                                    <?php

                                    $categorias = get_terms('empresas_extranjeras_segmento');
                                    $respaldos = get_terms('empresas_extranjeras_respaldo');
                                    $puntos_venta = get_terms('empresas_extranjeras_venta');
                                    $certificaciones = get_terms('empresas_extranjeras_porcentaje');                                     

                                    ?>

                                    <div class="row">
                                        <div class="col-xl-3 col-lg-6 bg-scroll"> 
                                            <p><?php  tradgb('categorias','Categorías');?></p>
                                            <i class="fas fa-sort-down" id="js-expand"></i>
                                            <div class="c-scrolls1">
                                            <ul class="one">

                                                <?php

                                                foreach ($categorias as $categoria) {
                                          
                                                    ?>

                                                        <li>
                                                            <input type="checkbox" id="<?php echo $categoria->slug; ?>" value="<?php echo $categoria->term_id; ?>" data-father="empresas_extranjeras_segmento">
                                                            <label for="<?php echo $categoria->slug; ?>"><?php echo $categoria->name; ?></label>
                                                        </li>

                                                    <?php

                                                }

                                                ?>
                                               
                                            </ul>
                                        </div>
                                        </div>
                                        <div class="col-xl-3 col-lg-6"> 
                                            <p><?php  tradgb('respaldo','Respaldo');?></p>
                                            <ul class="two">

                                                <?php

                                                foreach ($respaldos as $respaldo) {
                                                    
                                                    ?>

                                                        <li>
                                                            <input type="checkbox" id="<?php echo $categoria->slug; ?>" value="<?php echo $respaldo->term_id; ?>" data-father="empresas_extranjeras_respaldo">
                                                            <label for="<?php echo $respaldo->slug; ?>"><?php echo $respaldo->name; ?></label>
                                                        </li>

                                                    <?php

                                                }

                                                ?>
                                                
                                            </ul>
                                        </div>
                                        <div class="col-xl-3 col-lg-6 bg-scroll"> 
                                            <p><?php  tradgb('punto_de_venta','Punto de venta');?></p>
                                            <div class="c-scrolls3">
                                                <ul class="one">
                                                    
                                                    <?php

                                                    foreach ($puntos_venta as $punto_venta) {
                                                        
                                                        ?>

                                                            <li>
                                                                <input type="checkbox" id="<?php echo $categoria->slug; ?>" value="<?php echo $punto_venta->term_id; ?>" data-father="empresas_extranjeras_venta">
                                                                <label for="<?php echo $punto_venta->slug; ?>"><?php echo $punto_venta->name; ?></label>
                                                            </li>

                                                        <?php

                                                    }

                                                    ?>

                                                </ul>
                                            </div>
                                        </div>
                                        <div class="col-xl-3 col-lg-6">
                                            <p><?php  tradgb('otras_certificaciones','Otras Certificaciones');?></p>
                                            <div class="c-scrolls4">
                                                <ul class="one"> 
                                                    <!--li>
                                                        <input type="checkbox" id="certificado-vegano" value="4236" data-father="empresas_extranjeras_porcentaje">
                                                        
                                                    </li-->
                                                    <?php
                                                            foreach ($certificaciones as $certificacion) {
                                                                
                                                                ?>

                                                                    <li>
                                                                        <input type="checkbox" id="<?php echo $certificacion->slug; ?>" value="<?php echo $certificacion->term_id; ?>" data-father="empresas_extranjeras_porcentaje">
                                                                        <label for="<?php echo $certificacion->slug; ?>"><?php echo $certificacion->name; ?></label>
                                                                    </li>

                                                                <?php

                                                        } 

                                                    ?>
                                                    <!-- <li>
                                                        <input type="checkbox" id="sin-certificacion" value="4237" data-father="empresas_extranjeras_porcentaje">
                                                        <label for="sin-certificacion">Sin certificación</label>
                                                    </li> -->
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row c-search__articles">
                    <!-- <div class="col-md-12">
                        <h3>Marcas Cruelty Free</h3>
                        <div class="dropdown">
                            <select>
                                <option value="0">Mas recientes</option>
                                <option value="1">Por nombre A - Z</option>
                                <option value="2">Por nombre Z - A</option>
                            </select>
                            <i class="fas fa-caret-down"></i>
                        </div>
                    </div> -->
                    <div class="col-sm-12 col-md-10 offset-md-1">
                        <div id="posts_originales2" class="row ">
                        <?php  if( have_rows('marca_destacada') ): while ( have_rows('marca_destacada') ) : the_row(); 
                                $MarcasDestacadas = get_sub_field('marca');  
                                $id_post2 = $MarcasDestacadas->ID;
                                $img_post2 = get_the_post_thumbnail_url( $id_post2, 'img_front_blog' );
                                $categorias_post = wp_get_post_terms( $id_post2, 'empresas_extranjeras_segmento' );
                                $puntos_venta_post = wp_get_post_terms( $id_post2, 'empresas_extranjeras_venta' );
                                $respaldos_post = wp_get_post_terms( $id_post2, 'empresas_extranjeras_respaldo' );
                                $slug = get_post_field( 'post_name', $id_post2 );
                        ?>
                          
                               <div class="col-lg-3 col-md-6">
                                    <div class="c-search__articles-item"> 
                                        <img src="<?php echo $img_post2; ?>">
                                        <div class="c-search__articles-item-text imgbgDestacada marca-destacada">
                                            <div class="d-table w-100">
                                                <div class="d-table-cell text-center">
                                                    <h3 class="h3Destacado"><?php echo  $MarcasDestacadas->post_title; ?></h3>
                                                </div>
                                            </div>
                                            <img class="imgLineaDestacado" src="<?php bloginfo( 'template_url' ); ?>/img/linea.png" alt="">
                                            <img class="imgDestacado" src="<?php bloginfo( 'template_url' ); ?>/img/marca_destacada.png" alt="">
                                            <p class="category-trademark pDestacado"><?php  tradgb('categorias','Categorías');?>: 
                                                
                                                <?php

                                                $count = count($categorias_post); //servira para dejar de poner comas al listar

                                                foreach ($categorias_post as $categoria_post) {
                                                    
                                                if($count == 1){
                                                    
                                                        echo $categoria_post->name;
                                                        

                                                }
                                                else{

                                                        echo $categoria_post->name.', ';
                                                
                                                }

                                                $count--;
                                                }

                                                ?>
                                            </p>

                                            <p class="sale-point pDestacado"><?php  tradgb('punto_de_venta','Punto de venta');?>: 
                                                
                                                <?php

                                                $count = count($puntos_venta_post); //servira para dejar de poner comas al listar

                                                foreach ($puntos_venta_post as $punto_venta_post) {
                                                    
                                                if($count == 1){
                                                    
                                                        echo $punto_venta_post->name;

                                                }
                                                else{

                                                        echo $punto_venta_post->name.', ';
                                                
                                                }

                                                $count--;
                                                }

                                                ?> 

                                            </p>
                                            <p class="backup-trademark pDestacado"><?php  tradgb('respaldo','Respaldo');?>: 
                                                
                                                <?php

                                                $count = count($respaldos_post); //servira para dejar de poner comas al listar

                                                foreach ($respaldos_post as $respaldo_post) {
                                                    
                                                if($count == 1){
                                                    
                                                        echo $respaldo_post->name;

                                                }
                                                else{

                                                        echo $respaldo_post->name.', ';

                                                }

                                                $count--;
                                                }

                                                ?>
                                            </p>
                                            
                                            <p class="backup-trademark pDestacado"><?php  tradgb('rango_de_precio','Rango de precio');?>:
                                                    
                                                <?php
                                                $nivel_precio = get_field('nivel_precio',$id_post2);
                                                for ($nivel_precio; $nivel_precio > 0; $nivel_precio--) { 

                                                    ?>

                                                    <i class="fas fa-dollar-sign" style="font-sixe:15px;"></i>

                                                    <?php

                                                }

                                                ?>

                                            </p>

                                            <div class="d-table w-100 trademark-footer">
                                                <div class="d-table-cell">
                                                <?php
                                                    
                                                    $slug_title = quitar_tildes($MarcasDestacadas->post_title);
                                                    $slug_title = str_replace(" ", "-", $slug_title);
                                                    $post_slug = $MarcasDestacadas->post_name;
                                                    
                                                    ?>
                                                    <?php

                                                    

                                                    if(get_field('link_web',$id_post2)){

                                                        ?>

                                                        <a class="d-inline-block" id="<?php echo $post_slug."xx"; ?>" style="position:static" href="<?php the_field('link_web',$id_post2); ?>?utm_source=ongteprotejo.org&utm_medium=marcas_cruelty_free" target="_blank"><?php tradgb('visita_sitio_web','Visita Sitio Web');?></a>

                                                        <?php
                                                    }

                                                    ?>
                                                    
                                                </div>
                                                <div class="d-table-cell text-right">
                                                    <a class="d-inline-block" style="position:static" id="<?php echo $post_slug; ?>" href="<?php echo get_home_url(); ?>/archive-posts/?tag=<?php echo $slug_title; ?>"><?php  tradgb('ver','Ver');?> <?php echo ' ' . tradgb('reviews','Reviews');?></a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <script>
                           $("#<?php echo $post_slug ?>").on("click", function() {
                                gtag("event", "click", {
                                "event_category": "Btn_VerReview",
                                "event_label":  "<?php echo $post_slug ?>"  
                             }); 
                            });
						</script>
						<script>
                         $("#<?php echo $post_slug ?>xx").on("click", function() {
                                gtag("event", "click", {
                                "event_category": "Btn_Sitioweb",
                                "event_label":  "<?php echo $post_slug?>"  
                             }); 
                            });
                        </script>

                             <?php endwhile; else : endif; ?>
                             </div>
                             </div>
                    <div class="col-sm-12 col-md-10 offset-md-1">
                        <div id="posts_originales" class="row ">

                            <?php

                            //esta consulta es solo para saber cuantos post trae en total
                            $arg_marcas = array(
                                'posts_per_page' => -1,
                                'post_type' => array('marcas'),
                                'order' => 'DESC', 
                                'orderby' => 'date',
                                'tax_query' => array(
                                    'relation' => 'AND',
                                ),
                            );

                            // The Query
                            $the_query = new WP_Query( $arg_marcas );

                            $total_posts = count($the_query->posts);
                            //fin consulta para traer post totales//

                            $pagina_actual = 1; //indica el número de página en la que se encuentra el user
                            $cantidad_post_mostrados = 16; //indica cuantos post buscaran las querys

                            $arg_marcas = array(
                                'posts_per_page' => $cantidad_post_mostrados,
                                'paged' => $pagina_actual,
                                'post_type' => array('marcas'),
                                'post_status' => 'publish',
                                'order' => 'DESC', 
                                'orderby' => 'date',
                                'tax_query' => array(
                                    'relation' => 'AND',
                                ),
                            );


                            ?>

                            <script>
                                
                                var query_inicial = <?php echo json_encode($arg_marcas); ?>;
                            
                            </script>

                            <?php

                            // The Query
                            $the_query = new WP_Query( $arg_marcas );

                            // The Loop
                            while ( $the_query->have_posts() ) : $the_query->the_post();

                                $id_post = $post->ID;
                                $post_type = $post->post_type;
                                $img_post = get_the_post_thumbnail_url( null, 'img_front_blog' );
                                $categorias_post = wp_get_post_terms( $id_post, 'empresas_extranjeras_segmento' );
                                $puntos_venta_post = wp_get_post_terms( $id_post, 'empresas_extranjeras_venta' );
                                $respaldos_post = wp_get_post_terms( $id_post, 'empresas_extranjeras_respaldo' );
                                $vegano = wp_get_post_terms( $id_post, 'empresas_extranjeras_porcentaje' );
                                $nivel_precio = get_field('nivel_precio');
                                $otras_certificaciones = get_field('otras_certificaciones');

                            ?>
    
                                <div class="col-lg-3 col-md-6">
                                    <div class="c-search__articles-item"> 
                                        <img src="<?php echo $img_post; ?>">
                                        <div class="c-search__articles-item-text">
                                            <div class="d-table w-100">
                                                <div class="d-table-cell text-center">
                                                    <h3><?php the_title(); ?></h3>
                                                </div>
                                            </div>
                                            <p class="category-trademark"><?php  tradgb('categorias','Categorías');?>: 
                                                
                                                <?php

                                                $count = count($categorias_post); //servira para dejar de poner comas al listar

                                                foreach ($categorias_post as $categoria_post) {
                                                    
                                                if($count == 1){
                                                    
                                                        echo $categoria_post->name;
                                                        

                                                }
                                                else{

                                                        echo $categoria_post->name.', ';
                                                
                                                }

                                                $count--;
                                                }

                                                ?>
                                            </p>

                                            <p class="sale-point"><?php  tradgb('punto_de_venta','Punto de venta');?>: 
                                                
                                                <?php

                                                $count = count($puntos_venta_post); //servira para dejar de poner comas al listar

                                                foreach ($puntos_venta_post as $punto_venta_post) {
                                                    
                                                if($count == 1){
                                                    
                                                        echo $punto_venta_post->name;

                                                }
                                                else{

                                                        echo $punto_venta_post->name.', ';
                                                
                                                }

                                                $count--;
                                                }

                                                ?> 

                                            </p>

                                            <p class="backup-trademark"><?php  tradgb('respaldo','Respaldo');?>: 
                                                
                                                <?php

                                                $count = count($respaldos_post); //servira para dejar de poner comas al listar

                                                foreach ($respaldos_post as $respaldo_post) {
                                                    
                                                if($count == 1){
                                                    
                                                        echo $respaldo_post->name;

                                                }
                                                else{

                                                        echo $respaldo_post->name.', ';

                                                }

                                                $count--;
                                                }

                                                ?>

                                           
                                            
                                            
                                            <?php
                                              /*
                                            if($otras_certificaciones){

                                                ?>

                                                <p class="backup-trademark">Otras certificaciones: <?php echo $otras_certificaciones; ?> </p>

                                                <?php
                                            }
                                                */
                                            ?>

                                            <p class="backup-trademark"><?php  tradgb('rango_de_precio','Rango de precio');?>:
                                                    
                                                <?php

                                                for ($nivel_precio; $nivel_precio > 0; $nivel_precio--) { 

                                                    ?>

                                                    <i class="fas fa-dollar-sign" style="font-sixe:15px;"></i>

                                                    <?php

                                                }

                                                ?>

                                            </p>

                                            <div class="d-table w-100 trademark-footer">
                                                <div class="d-table-cell">
                                                <?php
                                                    
                                                    $slug_title = quitar_tildes(get_the_title());
                                                    $slug_title = str_replace(" ", "-", $slug_title);
                                                    $title_event = get_the_title();
                                                    global $post;
                                                    $slug_event = $post->post_name;
                                                    ?>
                                                    <?php

                                                    if(get_field('link_web')){

                                                        ?>

                                                        <a class="d-inline-block" id="<?php echo $slug_event."xx"; ?>" style="position:static" href="<?php the_field('link_web'); ?>?utm_source=ongteprotejo.org&utm_medium=marcas_cruelty_free" target="_blank"><?php tradgb('visita_sitio_web','Visita Sitio Web');?></a>

                                                        <?php
                                                    }

                                                    ?>
                                                    
                                                </div>
                                                <div class="d-table-cell text-right">
                                                    <a class="d-inline-block" style="position:static" id="<?php echo $slug_event; ?>" href="<?php echo get_home_url(); ?>/archive-posts/?tag=<?php echo $slug_title; ?>"><?php  tradgb('ver','Ver');?> <?php echo ' ' . tradgb('reviews','Reviews');?></a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
						<script>
                         $("#<?php echo $slug_event ?>").on("click", function() {
                                gtag("event", "click", {
                                "event_category": "Btn_VerReview",
                                "event_label":  "<?php echo $slug_event ?>"  
                             }); 
                            });
						</script>
						<script>
                         $("#<?php echo $slug_event ?>xx").on("click", function() {
                                gtag("event", "click", {
                                "event_category": "Btn_Sitioweb",
                                "event_label":  "<?php echo $slug_event ?>"  
                             }); 
                            });
                        </script>
                            <?php

                            endwhile;

                            ?>



                            <?php

                            // Reset Query
                            wp_reset_query();

                            ?>
                            
                        </div>
                        <div id="response_message" class="row c-search__articles">
                        </div>

                        <script> 
                                    
                            var pagina_actual = <?php echo json_encode($pagina_actual); ?>;
                            var total_posts = <?php echo json_encode($total_posts); ?>;
                            var cantidad_post_mostrados = <?php echo json_encode($cantidad_post_mostrados); ?>;
                            var var_busqueda = '';
                            
                        </script>
                        
                        <!-- <div id="paginador" class="col-md-12">
                            <p id="page-selection"></p>
                        </div> -->

                    </div>
                </div>
            </div>
            <div id="handler-scroll" class="c-newsletter">
                <div class="container">
                    <h4><?php  tradgb('suscribete_al_newsletter','Suscríbete al newsletter');?></h4>
                    <form>
                        <div class="row">
                            <div class="col-md-6">
                                <p><?php  tradgb('quieres_recibir_nuestras_noticias','¿Quieres recibir nuestras noticias?');?></p>
                            </div>
                            <div class="col-md-6">
                                <div class="c-newsletter__inputs">
                                    <input id="email_newsletter" type="text" placeholder="<?php  tradgb('escribe_tu_email','Escribe tu email');?>">
                                    <input type="hidden" id="blog_origen_newsletter" value="<?php echo $site_name; ?>">
                                    <button id="newsletter"><?php  tradgb('suscribete','Suscríbete');?></button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

  
    
<?php 

get_footer(); 

?>

<script src="<?php bloginfo( 'template_url' ); ?>/js/autocompletar-marcas.js"></script>
<script>
 
 
var maxHeight = Math.max.apply(null, $("div.marca-destacada").map(function ()
{
    return $(this).innerHeight();
}).get());

console.log(maxHeight);

$("div.marca-destacada").css('height',maxHeight+ "px");



    $( document ).ready(function() {
 
});

</script>