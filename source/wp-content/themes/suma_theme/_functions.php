<?php
/**
 * estudiosuma functions and definitions
 *
 * Sets up the theme and provides some helper functions. Some helper functions
 * are used in the theme as custom template tags. Others are attached to action and
 * filter hooks in WordPress to change core functionality.
 *
 * The first function, estudiosuma_setup(), sets up the theme by registering support
 * for various features in WordPress, such as post thumbnails, navigation menus, and the like.
 *
 * When using a child theme (see http://codex.wordpress.org/Theme_Development and
 * http://codex.wordpress.org/Child_Themes), you can override certain functions
 * (those wrapped in a function_exists() call) by defining them first in your child theme's
 * functions.php file. The child theme's functions.php file is included before the parent
 * theme's file, so the child theme functions would be used.
 *
 * Functions that are not pluggable (not wrapped in function_exists()) are instead attached
 * to a filter or action hook. The hook can be removed by using remove_action() or
 * remove_filter() and you can attach your own function to the hook.
 *
 * We can remove the parent theme's hook only after it is attached, which means we need to
 * wait until setting up the child theme:
 *
 * <code>
 * add_action( 'after_setup_theme', 'my_child_theme_setup' );
 * function my_child_theme_setup() {
 *     // We are providing our own filter for excerpt_length (or using the unfiltered value)
 *     remove_filter( 'excerpt_length', 'estudiosuma_excerpt_length' );
 *     ...
 * }
 * </code>
 *
 * For more information on hooks, actions, and filters, see http://codex.wordpress.org/Plugin_API.
 *
 * @package WordPress
 * @subpackage estudiosuma
 * @since Estudiosuma 1.0
 */

/**
 * Set the content width based on the theme's design and stylesheet.
 *
 * Used to set the width of images and content. Should be equal to the width the theme
 * is designed for, generally via the style.css stylesheet.
 */

 /*Genera un orden al listar atributos*/
function pr($data){
	echo '<pre>';
	print_r($data);
	echo '</pre>';
}

/*Tamaños personalizados de imgs */

add_image_size( 'img_front_blog', 361, 260, true );
add_image_size( 'img_destacada_blog', 480, 365, true );
add_image_size( 'img_pequenia_equipo', 101, 101, true );
add_image_size( 'img_grande_equipo', 360, 449, true );
add_image_size( 'img_objetivo_principal', 603, 369, true );
add_image_size( 'img_objetivo_secundario', 417, 369, true );
add_image_size( 'img_slider_posts', 750, 515, true );

/*Remueve el editor de texto de las páginas*/
add_action('init', 'init_remove_support',100);
function init_remove_support(){
    $post_type = 'page';
    remove_post_type_support( $post_type, 'editor');
}

//oculta elementos del admin menu
add_action( 'admin_menu', 'hide_elements' );
function hide_elements() {
	remove_menu_page('edit.php'); //oculta menú "Entradas"
}

/*Agrega thumbnails a los custom post type*/
add_theme_support( 'post-thumbnails', array( 'post', 'noticias', 'articulos', 'reviews', 'datos', 'tips', 'actividades', 'marcas' ) );

register_nav_menus( array( 
	'header' => 'Header menu', 
	'footer' => 'Footer menu' 
) );

//CORTAR STRINGS PARA EXTRACTOS DE LAS POST//
function cortar_cadena($cadena, $longitud) {
	// Inicializamos las variables
	$contador = 0;
	$texto = '';
	 
	// Cortamos la cadena por los espacios
	$arrayTexto = explode(' ', $cadena);
	 
	// Reconstruimos la cadena palabra a palabra mientras no sobrepasemos la longitud maxima
	while($longitud >= strlen($texto) + strlen($arrayTexto[$contador])) {
		$texto .= ' '.$arrayTexto[$contador];
		$contador++;
	}
	 
	//añadimos los ... al final de la cadena si esta era mas larga que la longitud maximo
	if(strlen($cadena)>$longitud){
		$texto .= '...';
	}
	 
	return trim($texto);
}

//TRANSFORMA NÚMEROS DE MESES EN NOMBRES DE MESES//
function nombre_mes($mes){

	switch ($mes) {
		case '01':
			$mes = 'Enero';
			break;
		case '02':
			$mes = 'Febrero';
			break;
		case '03':
			$mes = 'Marzo';
			break;
		case '04': 
			$mes = 'Abril';
			break;
		case '05': 
			$mes = 'Mayo';
			break;
		case '06': 
			$mes = 'Junio';
			break;
		case '07':
			$mes = 'Julio';
			break;
		case '08':
			$mes = 'Agosto';
			break;
		case '09':
			$mes = 'Septiembre';
			break;
		case '10':
			$mes = 'Octubre';
			break;
		case '11':
			$mes = 'Noviembre';
			break;
		case '12':
			$mes = 'Diciembre';
			break;
		default:
			$mes = ' ';
			break;
	}

	return $mes;
}
function turn_on_comments() { 
	update_option('default_comment_status', 'open');
 } 
 add_action('update_option', 'turn_on_comments');


 function default_comments_on( $data ) {
    if( $data['post_type'] == 'Reviews' ) {
        $data['comment_status'] = 1;
    }

    return $data;
}
add_filter( 'wp_insert_post_data', 'default_comments_on' );


/*PAGINADOR --CRÉDITOS PARA kriesi.at-- */
function kriesi_pagination($pages = '', $range = 2)
{  
	 //$showitems = ($range * 2)+1; cuantos items muestra
	 $showitems = 3;  

     global $paged;
     if(empty($paged)) $paged = 1;

     if($pages == '')
     {
         global $wp_query;
         $pages = $wp_query->max_num_pages;
         if(!$pages)
         {
             $pages = 1;
         }
     }   

     if(1 != $pages)
     {
         echo "<div class='c-pagination'>";
         if($paged > 2 && $paged > $range+1 && $showitems < $pages) echo "<a href='".get_pagenum_link(1)."#articulos'>&laquo;</a>";
         if($paged > 1 && $showitems < $pages) echo "<a href='".get_pagenum_link($paged - 1)."#articulos'>&lsaquo;</a>";

         for ($i=1; $i <= $pages; $i++)
         {
             if (1 != $pages &&( !($i >= $paged+$range+1 || $i <= $paged-$range-1) || $pages <= $showitems ))
             {
                 echo ($paged == $i)? "<a class='active'>".$i."</a>":"<a href='".get_pagenum_link($i)."#articulos' class='inactive' >".$i."</a>";
             }
         }

         if ($paged < $pages && $showitems < $pages) echo "<a href='".get_pagenum_link($paged + 1)."#articulos'>&rsaquo;</a>";  
         if ($paged < $pages-1 &&  $paged+$range-1 < $pages && $showitems < $pages) echo "<a href='".get_pagenum_link($pages)."#articulos'>&raquo;</a>";
         echo "</div>\n";
     }
}
 
/*CUSTOM POST-TYPE*/
add_action( 'init', 'create_post_type' );

function create_post_type(){
	
	register_post_type( 'Noticias',
		array(
			'labels' => array(
				'name' => __( 'Noticias' ),
				'singular_name' => __( 'noticia' )
			),
		'public' => true,
		'menu_icon' => 'dashicons-megaphone',
		'has_archive' => true,
		'supports' => array( 'title', 'editor', 'thumbnail' ),
		'taxonomies' => array('post_tag')

		)
	);

	register_post_type( 'Articulos',
		array(
			'labels' => array(
				'name' => __( 'Articulos' ),
				'singular_name' => __( 'articulo' )
			),
		'public' => true,
		'menu_icon' => 'dashicons-edit',
		'has_archive' => true,
		'supports' => array( 'title', 'editor', 'thumbnail' ),
		'taxonomies' => array('post_tag')

		)
	);

	register_post_type( 'Reviews',
		array(
			'labels' => array(
				'name' => __( 'Reviews ' ), //nombre con espacio al final para core WP no lo traduzca!!!
				'singular_name' => __( 'review' )
			),
		'public' => true,
		'menu_icon' => 'dashicons-visibility',
		'has_archive' => true,
		'supports' => array( 'title', 'editor', 'thumbnail','comments' ),
		'taxonomies' => array('post_tag')

		)
	);



	register_post_type( 'Datos',
		array(
			'labels' => array(
				'name' => __( 'Datos' ),
				'singular_name' => __( 'dato' )
			),
		'public' => true,
		'menu_icon' => 'dashicons-format-status',
		'has_archive' => true,
		'supports' => array( 'title', 'editor', 'thumbnail' ),
		'taxonomies' => array('post_tag')

		)
	);

	register_post_type( 'Tips',
		array(
			'labels' => array(
				'name' => __( 'Tips' ),
				'singular_name' => __( 'tip' )
			),
		'public' => true,
		'menu_icon' => 'dashicons-post-status',
		'has_archive' => true,
		'supports' => array( 'title', 'editor', 'thumbnail' ),
		'taxonomies' => array('post_tag')

		)
	);

	register_post_type( 'Actividades',
		array(
			'labels' => array(
				'name' => __( 'Actividades' ),
				'singular_name' => __( 'actividad' )
			),
		'public' => true,
		'menu_icon' => 'dashicons-tickets-alt',
		'has_archive' => true,
		'supports' => array( 'title', 'editor', 'thumbnail' ),
		'taxonomies' => array('post_tag')

		)
	);

	register_post_type( 'Marcas',
		array(
			'labels' => array(
				'name' => __( 'Marcas' ),
				'singular_name' => __( 'marca' )
			),
		'public' => true,
		'menu_icon' => 'dashicons-store',
		'has_archive' => true,
		'supports' => array( 'title', 'editor', 'thumbnail' ),
		'taxonomies' => array('post_tag')
		)
	);

	register_post_type( 'Equipos',
		array(
			'labels' => array(
				'name' => __( 'Equipos' ),
				'singular_name' => __( 'equipo' )
			),
		'public' => true,
		'menu_icon' => 'dashicons-groups',
		'has_archive' => true,
		'supports' => array( 'title' )

		)
	);
}

/*TAXONOMIAS*/
add_action( 'init', 'create_taxonomy' );
function create_taxonomy() {

	register_taxonomy('segmento','marcas',
		array(
			'label' => __( 'Segmento' ),
			'rewrite' => array( 'slug' => 'segmento' ),
			'hierarchical' => true,
		)
	);
	register_taxonomy('punto_venta','marcas',
		array(
			'label' => __( 'Punto de  Venta' ),
			'rewrite' => array( 'slug' => 'punto_venta' ),
			'hierarchical' => true,
		)
	);
	register_taxonomy('respaldo','marcas',
		array(
			'label' => __( 'Respaldo' ),
			'rewrite' => array( 'slug' => 'respaldo' ),
			'hierarchical' => true,
		)
	);
	register_taxonomy('vegano','marcas',
		array(
			'label' => __( 'Vegano' ),
			'rewrite' => array( 'slug' => 'vegano' ),
			'hierarchical' => true,
		)
	);
}

flush_rewrite_rules();

/*AJAX FILTROS MARCAS*/

add_action('wp_enqueue_scripts', 'ajax_filtros');
add_action('wp_ajax_nopriv_ajax_filtros','ajax_filtros_contenido');
add_action('wp_ajax_ajax_filtros','ajax_filtros_contenido');

function ajax_filtros() {

	wp_register_script('ajax_filtros_script', get_template_directory_uri(). '/js/filtros-marcas.js', array('jquery'), '1', true );
	wp_enqueue_script('ajax_filtros_script');

	wp_localize_script('ajax_filtros_script','ajax_wp',['ajaxurl'=>admin_url('admin-ajax.php')]);

};

function ajax_filtros_contenido(){
	
	$pool_ids_categorias = $_POST['pool_ids_categorias'];
	$pool_ids_puntos_venta = $_POST['pool_ids_puntos_venta'];
	$pool_ids_respaldos = $_POST['pool_ids_respaldos'];
	$pool_ids_vegano = $_POST['pool_ids_vegano'];
	
	$var_busqueda = $_POST['var_busqueda'];
	$query_busqueda = $_POST['query_busqueda'];
	$pagina_actual = $_POST['pagina_actual'];
	$cantidad_post_mostrados = $_POST['cantidad_post_mostrados'];
	
	$resultado_ajax = array();
	$tax_query_array = array();
	$response_ajax = array();


	if(count($pool_ids_categorias) > 0){

		array_push($tax_query_array, array(
			'taxonomy' => 'segmento',
			'field' => 'term_id',
			'terms' => $pool_ids_categorias,
		));

	}
	if(count($pool_ids_puntos_venta) > 0){

		array_push($tax_query_array, array(
			'taxonomy' => 'punto_venta',
			'field' => 'term_id',
			'terms' => $pool_ids_puntos_venta,
		));

	}
	if(count($pool_ids_respaldos) > 0){

		array_push($tax_query_array, array(
			'taxonomy' => 'respaldo',
			'field' => 'term_id',
			'terms' => $pool_ids_respaldos,
		));

	}
	if(count($pool_ids_vegano) > 0){

		array_push($tax_query_array, array(
			'taxonomy' => 'vegano',
			'field' => 'term_id',
			'terms' => $pool_ids_vegano,
		));

	}

	if(!empty($var_busqueda)){ //se ejecuta si alguien realiza una busqueda de tags desde la sección de marcas
		
		if(count($tax_query_array) > 0){ //si vienen filtros los inserta en query_busqueda
			
			array_push($query_busqueda['tax_query'], $tax_query_array); //inserto filtros en tax_query de la query ya generada en la busqueda
		
		}
		else{

			$restaurar_query_busqueda = array(
				'relation'=>'AND',
					array(
					'taxonomy' => 'post_tag',
					'field' => 'slug',
					'terms' => $var_busqueda,
					),
				);

			unset($query_busqueda['tax_query']);
			$query_busqueda['tax_query'] = array();
			array_push($query_busqueda['tax_query'], array(
				'relation'=>'AND',
					array(
					'taxonomy' => 'post_tag',
					'field' => 'slug',
					'terms' => $var_busqueda,
					),
				));
		}
		
		$arg_marcas_ajax = $query_busqueda;

	}elseif(count($tax_query_array) > 0){ //se ejecuta si vienen filtros seleccionados

		$arg_marcas_ajax = array( 
			'posts_per_page' => $cantidad_post_mostrados,
			'paged' => $pagina_actual,
			'post_type' => array('marcas'),
			'order' => 'DESC', 
			'orderby' => 'date',
			'tax_query' => array(
				'relation' => 'AND',
				$tax_query_array
			),
		);

	}else{ //se ejecuta si el array de filtros queda vacio, trae todos los post

		$arg_marcas_ajax = array(
			'posts_per_page' => $cantidad_post_mostrados,
			'paged' => $pagina_actual,
			'post_type' => array('marcas'),
			'order' => 'DESC', 
			'orderby' => 'date',
		);

	}
	
	$html_post = array(); //se usara para guardar momentaneamente los posts recien llamados
	$array_posts = array(); //array final de posts, guardara el html de cada post para devolver en el ajax a js

	//esta consulta es solo para saber cuantos post trae en total
		$arg_marcas_ajax['posts_per_page'] = -1;
			
		// The Query
		$the_query = new WP_Query( $arg_marcas_ajax );

		$total_posts = count($the_query->posts);

		// Reset Query
		wp_reset_query();

		$arg_marcas_ajax['posts_per_page'] = $cantidad_post_mostrados; //vuelta al orignal de los post mostrados
	//fin consulta para traer post totales//

	// The Query
	$the_query = new WP_Query( $arg_marcas_ajax );

	// The Loop
	while ( $the_query->have_posts() ) : $the_query->the_post();

		global $post; //esto logra que dentro del loop exista el objeto global del post

		$id_post = $post->ID;
		$titulo = get_the_title();
		$post_type = $post->post_type;
		$img_post = get_the_post_thumbnail_url( null, 'img_front_blog' );
		$categorias_post = wp_get_post_terms( $id_post, 'segmento' );
		$string_categorias = "";
		$puntos_venta_post = wp_get_post_terms( $id_post, 'punto_venta' );
		$string_punto_venta = "";
		$respaldos_post = wp_get_post_terms( $id_post, 'respaldo' );
		$string_respaldo = "";
		$vegano = wp_get_post_terms( $id_post, 'vegano' );

		//Arma array de categorias (segmentos)
		$count = count($categorias_post); 
		foreach ($categorias_post as $categoria_post) {
			
			$string_categorias .= $categoria_post->name;

			if($count != 1){

				$string_categorias .= ', ';
			
			}

			$count--;
		}

		//Arma array de puntos de ventas
		$count = count($puntos_venta_post); 
		foreach ($puntos_venta_post as $punto_venta_post) {
			
			$string_punto_venta .= $punto_venta_post->name;

			if($count != 1){

				$string_punto_venta .= ', ';
			
			}

			$count--;
		}

		//Arma array de respaldos
		$count = count($respaldos_post); 
		foreach ($respaldos_post as $respaldo_post) {
			
			$string_respaldo .= $respaldo_post->name;
			
			if($count != 1){

				$string_respaldo .= ', ';

			}

			$count--;
		}
		

		$html_post = ' 	<div class="col-lg-3 col-md-6 posts_ajax">
							<div class="c-search__articles-item"> 
								<img src="'.$img_post.'">
								<div class="c-search__articles-item-text"> 
									<h3>'.$titulo.'</h3>
									<p>Categorías: '.$string_categorias.'</p> 
									<p>Punto de Venta: '.$string_punto_venta.'</p>
									<p>Respaldo: '.$string_respaldo.'</p>
									<p>Vegano: '.$vegano[0]->name.'</p>
									<a href="#">Visita Sitio Web</a>
								</div>
							</div>
						</div>';

		array_push($array_posts, $html_post);

	endwhile;

	// Reset Query
	wp_reset_query();

	$response_ajax = array('query_object'=>$the_query, 'posts'=>$array_posts, 'var_busqueda'=>$var_busqueda, 'total_posts'=>$total_posts);

	echo json_encode($response_ajax);

	wp_die();

}

/*AJAX BUSCAR MARCAS*/

add_action('wp_enqueue_scripts', 'ajax_buscar');
add_action('wp_ajax_nopriv_ajax_buscar','ajax_buscar_contenido');
add_action('wp_ajax_ajax_buscar','ajax_buscar_contenido');

function ajax_buscar() {

	wp_register_script('ajax_buscar_script', get_template_directory_uri(). '/js/buscar-marcas.js', array('jquery'), '1', true );
	wp_enqueue_script('ajax_buscar_script');
	wp_localize_script('ajax_buscar_script','ajax_wp',['ajaxurl'=>admin_url('admin-ajax.php')]);

};

function ajax_buscar_contenido(){
	
	$var_busqueda = $_POST['var_busqueda'];
	$tax_query_array = array();

	$pagina_actual = $_POST['pagina_actual'];
	$cantidad_post_mostrados = $_POST['cantidad_post_mostrados'];

	
	if(count($var_busqueda) > 0){

		$tax_query_array = array(
			'taxonomy' => 'post_tag',
			'field' => 'slug',
			'terms' => $var_busqueda,
		);

	}

	$arg_marcas_ajax = array(
		'posts_per_page' => $cantidad_post_mostrados,
		'paged' => $pagina_actual,
		'post_type' => array('marcas'),
		'order' => 'DESC', 
		'orderby' => 'date',
		'tax_query' => array(
			'relation' => 'AND',
			$tax_query_array
		)
	);

	$html_post = array(); //se usara para guardar momentaneamente los posts recien llamados
	$array_posts = array(); //array final de posts, guardara el html de cada post para devolver en el ajax a js
	$response_ajax = array(); //contendra los posts y la query utilizada.

	//esta consulta es solo para saber cuantos post trae en total
		$arg_marcas_ajax['posts_per_page'] = -1;
		
		// The Query
		$the_query = new WP_Query( $arg_marcas_ajax );

		$total_posts = count($the_query->posts);

		// Reset Query
		wp_reset_query();

		$arg_marcas_ajax['posts_per_page'] = $cantidad_post_mostrados; //vuelta al orignal de los post mostrados
	//fin consulta para traer post totales//

	// The Query
	$the_query = new WP_Query( $arg_marcas_ajax );

	// The Loop
	while ( $the_query->have_posts() ) : $the_query->the_post();

		global $post; //esto logra que dentro del loop exista el objeto global del post

		$id_post = $post->ID;
		$titulo = get_the_title();
		$post_type = $post->post_type;
		$img_post = get_the_post_thumbnail_url( null, 'img_front_blog' );
		$categorias_post = wp_get_post_terms( $id_post, 'segmento' );
		$string_categorias = "";
		$puntos_venta_post = wp_get_post_terms( $id_post, 'punto_venta' );
		$string_punto_venta = "";
		$respaldos_post = wp_get_post_terms( $id_post, 'respaldo' );
		$string_respaldo = "";
		$vegano = wp_get_post_terms( $id_post, 'vegano' );

		//Arma array de categorias (segmentos)
		$count = count($categorias_post); 
		foreach ($categorias_post as $categoria_post) {
			
			$string_categorias .= $categoria_post->name;

			if($count != 1){

				$string_categorias .= ', ';
			
			}

			$count--;
		}

		//Arma array de puntos de ventas
		$count = count($puntos_venta_post); 
		foreach ($puntos_venta_post as $punto_venta_post) {
			
			$string_punto_venta .= $punto_venta_post->name;

			if($count != 1){

				$string_punto_venta .= ', ';
			
			}

			$count--;
		}

		//Arma array de respaldos
		$count = count($respaldos_post); 
		foreach ($respaldos_post as $respaldo_post) {
			
			$string_respaldo .= $respaldo_post->name;
			
			if($count != 1){

				$string_respaldo .= ', ';

			}

			$count--;
		}
		

		$html_post = ' 	<div class="col-lg-3 col-md-6 posts_ajax">
							<div class="c-search__articles-item"> 
								<img src="'.$img_post.'">
								<div class="c-search__articles-item-text"> 
									<h3>'.$titulo.'</h3>
									<p>Categorías: '.$string_categorias.'</p> 
									<p>Punto de Venta: '.$string_punto_venta.'</p>
									<p>Respaldo: '.$string_respaldo.'</p>
									<p>Vegano: '.$vegano[0]->name.'</p>
									<a href="#">Visita Sitio Web</a>
								</div>
							</div>
						</div>';

		array_push($array_posts, $html_post);

	endwhile;

	// Reset Query
	wp_reset_query();

	$response_ajax = array('query_object'=>$the_query, 'posts'=>$array_posts, 'var_busqueda'=>$var_busqueda, 'total_posts'=>$total_posts);

	echo json_encode($response_ajax);

	wp_die();
}


/*AJAX PAGINADOR*/

add_action('wp_enqueue_scripts', 'ajax_paginador');
add_action('wp_ajax_nopriv_ajax_paginador','ajax_paginador_contenido');
add_action('wp_ajax_ajax_paginador','ajax_paginador_contenido');

function ajax_paginador() {

	wp_register_script('ajax_paginador_script', get_template_directory_uri(). '/js/data-paginador.js', array('jquery'), '1', true );
	wp_enqueue_script('ajax_paginador_script');
	wp_localize_script('ajax_paginador_script','ajax_wp',['ajaxurl'=>admin_url('admin-ajax.php')]);

};

function ajax_paginador_contenido(){

	$var_busqueda = $_POST['var_busqueda'];
	$pagina_actual = $_POST['pagina_actual'];
	$query_busqueda = $_POST['query_busqueda'];
	$cantidad_post_mostrados =  $_POST['cantidad_post_mostrados'];
	
	$query_busqueda['paged'] = $pagina_actual;

	//esta consulta es solo para saber cuantos post trae en total
		$query_busqueda['posts_per_page'] = -1;
		
		// The Query
		$the_query = new WP_Query( $query_busqueda );

		$total_posts = count($the_query->posts);

		// Reset Query
		wp_reset_query();

		$query_busqueda['posts_per_page'] = $cantidad_post_mostrados; //se reasigna el valor original
	//fin consulta para traer post totales//

	$html_post = array(); //se usara para guardar momentaneamente los posts recien llamados
	$array_posts = array(); //array final de posts, guardara el html de cada post para devolver en el ajax a js
	$response_ajax = array(); //contendra los posts y la query utilizada.

	// The Query
	$the_query = new WP_Query( $query_busqueda );

	// The Loop
	while ( $the_query->have_posts() ) : $the_query->the_post();

		global $post; //esto logra que dentro del loop exista el objeto global del post

		$id_post = $post->ID;
		$titulo = get_the_title();
		$post_type = $post->post_type;
		$img_post = get_the_post_thumbnail_url( null, 'img_front_blog' );
		$categorias_post = wp_get_post_terms( $id_post, 'segmento' );
		$string_categorias = "";
		$puntos_venta_post = wp_get_post_terms( $id_post, 'punto_venta' );
		$string_punto_venta = "";
		$respaldos_post = wp_get_post_terms( $id_post, 'respaldo' );
		$string_respaldo = "";
		$vegano = wp_get_post_terms( $id_post, 'vegano' );

		//Arma array de categorias (segmentos)
		$count = count($categorias_post); 
		foreach ($categorias_post as $categoria_post) {
			
			$string_categorias .= $categoria_post->name;

			if($count != 1){

				$string_categorias .= ', ';
			
			}

			$count--;
		}

		//Arma array de puntos de ventas
		$count = count($puntos_venta_post); 
		foreach ($puntos_venta_post as $punto_venta_post) {
			
			$string_punto_venta .= $punto_venta_post->name;

			if($count != 1){

				$string_punto_venta .= ', ';
			
			}

			$count--;
		}

		//Arma array de respaldos
		$count = count($respaldos_post); 
		foreach ($respaldos_post as $respaldo_post) {
			
			$string_respaldo .= $respaldo_post->name;
			
			if($count != 1){

				$string_respaldo .= ', ';

			}

			$count--;
		}
		

		$html_post = ' 	<div class="col-lg-3 col-md-6 posts_ajax">
							<div class="c-search__articles-item"> 
								<img src="'.$img_post.'">
								<div class="c-search__articles-item-text"> 
									<h3>'.$titulo.'</h3>
									<p>Categorías: '.$string_categorias.'</p> 
									<p>Punto de Venta: '.$string_punto_venta.'</p>
									<p>Respaldo: '.$string_respaldo.'</p>
									<p>Vegano: '.$vegano[0]->name.'</p>
									<a href="#">Visita Sitio Web</a>
								</div>
							</div>
						</div>';

		array_push($array_posts, $html_post);

	endwhile;

	// Reset Query
	wp_reset_query();

	$response_ajax = array('query_object'=>$the_query, 'posts'=>$array_posts, 'var_busqueda'=>$var_busqueda, 'total_posts'=>$total_posts);

	echo json_encode($response_ajax);

	wp_die();
}

