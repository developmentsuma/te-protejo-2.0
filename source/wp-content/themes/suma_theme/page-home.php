<?php

/*Template Name: Home*/ 

get_header(); 

$count = 0;
$blog_id = get_current_blog_id();
$site_name = get_blog_details($blog_id)->blogname;


?>

    <div class="main" role="main">
        <div class="c-main">
            <div class="carousel slide" id="carouselExampleSlidesOnly" data-ride="carousel">
                <div class="carousel-inner">

                <?php

                // check if the repeater field has rows of data
                if( have_rows('slider') ):

                    // loop through the rows of data
                    while ( have_rows('slider') ) : the_row();

                    ?>
                        
                        <div class="carousel-item <?php if($count == 0){ echo 'active'; } ?>" style="background:#ffffff url(<?php the_sub_field('imagen_slide'); ?>) center top no-repeat;">
                            <p><?php the_sub_field('texto_cursiva'); ?></p>
                            <h2><?php the_sub_field('texto_principal'); ?></h2>
                            <a href="<?php the_sub_field('link_boton'); ?>"><?php the_sub_field('texto_boton'); ?></a>
                        </div>

                    <?php

                    $count++;

                    endwhile;

                else :

                    // no rows found

                endif;

                ?>

                </div>
                <ol class="carousel-indicators-new carousel-indicators">

                    <?php

                    $count = 0; //reset count

                    // check if the repeater field has rows of data
                    if( have_rows('slider') ):

                        // loop through the rows of data
                        while ( have_rows('slider') ) : the_row();

                        ?>
                            
                            <li class="<?php if($count == 0){ echo 'active'; } ?>" data-target="#carouselExampleSlidesOnly" data-slide-to="<?php echo $count; ?>"><?php echo $count+1; ?></li>

                        <?php

                        $count++;

                        endwhile;

                    else :

                        // no rows found

                    endif;

                    ?>
                    
                </ol>
            </div>
        </div>
        <div class="c-fijos">
            <div class="row">
                <div class="col-md-4 col-brand" style="background:<?php the_field('banner_1_color_relleno'); ?> url(<?php the_field('banner_1_imagen'); ?>) right bottom no-repeat">
                    <div class="c-fijos__item">
                        <h3><?php the_field('banner_1_texto'); ?></h3>
                        <a class="btn btn-primary btn-black" href="<?php the_field('banner_1_link_boton'); ?>"><?php the_field('banner_1_texto_boton'); ?></a>
                    </div>
                </div>
                <div class="col-md-4 col-tips" style="background:<?php the_field('banner_2_color_relleno'); ?> url(<?php the_field('banner_2_imagen'); ?>) center top no-repeat">
                    <div class="c-fijos__item">
                        <h3><?php the_field('banner_2_texto'); ?></h3>
                        <a class="btn btn-primary btn-green" href="<?php the_field('banner_2_link_boton'); ?>"><?php the_field('banner_2_texto_boton'); ?></a>
                    </div>
                </div>
                <div class="col-md-4 col-certification" style="background:<?php the_field('banner_3_color_relleno'); ?> url(<?php the_field('banner_3_imagen'); ?>) left top no-repeat">
                    <div class="c-fijos__item">
                        <h3><?php the_field('banner_3_texto'); ?></h3>
                        <a class="btn btn-primary btn-purple" href="<?php the_field('banner_3_link_boton'); ?>"><?php the_field('banner_3_texto_boton'); ?></a>
                    </div>
                </div>
            </div>
        </div>
        <div class="container">
            <div class="row c-blog__articles">
                <div class="col-md-12">
                    <h2>

                        <?php 
                        
                            if(get_field('lo_ultimo_del_blog', 'option') != ""){ 
                            
                                the_field('lo_ultimo_del_blog', 'option');
                                
                            
                            }else{

                                $field_object = get_field_object('lo_ultimo_del_blog', 'option');
                                echo $field_object['label'];
                                
                            } 
                            
                        ?>

                    </h2>
                    <a class="btn-all" href="<?php echo get_home_url(); ?>/blog-cruelty-free">
                        <?php 
                        
                            if(get_field('ver_todo', 'option') != ""){ 
                            
                                the_field('ver_todo', 'option');
                            
                            }else{

                                $field_object = get_field_object('ver_todo', 'option');
                                echo $field_object['label'];

                            } 
                        ?>
                    </a>
                </div>

                <?php

                $arg_blog = array(
                    'posts_per_page' => 4,
                    'post_type' => array('noticias', 'articulos', 'reviews', 'datos', 'tips', 'actividades'),
                    'order' => 'DESC', 
                    'orderby' => 'date',
                );

                // The Query
                query_posts( $arg_blog );
                
                // The Loop
                while ( have_posts() ) : the_post();

                    //pr($post);
                    $post_type = $post->post_type;
                    $post_type_object = get_post_type_object( $post_type );
                    $img_post = get_the_post_thumbnail_url( null, 'img_front_blog' );

                ?>

                    <div class="col-lg-3 col-md-6">
                        <div class="c-blog__articles-item"> 
                            <a class="picture-container" href="<?php the_permalink(); ?>">
                                <img src="<?php echo $img_post; ?>">
                            </a>
                            <div class="c-blog__articles-item-text">
                                <span>

                                    <?php 
                        
                                        if(get_field($post_type_object->name, 'option') != ""){ 
                                        
                                            the_field($post_type_object->name, 'option');
                                        
                                        }else{

                                            $field_object = get_field_object($post_type_object->name, 'option');
                                            echo $field_object['label'];
                                            
                                        } 
                                        
                                    ?>

                                </span>
                                <h3><?php the_title(); ?></h3>
                                <div class="c-blog__articles-item-text-description">
                                    <p><?php echo get_excerpt(80); ?></p>
                                </div>
                                <a href="<?php the_permalink(); ?>">
                                    Ver 
                                    <?php 
                                        
                                        if(get_field($post_type_object->labels->singular_name, 'option') != ""){ 
                                        
                                            the_field($post_type_object->labels->singular_name, 'option');
                                        
                                        }else{

                                            $field_object = get_field_object($post_type_object->labels->singular_name, 'option');
                                            echo $field_object['label'];
                                            
                                        } 
                                        
                                    ?>
                                </a>
                            </div>
                        </div>
                    </div>

                <?php
                
                endwhile;
                
                // Reset Query
                wp_reset_query();

                ?>
                
            </div>
            <div class="row c-blog__youtube --home">
                <div class="col-md-12">
                    <h2>

                        <?php 
                        
                            if(get_field('lo_ultimo_de_youtube', 'option') != ""){ 
                            
                                the_field('lo_ultimo_de_youtube', 'option');
                            
                            }else{

                                $field_object = get_field_object('lo_ultimo_de_youtube', 'option');
                                echo $field_object['label'];
                                
                            } 
                            
                        ?>

                    </h2>
                    <a class="btn-all" href="https://www.youtube.com/channel/UCJRZXwBBOAPuzPJkQoMrBMA">
                        <?php 
                            
                            if(get_field('ver_todo', 'option') != ""){ 
                            
                                the_field('ver_todo', 'option');
                            
                            }else{

                                $field_object = get_field_object('ver_todo', 'option');
                                echo $field_object['label'];

                            } 
                        ?>
                    </a>
                </div>
                <div class="col-md-6">
                    <iframe width="560" height="400" src="https://www.youtube.com/embed/<?php the_field('video_1'); ?>" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen=""></iframe>
                </div>
                <div class="col-md-6">
                    <iframe width="560" height="400" src="https://www.youtube.com/embed/<?php the_field('video_2'); ?>" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen=""></iframe>
                </div>
            </div>
        </div>
        <div class="c-home__extra">
            <div class="container">
                <div class="row">
                    <div class="col-xl-7">
                        <div class="row c-blog__related">

                            <?php
                            if( get_field('post_destacado')){
                                $posts = get_field('post_destacado');
                            }else{
                                $posts;
                            }
                            

                            foreach( $posts as $post): // variable must be called $post (IMPORTANT) *Debe ir en foreach aunque venga 1 post*
                            
                                setup_postdata($post);

                                $post_type = $post->post_type;
                                $post_type_object = get_post_type_object( $post_type );
                                $img_post = get_the_post_thumbnail_url( null, 'img_destacada_blog' );
                                
                                ?>
                                
                                <div class="col-md-12">
                                    <h2>
                                        <?php 
                            
                                            if(get_field('destacamos', 'option') != ""){ 
                                            
                                                the_field('destacamos', 'option');
                                            
                                            }else{

                                                $field_object = get_field_object('destacamos', 'option');
                                                echo $field_object['label'];

                                            } 
                                        ?>
                                    </h2>
                                    <a class="btn-all" href="<?php echo get_home_url(); ?>/archive-posts/?post_type=<?php echo $post_type_object->label; ?>">
                                        <?php 
                                
                                            if(get_field('ver_todo', 'option') != ""){ 
                                            
                                                the_field('ver_todo', 'option');
                                            
                                            }else{

                                                $field_object = get_field_object('ver_todo', 'option');
                                                echo $field_object['label'];

                                            } 
                                        ?>
                                    </a>
                                </div>
                                <div class="col-md-12">
                                    <div class="c-blog__related-item">
                                        <div class="row">
                                            <div class="col-md-6">
                                                <a href="<?php the_permalink(); ?>">
                                                    <img src="<?php echo $img_post; ?>">
                                                </a>
                                            </div>
                                            <div class="col-md-6"> 
                                                <span>
                                                    <?php 
                            
                                                        if(get_field($post_type_object->name, 'option') != ""){ 
                                                        
                                                            the_field($post_type_object->name, 'option');
                                                        
                                                        }else{

                                                            $field_object = get_field_object($post_type_object->name, 'option');
                                                            echo $field_object['label'];
                                                            
                                                        } 
                                                        
                                                    ?>
                                                </span>
                                                <h3><?php the_title(); ?></h3>
                                                <div class="c-blog__articles-item-text-description">
                                                    <p><?php echo get_excerpt(80); ?></p>
                                                </div>
                                                <a href="<?php the_permalink(); ?>">Ver <?php echo tradgb($post_type_object->labels->singular_name,$post_type_object->labels->singular_name); ?> </a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            
                                <?php 
                                
                            endforeach;
                                 
                            wp_reset_postdata(); // IMPORTANT - reset the $post object so the rest of the page works correctly 
                            
                            ?>

                        </div>
                    </div>
                    <div class="col-xl-5">
                        <div class="c-team">
                            <h2>
                            <?php 
                                
                                if(get_field('unete_al_equipo', 'option') != ""){ 
                                
                                    the_field('unete_al_equipo', 'option');
                                
                                }else{

                                    $field_object = get_field_object('unete_al_equipo', 'option');
                                    echo $field_object['label'];

                                } 
                            ?>
                            </h2>

                            <?php

                            $count = 0; //reset count
                            $nombres_seleccion_usuarios = array(); //contendra nombres de users seleccionados
                            $usuarios_seleccionados = get_field('seleccion_equipo');
                            $match_nombres_equipo = array(); //contendra el match entre users seleccionados y los miembros de equipos
                            $nombres_usuarios = array();
                            $match_nombres_equipo2 = array();
                           //pr($usuarios_seleccionados);
                           /* foreach ($usuarios_seleccionados as $miembro) {
                                
                                array_push($nombres_seleccion_usuarios, $miembro['display_name']);
                            
                            } */
                            foreach ($usuarios_seleccionados as $miembro) {
                                $nombre_limpio = quitar_tildes($miembro['display_name']);
                                array_push($nombres_seleccion_usuarios, $nombre_limpio);
                            
                            }
                           // pr($nombres_seleccion_usuarios);
                            $arg_equipos = array(
                                'posts_per_page' => -1,
                                'post_type' => array('equipos'),
                                'order' => 'DESC', 
                                'orderby' => 'order',
                            );

                            // The Query
                            query_posts( $arg_equipos );

                            //pr($arg_equipos);
                            
                            // The Loop
                            while ( have_posts() ) : the_post();

                                // check if the repeater field has rows of data
                                if( have_rows('equipo') ):

                                        // loop through the rows of data
                                    while ( have_rows('equipo') ) : the_row();

                                        $count++; //cuenta cada integrante
                                        $nombre_miembro = get_sub_field('nombre');
                                        $nombre_limpio = quitar_tildes($nombre_miembro);
                                        array_push($nombres_usuarios, $nombre_miembro);
                                        //echo $nombre_miembro ;
                                        $fotografia_miembro = get_sub_field('fotografia')['sizes']['img_pequenia_equipo'];
                                      
                                        foreach ($nombres_seleccion_usuarios as $sKey)
                                        {
                                            if( stripos( strtolower($sKey) , strtolower($nombre_miembro) ) !== false )
                                            {
                                                array_push($match_nombres_equipo2, array('fotografia'=>$fotografia_miembro, 'nombre'=>$nombre_miembro));
                                            }
                                        }
                                        
                                        
                                   
                                        
                                        if (in_array($nombre_limpio, $nombres_seleccion_usuarios)) {
                                            
                                            array_push($match_nombres_equipo, array('fotografia'=>$fotografia_miembro, 'nombre'=>$nombre_miembro));
                                        }
                                      
                                    endwhile;

                                else :

                                // no rows found

                                endif;
                            
                            endwhile;
                            
                            // Reset Query
                            wp_reset_query();
                            //pr($match_nombres_equipo);
                           // pr($nombres_usuarios);
                           //pr($match_nombres_equipo2);
                            ?>
                           
                            <a href="<?php echo get_home_url(); ?>/ong-te-protejo/">(<?php echo $count; ?>+)</a>
                            <ul>

                                <?php

                                foreach ($match_nombres_equipo as $match_miembro) {
                                    
                                ?>

                                    <li>
                                        <img src="<?php echo $match_miembro['fotografia']; ?>" alt="<?php echo $match_miembro['nombre']; ?>">
                                    </li>

                                <?php

                                }

                                ?>

                            </ul>
                        </div>
                        <div class="c-newsletter__home"> 
                            <h4>
                                <?php 
                                    
                                    if(get_field('suscribete_al_newsletter', 'option') != ""){ 
                                    
                                        the_field('suscribete_al_newsletter', 'option');
                                    
                                    }else{

                                        $field_object = get_field_object('suscribete_al_newsletter', 'option');
                                        echo $field_object['label'];

                                    } 
                                ?>
                            </h4>
                            <form>
                                <div class="row">
                                    <div class="col-md-12">
                                        <p>
                                            <?php 
                                        
                                                if(get_field('quieres_recibir_nuestras_noticias', 'option') != ""){ 
                                                
                                                    the_field('quieres_recibir_nuestras_noticias', 'option');
                                                
                                                }else{

                                                    $field_object = get_field_object('quieres_recibir_nuestras_noticias', 'option');
                                                    echo $field_object['label'];

                                                } 
                                            ?>
                                        </p>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="c-newsletter__inputs">
                                            <input id="email_newsletter" type="text" placeholder="<?php 
                                        
                                                if(get_field('escribe_tu_email', 'option') != ""){ 
                                                
                                                    the_field('escribe_tu_email', 'option');
                                                
                                                }else{

                                                    $field_object = get_field_object('escribe_tu_email', 'option');
                                                    echo $field_object['label'];

                                                } 
                                            ?>">
                                            <input type="hidden" id="blog_origen_newsletter" value="<?php echo $site_name; ?>">
                                            <button id="newsletter">
                                            <?php  tradgb('suscribete','Suscríbete');?>
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    
<?php 

get_footer(); 

?>