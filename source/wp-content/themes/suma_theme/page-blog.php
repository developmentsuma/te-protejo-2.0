<?php

/*Template Name: Blog Cruelty-Free*/ 
/*V-2*/
get_header('nuevo'); 

$count = 0;
$blog_id = get_current_blog_id();
$site_name = get_blog_details($blog_id)->blogname;


?>
    <div class="main" role="main">
        <div class="c-blog">
            <div class="c-blog__bg" style="background-image:url('<?php the_field('imagen_header_izquierda'); ?>');">
                <div class="c-blog__bg2" style="background-image:url('<?php the_field('imagen_header_derecha'); ?>');">
                </div>
            </div>
            <div class="container">
                <div class="row">
                    <div class="col-md-1">
                    </div>
                    <div class="col-md-10">
                        <div class="c-blog__top">
                            <div class="row">
                                <div class="col-12">
                                    <h1><?php  tradgb('td_blog_vive_cruelty','Blog Vive Cruelty Free');?></h1>
                                    <h2><?php  tradgb('td_sub_titulo_blog','Noticias, tips, reviews, actividades y mucho más.');?></h2>
                                </div>
                            </div>
                            <div class="c-blog__search">
                                <input id="buscar_posts" type="text" placeholder="<?php  tradgb('buscas_algo_en_especial','¿Buscas algo en especial?');?>">
                                <button id="btn_buscar_posts"> <i class="fas fa-search"></i></button>
                            </div>
                        </div>
                        <div class="c-blog__important">
                            <div class="carousel slide" id="carouselExampleSlidesOnly" data-ride="carousel">
                                <div class="carousel-inner">

                                    <?php 

                                    $posts = get_field('slider_de_posts');
                                    $total_slides = count($posts);
                                    $id_posts_slider = array();

                                    if( $posts ):
                                        
                                        foreach( $posts as $post): // variable must be called $post (IMPORTANT) 
                                        
                                            setup_postdata($post); 

                                            $post_type = $post->post_type;
                                            $post_type_object = get_post_type_object( $post_type );
                                            $img_post = get_the_post_thumbnail_url();
                                            /* $img_post = get_the_post_thumbnail_url( null, 'img_slider_posts' ); */                                            
                                            $id_post = $post->ID;

                                            ?>

                                            <div class="carousel-item <?php if($count == 0){ echo 'active'; } ?>">
                                                <div class="row">
                                                    <div class="col-lg-6">
                                                        <a href="<?php the_permalink(); ?>">
                                                            <img src="<?php echo $img_post; ?>">
                                                        </a>
                                                    </div>
                                                    <div class="col-lg-6">
                                                        <div class="c-blog__important-txt">
                                                            <span><?php tradgb($post_type_object->labels->singular_name,$post_type_object->labels->singular_name); ?></span>
                                                            <h3><?php the_title(); ?></h3>
                                                            <p><?php echo get_excerpt(80); ?></p>
                                                            <a href="<?php the_permalink(); ?>"><?php tradgb('ver', 'Ver'); echo ' '; tradgb($post_type_object->labels->singular_name,$post_type_object->labels->singular_name);?></a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                            <?php 

                                            $count++;
                                            array_push($id_posts_slider, $id_post); //se agregan ids de posts ya mostrados

                                        endforeach;
                                        
                                        wp_reset_postdata(); // IMPORTANT - reset the $post object so the rest of the page works correctly 
                                        
                                    endif; 

                                    ?>
                            </div>
                            <ol class="carousel-indicators-new ol carousel-indicators">
                            
                            <?php

                            for ($i=0; $i < $total_slides; $i++) { 

                                ?>

                                <li class="<?php if($i == 0){ echo 'active'; } ?>" data-target="#carouselExampleSlidesOnly" data-slide-to="<?php echo $i; ?>"><?php echo $i+1; ?></li>

                                <?php

                            }

                            ?>

                            </ol>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row c-blog__articles">
                <div class="col-md-12">
                    <h2><?php  tradgb('lo_ultimo_del_blog','Lo último del blog');?></h2>
                </div>

                <?php

                $arg_ultimos_blog = array(
                    'posts_per_page' => 4,
                    'post_type' => array('noticias', 'articulos', 'reviews', 'datos', 'tips', 'actividades'),
                    'post__not_in' => $id_posts_slider,
                    'order' => 'DESC', 
                    'orderby' => 'date',
                );

                // The Query
                query_posts( $arg_ultimos_blog );

                // The Loop
                while ( have_posts() ) : the_post();

                    $post_type = $post->post_type;
                    $post_type_object = get_post_type_object( $post_type );
                    /* $img_post = get_the_post_thumbnail_url( null, 'img_front_blog' ); */
                    $img_post = get_the_post_thumbnail_url();
                    $id_post = $post->ID;

                ?>

                    <div class="col-lg-3 col-md-6">
                        <div class="c-blog__articles-item">
                            <a class="picture-container" href="<?php the_permalink(); ?>">
                                <img src="<?php echo $img_post; ?>">
                            </a>
                            <div class="c-blog__articles-item-text">
                                <span><?php  tradgb($post_type_object->name ,$post_type_object->name );?></span><!-- traducir -->
                                <h3><?php the_title(); ?></h3>
                                <div class="c-blog__articles-item-text-description">
                                    <p><?php echo get_excerpt(80); ?></p>
                                </div>
                                <a href="<?php the_permalink(); ?>"><?php  tradgb('ver' ,'Ver');?> <?php echo ' ' . tradgb($post_type_object->labels->singular_name ,$post_type_object->labels->singular_name );?></a>
                            </div>
                        </div>
                    </div>

                    <?php

                    array_push($id_posts_slider, $id_post); //se agregan más ids de posts ya mostrados

                endwhile;

                // Reset Query
                wp_reset_query();

                ?>
            
            </div>
            <div class="row c-blog__youtube">
                <div class="col-md-12">
                    <h2><?php  tradgb('lo_ultimo_de_youtube','Lo último de youtube');?></h2>
                    <a class="btn-all" href="https://www.youtube.com/channel/UCJRZXwBBOAPuzPJkQoMrBMA"><?php  tradgb('ver_todo','Ver todo');?></a>
                </div>
                <div class="col-md-6">
                    <iframe width="560" height="315" src="https://www.youtube.com/embed/<?php the_field('video_1'); ?>" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen=""></iframe>
                </div>
                <div class="col-md-6">
                    <iframe width="560" height="315" src="https://www.youtube.com/embed/<?php the_field('video_2'); ?>" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen=""></iframe>
                </div>
            </div>
            <div class="row c-blog__related">
                <div class="col-md-12">
                    <h2><?php  tradgb('noticias','Noticias');?></h2>
                    <a class="btn-all" href="<?php echo get_home_url(); ?>/archive-posts/?post_type=Noticias"><?php  tradgb('ver_todas','Ver todas');?></a>
                </div>

                <?php

                $arg_ultimos_noticias = array(
                    'posts_per_page' => 2,
                    'post_type' => array('noticias'),
                    //'post__not_in' => $id_posts_slider,
                    'order' => 'DESC',
                    'orderby' => 'date',
                );

                // The Query
                query_posts( $arg_ultimos_noticias );

                // The Loop
                while ( have_posts() ) : the_post();

                    $post_type = $post->post_type;
                    $post_type_object = get_post_type_object( $post_type );
                    /* $img_post = get_the_post_thumbnail_url( null, 'img_front_blog' ); */
                    $img_post = get_the_post_thumbnail_url();

                ?>

                    <div class="col-xl-6">
                        <div class="c-blog__related-item">
                            <div class="row">
                                <div class="col-xl-6 col-lg-5 col-md-5">
                                    <a href="<?php the_permalink(); ?>">
                                        <img src="<?php echo $img_post; ?>">
                                    </a>
                                </div>
                                <div class="col-md-6"> 
                                    <span><?php  tradgb($post_type_object->name,$post_type_object->name);?></span>
                                    <h3><?php the_title(); ?></h3>
                                    <div class="c-blog__articles-item-text-description">
                                        <p><?php echo get_excerpt(80); ?></p>
                                    </div>
                                    <a href="<?php the_permalink(); ?>"><?php  tradgb('ver','Ver');?> <?php echo ' ' . tradgb($post_type_object->labels->singular_name,$post_type_object->labels->singular_name);?></a>
                                </div>
                            </div>
                        </div>
                    </div>

                <?php

                endwhile;

                // Reset Query
                wp_reset_query();

                ?>
                
            </div>
            <div class="row c-blog__articles">
                
                <div class="col-lg-6"> 
                    <div class="row">
                        <div class="col-md-12">
                            <div class="c-blog__articles-top-b">
                                <h2><?php  tradgb('reviews','Reviews');?></h2>
                                <a class="btn-all" href="<?php echo get_home_url(); ?>/archive-posts/?post_type=Reviews"><?php  tradgb('ver_todo','Ver todo');?></a>
                            </div>
                        </div>

                        <?php

                        $arg_ultimos_reviews = array(
                            'posts_per_page' => 2,
                            'post_type' => array('reviews'),
                            //'post__not_in' => $id_posts_slider,
                            'order' => 'DESC',
                            'orderby' => 'date',
                        );

                        // The Query
                        query_posts( $arg_ultimos_reviews );

                        // The Loop
                        while ( have_posts() ) : the_post();

                            $post_type = $post->post_type;
                            $post_type_object = get_post_type_object( $post_type );
                            /* $img_post = get_the_post_thumbnail_url( null, 'img_front_blog' ); */
                            $img_post = get_the_post_thumbnail_url();

                        ?>

                            <div class="col-md-6">
                                <div class="c-blog__articles-item">
                                    <a class="picture-container" href="<?php the_permalink(); ?>">
                                        <img src="<?php echo $img_post; ?>">
                                    </a>
                                    <div class="c-blog__articles-item-text">
                                        <span><?php  tradgb($post_type_object->name,$post_type_object->name);?></span>
                                        <h3><?php the_title(); ?></h3>
                                        <div class="c-blog__articles-item-text-description">
                                            <p><?php echo get_excerpt(80); ?></p>
                                        </div>
                                        <a href="<?php the_permalink(); ?>"><?php  tradgb('ver','Ver');?> <?php echo ' ' . tradgb($post_type_object->labels->singular_name,$post_type_object->labels->singular_name);?></a>
                                    </div>
                                </div>
                            </div>

                        <?php

                        endwhile;

                        // Reset Query
                        wp_reset_query();

                        ?>
                        
                    </div>
                </div>
                
                <div class="col-lg-6"> 
                    <div class="row">
                        <div class="col-md-12">
                            <div class="c-blog__articles-top-b">
                                <h2><?php  tradgb('datos','Datos');?></h2>
                                <a class="btn-all" href="<?php echo get_home_url(); ?>/archive-posts/?post_type=Datos"><?php  tradgb('ver_todo','Ver todo');?></a>
                            </div>
                        </div>

                        <?php

                        $arg_ultimos_datos = array(
                            'posts_per_page' => 2,
                            'post_type' => array('datos'),
                            //'post__not_in' => $id_posts_slider,
                            'order' => 'DESC',
                            'orderby' => 'date',
                        );

                        // The Query
                        query_posts( $arg_ultimos_datos );

                        // The Loop
                        while ( have_posts() ) : the_post();

                            $post_type = $post->post_type;
                            $post_type_object = get_post_type_object( $post_type );
                            /* $img_post = get_the_post_thumbnail_url( null, 'img_front_blog' ); */
                            $img_post = get_the_post_thumbnail_url();

                        ?>

                            <div class="col-md-6">
                                <div class="c-blog__articles-item">
                                    <a class="picture-container" href="<?php the_permalink(); ?>">
                                        <img src="<?php echo $img_post; ?>">
                                    </a>
                                    <div class="c-blog__articles-item-text">
                                        <span><?php  tradgb($post_type_object->name,$post_type_object->name);?></span>
                                        <h3><?php the_title(); ?></h3>
                                        <div class="c-blog__articles-item-text-description">
                                            <p><?php echo get_excerpt(80); ?></p>
                                        </div>
                                        <a href="<?php the_permalink(); ?>"><?php  tradgb('ver','Ver');?> <?php echo ' ' . tradgb($post_type_object->labels->singular_name,$post_type_object->labels->singular_name);?></a>
                                    </div>
                                </div>
                            </div>

                        <?php

                        endwhile;

                        // Reset Query
                        wp_reset_query();

                        ?>
                        
                    </div>
                </div>

                <div class="col-lg-6"> 
                    <div class="row">
                        <div class="col-md-12">
                            <div class="c-blog__articles-top-b">
                                <h2><?php  tradgb('tips','Tips');?></h2>
                                <a class="btn-all" href="<?php echo get_home_url(); ?>/archive-posts/?post_type=Tips"><?php  tradgb('ver_todo','Ver todo');?></a>
                            </div>
                        </div>

                        <?php

                        $arg_ultimos_tips = array(
                            'posts_per_page' => 2,
                            'post_type' => array('tips'),
                            //'post__not_in' => $id_posts_slider,
                            'order' => 'DESC',
                            'orderby' => 'date',
                        );

                        // The Query
                        query_posts( $arg_ultimos_tips );

                        // The Loop
                        while ( have_posts() ) : the_post();

                            $post_type = $post->post_type;
                            $post_type_object = get_post_type_object( $post_type );
                            /* $img_post = get_the_post_thumbnail_url( null, 'img_front_blog' ); */
                            $img_post = get_the_post_thumbnail_url();

                        ?>

                            <div class="col-md-6">
                                <div class="c-blog__articles-item">
                                    <a class="picture-container" href="<?php the_permalink(); ?>"><!-- aaaaaaaaaaaaaaaaa-->
                                        <img src="<?php echo $img_post; ?>">
                                    </a>
                                    <div class="c-blog__articles-item-text">
                                        <span><?php  tradgb($post_type_object->name,$post_type_object->name);?></span>
                                        <h3><?php the_title(); ?></h3>
                                        <div class="c-blog__articles-item-text-description">
                                            <p><?php echo get_excerpt(80); ?></p>
                                        </div>
                                        <a href="<?php the_permalink(); ?>"><?php  tradgb('ver','Ver');?> <?php echo ' ' . tradgb($post_type_object->labels->singular_name,$post_type_object->labels->singular_name);?></a>
                                    </div>
                                </div>
                            </div>

                        <?php

                        endwhile;

                        // Reset Query
                        wp_reset_query();

                        ?>
                    
                    </div>
                </div>

                <div class="col-lg-6"> 
                    <div class="row">
                        <div class="col-md-12">
                            <div class="c-blog__articles-top-b">
                                <h2><?php  tradgb('actividades','Actividades');?></h2>
                                <a class="btn-all" href="<?php echo get_home_url(); ?>/archive-posts/?post_type=Actividades"><?php  tradgb('ver_todo','Ver todo');?></a>
                            </div>
                        </div>

                        <?php

                        $arg_ultimos_datos = array(
                            'posts_per_page' => 2,
                            'post_type' => array('actividades'),
                            //'post__not_in' => $id_posts_slider,
                            'order' => 'DESC',
                            'orderby' => 'date',
                        );

                        // The Query
                        query_posts( $arg_ultimos_datos );

                        // The Loop
                        while ( have_posts() ) : the_post();

                            $post_type = $post->post_type;
                            $post_type_object = get_post_type_object( $post_type );
                            /* $img_post = get_the_post_thumbnail_url( null, 'img_front_blog' ); */
                            $img_post = get_the_post_thumbnail_url();

                        ?>

                            <div class="col-md-6">
                                <div class="c-blog__articles-item">
                                    <a class="picture-container" href="<?php the_permalink(); ?>">
                                        <img src="<?php echo $img_post; ?>">
                                    </a>
                                    <div class="c-blog__articles-item-text">
                                        <span><?php  tradgb($post_type_object->name,$post_type_object->name);?></span>
                                        <h3><?php the_title(); ?></h3>
                                        <div class="c-blog__articles-item-text-description">
                                            <p><?php echo get_excerpt(80); ?></p>
                                        </div>
                                        <a href="<?php the_permalink(); ?>"><?php  tradgb('ver','Ver');?> <?php echo ' ' . tradgb($post_type_object->labels->singular_name,$post_type_object->labels->singular_name);?></a>
                                    </div>
                                </div>
                            </div>

                        <?php

                        endwhile;

                        // Reset Query
                        wp_reset_query();

                        ?>
                        
                    </div>
                </div>
            </div>
        </div>
        <div class="c-newsletter">
          <div class="container">
            <h4><?php  tradgb('suscribete_al_newsletter','Suscríbete al newsletter');?></h4>
            <form>
              <div class="row">
                <div class="col-md-6">
                  <p><?php  tradgb('quieres_recibir_nuestras_noticias','¿Quieres recibir nuestras noticias?');?></p>
                </div>
                <div class="col-md-6">
                    <div class="c-newsletter__inputs">
                        <input id="email_newsletter" type="text" placeholder="<?php  tradgb('escribe_tu_email','Escribe tu email');?>">
                        <input type="hidden" id="blog_origen_newsletter" value="<?php echo $site_name; ?>">
                        <button id="newsletter"><?php  tradgb('suscribete','Suscríbete');?></button>
                    </div>
                </div>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
    <!--- version 2 --->
<?php 

get_footer(); 

?>