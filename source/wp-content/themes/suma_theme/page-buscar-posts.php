<?php

/*Template Name: Buscar Posts*/ 

get_header('nuevo'); 

$frase_busqueda = $_GET['busqueda'];
$blog_id = get_current_blog_id();
$site_name = get_blog_details($blog_id)->blogname;


?>

    <div class="main" role="main">
        <div class="c-blog">
            <div class="c-blog__bg c-blog__bg--eco">
                <div class="c-blog__bg2 c-blog__bg--eco2"></div>
            </div>
            <div class="container">
                <div class="row justify-content-md-center">
                    <div class="col-xl-10">
                        <div class="c-blog__top">
                            <div class="row">
                                <div class="col-12">

                                    <h1><?php  tradgb('busqueda','Búsqueda');?>: <?php echo $frase_busqueda; ?></h1>

                                </div>
                                <div class="c-blog__search">
                                    <input id="buscar_posts" type="text" placeholder="<?php  tradgb('vuelve_a_buscar','Vuelve a buscar');?>...">
                                    <button id="btn_buscar_posts"> <i class="fas fa-search"></i></button>
                                </div>
                                <div class="col-6">
                                    <a href="<?php echo get_home_url(); ?>/blog-cruelty-free/"> <i class="fas fa-arrow-left"></i><?php  tradgb('volver_al_blog','Volver al blog');?></a>
                                </div>
                            </div>
                        </div>
                        <!--
                        <div class="c-blog__important">
                            <div class="row">
                                <div class="col-lg-6">
                                    <img src="<?php bloginfo( 'template_url' ); ?>/img/cnt-important.jpg">
                                </div>
                                <div class="col-lg-6">
                                    <div class="c-blog__important-txt">
                                        <span><?php  tradgb('feria_eco_belleza','Feria eco belleza');?></span>
                                        <h3><?php  tradgb('td_no_te_pierdas_feria','¡No te pierdas Feria Ecobelleza Concepción!');?></h3>
                                        <p><?php  tradgb('td_te_protejo_comienza_una_nueva','Te Protejo comienza una nueva campaña para llevar la vida cruelty free a toda Latinoamérica a través de Enlightaid.');?></p><a href="#"><?php tradgb('ver', 'Ver');?> <?php tradgb('articulo', 'artículo');?></a>
                                    </div>
                                </div>
                            </div>
                        </div>-->  
                        <div class="c-blog__important">
                            <div class="carousel slide" id="carouselExampleSlidesOnly" data-ride="carousel">
                                <div class="carousel-inner">

                                    <?php 

                                    $posts = get_field('slider_de_posts');
                                    $total_slides = count($posts);
                                    $id_posts_slider = array();

                                    if( $posts ):
                                        
                                        foreach( $posts as $post): // variable must be called $post (IMPORTANT) 
                                        
                                            setup_postdata($post); 

                                            $post_type = $post->post_type;
                                            $post_type_object = get_post_type_object( $post_type );
                                            $img_post = get_the_post_thumbnail_url( null, 'img_slider_posts' );
                                            $id_post = $post->ID;

                                            ?>

                                            <div class="carousel-item <?php if($count == 0){ echo 'active'; } ?>">
                                                <div class="row">
                                                    <div class="col-lg-6">
                                                        <a href="<?php the_permalink(); ?>">
                                                            <img src="<?php echo $img_post; ?>">
                                                        </a>
                                                    </div>
                                                    <div class="col-lg-6">
                                                        <div class="c-blog__important-txt">
                                                            <span><?php tradgb($post_type_object->labels->singular_name,$post_type_object->labels->singular_name); ?></span>
                                                            <h3><?php the_title(); ?></h3>
                                                            <p><?php echo get_excerpt(80); ?></p>
                                                            <a href="<?php the_permalink(); ?>"><?php tradgb('ver', 'Ver'); echo ' '; tradgb($post_type_object->labels->singular_name,$post_type_object->labels->singular_name);?></a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                            <?php 

                                            $count++;
                                            array_push($id_posts_slider, $id_post); //se agregan ids de posts ya mostrados

                                        endforeach;
                                        
                                        wp_reset_postdata(); // IMPORTANT - reset the $post object so the rest of the page works correctly 
                                        
                                    endif; 

                                    ?>
                            </div>
                            <ol class="carousel-indicators-new ol carousel-indicators">
                            
                            <?php

                            for ($i=0; $i < $total_slides; $i++) { 

                                ?>

                                <li class="<?php if($i == 0){ echo 'active'; } ?>" data-target="#carouselExampleSlidesOnly" data-slide-to="<?php echo $i; ?>"><?php echo $i+1; ?></li>

                                <?php

                            }

                            ?>

                            </ol>
                        </div>
                    </div>
                </div>
                <div class="row c-blog__articles" id="posts_originales">
                    <div id="articulos" class="col-md-12">
                        <h2><?php  tradgb('todos_los_post_que_contienen','Todos los post que contienen');?> "<?php echo $frase_busqueda; ?>" </h2>
                    </div>

                    <?php

                    $paged = 1;
                    

                    $arg_busqueda = array(
                        'posts_per_page' => 8,
                        'post_type' => array('post', 'noticias', 'articulos', 'reviews', 'datos', 'tips', 'actividades'),
                        's' => $frase_busqueda,
                        'order' => 'DESC', 
                        'orderby' => 'relevance',
                        'paged' => $paged,
                    );
                
                    // The Query
                    $the_query = new WP_Query( $arg_busqueda );

                    // The Loop
                    while ( $the_query->have_posts() ) : $the_query->the_post();

                        $frase_busqueda = $post->post_type;
                        $frase_busqueda_object = get_post_type_object( $frase_busqueda );
                        $img_post = get_the_post_thumbnail_url( null, 'img_front_blog' );

                    ?>

                        <div class="col-lg-3 col-md-6">
                            <div class="c-blog__articles-item"> 
                                <a class="picture-container" href="<?php the_permalink(); ?>">
                                    <img src="<?php echo $img_post; ?>">
                                </a>
                                <div class="c-blog__articles-item-text">
                                    <span><?php  tradgb($frase_busqueda_object->name,$frase_busqueda_object->name);?></span>
                                    <h3><?php the_title(); ?></h3>
                                    <div class="c-blog__articles-item-text-description">
                                        <p><?php echo get_excerpt(80); ?></p>
                                    </div>
                                    <a href="<?php the_permalink(); ?>"><?php tradgb('ver', 'Ver'); echo ' '; tradgb($frase_busqueda_object->labels->singular_name,$frase_busqueda_object->labels->singular_name);?></a>
                                </div>
                            </div>
                        </div>

                    <?php

                    endwhile;

                    ?>

                        <script>

                            var pagina_actual = <?php echo json_encode($paged); ?>;
                            var query_inicial = <?php echo json_encode($arg_busqueda); ?>;

                        </script>

                        <?php

                    ?>

                    <!-- <div class="col-md-12">
                        
                        <?php kriesi_pagination($the_query->max_num_pages); ?>

                    </div>
                     -->
                    <?php

                    // Reset Query
                    wp_reset_query();

                    ?>
                    
                </div>
                <div class="row c-blog__articles" id="response_message">
                </div>
            </div>
            <div id="hook-scroll" class="c-newsletter">
                <div class="container">
                    <h4><?php  tradgb('suscribete_al_newsletter','Suscríbete al newsletter');?></h4>
                    <form>
                        <div class="row">
                            <div class="col-md-6">
                                <p><?php  tradgb('quieres_recibir_nuestras_noticias','¿Quieres recibir nuestras noticias?');?></p>
                            </div>
                            <div class="col-md-6">
                                <div class="c-newsletter__inputs">
                                    <input id="email_newsletter" type="text" placeholder="<?php  tradgb('escribe_tu_email','Escribe tu email');?>">
                                    <input type="hidden" id="blog_origen_newsletter" value="<?php echo $site_name; ?>">
                                    <button id="newsletter"><?php  tradgb('suscribete','Suscríbete');?></button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    
<?php 

get_footer(); 

?>