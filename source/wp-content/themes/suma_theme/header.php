<?php
/**
 * The Header for our theme.
 *
 * Displays all of the <head> section and everything up till <div id="main">
 *
 * @package WordPress
 * @subpackage SUMA
 */
?>

<!DOCTYPE html>
<html lang="es">
    <head>
        <!-- Global site tag (gtag.js) - Google Analytics -->
        <script async src="https://www.googletagmanager.com/gtag/js?id=UA-126564978-1"></script>
        <script>
        window.dataLayer = window.dataLayer || [];
        function gtag(){dataLayer.push(arguments);}
        gtag('js', new Date());

        gtag('config', 'UA-126564978-1');
        </script>
        <meta charset="<?php bloginfo( 'charset' ); ?>" />
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <meta name="template-url" content="<?php bloginfo( 'template_url' ); ?>">
        <meta name="home-url" content="<?php bloginfo('url'); ?>">
        <style>
        li.page-item a.page-link{
            border-radius: 20px !important;
            margin: 0 3px;
            height: 40px;
            width: 40px;
            text-align: center;
            color: #898989;
        }

        li.page-item.active a.page-link{
            z-index: 1;
            color: #fff;
            background-color: #282828;
            border-color: #898989;
        }

        li.page-item.disable a.page-link {
            background-color: #a0a0a0;
            border-color: #282828;
        }

        a.page-link:hover {
            color: #fff !important;
            text-decoration: none;
            background-color: #282828;
        }

        a.page-link {
            color: #898989;
        }

        a.page-link:focus {
            z-index: 2;
            outline: 0;
            box-shadow: 0 0 0 0.2rem rgb(188, 187, 180);
        }
        </style>

        <title>
        <?php
            /*
            * Print the <title> tag based on what is being viewed.
            */
            global $page, $paged;

            wp_title( '|', true, 'right' );

            // Add the blog name.
            bloginfo( 'name' );

            // Add the blog description for the home/front page.
            $site_description = get_bloginfo( 'description', 'display' );
            if ( $site_description && ( is_home() || is_front_page() ) )
                echo " | $site_description";

            // Add a page number if necessary:
            if ( $paged >= 2 || $page >= 2 )
                echo ' | ' . sprintf( __( 'Page %s', 'twentyten' ), max( $paged, $page ) );

        ?>
        </title>
            
        <link rel="profile" href="http://gmpg.org/xfn/11" />
        <link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>" />
        
        <?php
        
            /* We add some JavaScript to pages with the comment form
            * to support sites with threaded comments (when in use).
            */
            if ( is_singular() && get_option( 'thread_comments' ) )
                wp_enqueue_script( 'comment-reply' );

            /* Always have wp_head() just before the closing </head>
            * tag of your theme, or you will break many plugins, which
            * generally use this hook to add elements to <head> such
            * as styles, scripts, and meta tags.
            */
            wp_head();

        ?>

        <link href="https://fonts.googleapis.com/css?family=Signika:300,400,600,700" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Caveat" rel="stylesheet">
        <link href="<?php bloginfo( 'template_url' ); ?>/css/fontawesome.css" rel="stylesheet">
        <link href="<?php bloginfo( 'template_url' ); ?>/css/bootstrap.css" rel="stylesheet">
        <link href="https://rawgit.com/utatti/perfect-scrollbar/master/css/perfect-scrollbar.css" rel="stylesheet">
        <link href="<?php bloginfo( 'template_url' ); ?>/css/main.css?v=2.8" rel="stylesheet">
        <link href="<?php bloginfo( 'template_url' ); ?>/css/jquery-ui.css" rel="stylesheet">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/selectize.js/0.12.6/css/selectize.bootstrap3.min.css">
        <!-- <link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css"> -->

        <!--OG de facebook share-->

        <meta property="og:url" content="<?php the_permalink(); ?>" />
        <meta property="og:title" content="<?php the_title(); ?>" />
        <meta property="og:description" content="<?php echo get_excerpt(80); ?>" />
        <meta property="og:image" content="<?php echo get_the_post_thumbnail_url(get_the_ID(),'img_front_blog'); ?>" />
        <meta property="og:type" content="article" />

        <!-- Load Facebook SDK for JavaScript -->
        <div id="fb-root"></div>
        <script>(function(d, s, id) {
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id)) return;
        js = d.createElement(s); js.id = id;
        js.src = 'https://connect.facebook.net/es_ES/sdk.js#xfbml=1&version=v3.0';
        fjs.parentNode.insertBefore(js, fjs);
        }(document, 'script', 'facebook-jssdk'));</script>

    </head>

    <body class=" "> 
    <div class="c-header">
        <div class="c-header__bg"> </div>
        <h1>Te protejo</h1>
        <nav class="navbar navbar-expand-lg navbar-light">
            <a class="navbar-brand" href="<?php echo get_home_url(); ?>">
                <img src="<?php bloginfo( 'template_url' ); ?>/img/cnt-logo.png">
            </a>
            <button class="hamburger navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
                <span class="hamburger-box">
                    <span class="hamburger-inner"></span>
                </span>
            </button>
            <div class="collapse navbar-collapse" id="navbarNav">
                <ul class="navbar-nav ml-md-auto">
                    <li class="nav-item <?php if (is_page('ong-te-protejo')) echo 'active'; ?>">
                        <a class="nav-link" href="<?php echo get_home_url(); ?>/ong-te-protejo"><?php tradgb('td_menu_org_te_protejo', 'ONG Te <br> Protejo');?><span class="sr-only">(current)</span>
                        </a>
                    </li>
                    <li class="nav-item <?php if (is_page('marcas-cruelty-free')) echo 'active'; ?>">
                        <a class="nav-link" href="<?php echo get_home_url(); ?>/marcas-cruelty-free"><?php tradgb('td_menu_marcas_cruelty-free', 'Marcas <br> Cruelty-free');?></a>
                    </li>
                    <li class="nav-item <?php if (is_page('blog-cruelty-free','archive-posts','buscar-posts')) echo 'active'; ?>">
                        <a class="nav-link" href="<?php echo get_home_url(); ?>/blog-cruelty-free"><?php tradgb('td_menu_blog_cruelty-free', 'Blog <br> Cruelty-free');?></a>
                    </li>

                    <?php
                    //Menú Feria Eco Belleza prensente solo en Chile
                    if (get_current_blog_id() == 1) {
                        ?>
                        <li class="nav-item <?php if (is_page('feria-ecobelleza')) echo 'active'; ?>">
                            <a class="nav-link" href="<?php echo get_home_url(); ?>/feria-ecobelleza"><?php tradgb('td_menu_feria_ecobelleza', 'Feria <br> Ecobelleza');?></a>
                        </li>

                        <!-- <li class="nav-item <?php if (is_page('blog-cruelty-free','archive-posts','buscar-posts')) echo 'active'; ?>">
                            <a class="nav-link" href="https://www.tiendaecobelleza.cl/">Tienda <br> Ecobelleza</a>
                         </li> -->
                        <?php
                    }

                    ?>
                    

                    <li class="nav-item <?php if (is_page('contacto')) echo 'active'; ?>">
                        <a class="nav-link" href="<?php echo get_home_url(); ?>/contacto"><?php tradgb('td_menu_contacto', 'Contacto ');?></a>
                    </li>

                    <li class="nav-item nav-social">
                        <a href="https://www.instagram.com/teprotejo/"><i class="fab fa-instagram"></i></a>
                        <a href="https://www.facebook.com/TeProtejo/"><i class="fab fa-facebook"></i></a>
                        <a href="https://twitter.com/TeProtejo"><i class="fab fa-twitter"></i></a>
                        <a href="https://www.youtube.com/channel/UCJRZXwBBOAPuzPJkQoMrBMA"><i class="fab fa-youtube"></i></a>
                        <div class="c-dropdown">
                            <!--<a class="nav-link" href="#"><?php //echo get_blog_details(get_current_blog_id())->blogname; ?><?php  tradgb('td_selecciona_tu_pais','Selecciona tu país');?>-->
                            <a class="nav-link" href="#"><?php echo get_blog_details(get_current_blog_id())->blogname; ?> </a>
                                <div class="fas fa-caret-down"></div>
                                <div class="c-dropdown__body">
                                    <!-- <div style="display:none">
                                        <?php echo traductor('ver todo', get_blog_details()->blogname); ?>
                                    </div>
                                    <a  href="#"><?php echo get_blog_details(get_current_blog_id())->blogname; ?> </a>
                                    <?php
                                        $subsites = get_sites();  
                                        foreach ($subsites as $subsite) {
                                            $subsite_id = get_object_vars($subsite)["blog_id"];
                                            if (get_current_blog_id() != $subsite_id) {
                                                ?> <a href="<?php echo get_blog_details($subsite_id)->home; ?>"><?php echo get_blog_details($subsite_id)->blogname; ?></a> <?php
                                            }
                                        }
                                    ?> -->
                                    <a href="#">Chile</a>
                                    <a href="https://ongteprotejo.org/br">Brasil</a>  
                                    <a href="https://ongteprotejo.org/mx">México</a>  
                                    <a href="https://ongteprotejo.org/co">Colombia</a>  
                                    <a href="https://ongteprotejo.org/ar">Argentina</a>  
                                    <a href="https://ongteprotejo.org/pe">Perú</a>       
                                </div>
                            </a>
                        </div>
                    </li>
                </ul>
            </div>
        </nav>
    </div>