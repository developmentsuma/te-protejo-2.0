<?php

get_header('nuevo'); 

?>

    <div class="main" role="main">
        <div class="c-blog">
            <div class="c-blog__bg">
                <div class="c-blog__bg2">
                </div>
            </div>
            <div class="container">
                <div class="row justify-content-md-center"> 
                    <div class="col-xl-10">
                        <div class="c-blog__top">
                            <div class="row">
                                <div class="col-6">
                                    <a href=""><i class="fas fa-arrow-left"></i>Volver al blog</a>
                                </div>
                                <div class="col-6 text-right"> 
                                    <a href="">Siguiente post<i class="fas fa-arrow-right"></i></a>
                                </div>
                            </div>
                        </div>
                        <div class="c-blog__single">
                            <div class="c-blog__single-info">
                                <a href="#"><strong>Noticias  </strong></a>
                                <span>|</span><small>Publicado el 26/02/2018</small>
                            </div>
                            <h1>TE PROTEJO SE UNE A ENLIGHTAID PARA LOGRAR UNA AMÉRICA LATINA CRUELTY FREE</h1>
                            <img src="img/cnt-single.jpg">
                            <div class="c-blog__tag"> 
                                <i class="fas fa-tag"> </i><a href="#">Noticias</a>, 
                                <a href="#">curelty-free</a>, <a href="#">alianzas</a>
                            </div>
                            <div class="c-blog__resume">
                                <p>Te Protejo comienza una nueva campaña para llevar la vida cruelty free a toda Latinoamérica a través de Enlightaid.</p>
                            </div>
                            <div class="c-blog__main">
                                <p>Te Protejo ha experimentado la creciente necesidad de Latinoamérica por preferir productos no testeados en animales, muchas personas de distintos países se contactan día a día para recibir la ayuda que los chilenos obtienen a través de nuestra ONG, debido a que somos la única organización en Latinoamérica que realiza esta labor.</p>
                                <p>Son millones los consumidores conscientes en Latinoamérica pero no todos están informados y tienen las herramientas que Te Protejo ha logrado instaurar en Chile. Si llegáramos a toda Latinoamérica ¿Cuántos animales salvaríamos? ¿Cómo cambiaría su situación ante la experimentación si el mercado comienza a preferir lo cruelty free?</p>
                                <p>Sabemos que los consumidores de América Latina quieren ser responsables con los animales y el medio ambiente, sin embargo 19 países de los 20 que la componen no cuentan con la plataforma para lograrlo. Nuestra primera experiencia de expansión fue con la filial de Perú desde el 2013, alianza que ha sido exitosa y un excelente precedente para nuestros objetivos, no obstante aún no cuenta con las herramientas de difusión que tenemos en Chile.Por lo anterior, hace unos meses Te Protejo creó a ONG Te Protejo Latam y expandir el bienestar animal dejó de ser solo un sueño. Como han visto estos últimos meses, también nos hemos asociado con activistas de distintos países que realizan post informativos y reseñas en el blog Vive Cruelty Free, estos son México, Brasil y Colombia.</p>
                            </div>
                        </div>
                        <div class="c-blog__share">
                            <p>comparte:</p>
                            <a class="btn-fb" href="#"><i class="fab fa-facebook"></i>Compartir</a>
                            <a class="btn-copy" href="#"><i class="fas fa-copy"></i>Copiar URL</a>
                        </div>
                    </div>
                </div>
                <div class="row c-blog__related">
                    <div class="col-md-12">
                        <h2>Noticias</h2>
                        <a class="btn-all" href="#">Ver todas</a>
                    </div>
                    <div class="col-xl-6">
                        <div class="c-blog__related-item">
                            <div class="row">
                                <div class="col-xl-6 col-lg-5 col-md-5">
                                    <img src="img/cnt-related.jpg">
                                </div>
                                <div class="col-md-6"> 
                                    <span>Noticias</span>
                                    <h3>TE PROTEJO SE UNE A ENLIGHTAID PARA LOGRAR UNA AMÉRICA LATINA CRUELTY FREE</h3>
                                    <p>Te Protejo comienza una nueva campaña para llevar la vida cruelty free a toda Latinoamérica a través de Enlightaid.</p>
                                    <a href="#">Ver artículo</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-6">
                        <div class="c-blog__related-item">
                            <div class="row">
                                <div class="col-xl-6 col-lg-5 col-md-5">
                                    <img src="img/cnt-related.jpg">
                                </div>
                                <div class="col-md-6"> 
                                    <span>Noticias</span>
                                    <h3>TE PROTEJO SE UNE A ENLIGHTAID PARA LOGRAR UNA AMÉRICA LATINA CRUELTY FREE</h3>
                                    <p>Te Protejo comienza una nueva campaña para llevar la vida cruelty free a toda Latinoamérica a través de Enlightaid.</p>
                                    <a href="#">Ver artículo</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="c-newsletter">
                <div class="container">
                    <h4>Suscríbete al newsletter</h4>
                    <form>
                        <div class="row">
                            <div class="col-md-6">
                                <p>¿Quieres recibir nuestras noticias?</p>
                            </div>
                            <div class="col-md-6">
                                <div class="c-newsletter__inputs">
                                    <input id="email_newsletter" placeholder="Escribe tu email" type="text">
                                    <input type="hidden" id="blog_origen_newsletter" value="<?php echo $site_name; ?>">
                                    <button id="newsletter">Suscríbete</button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

		
<?php 

get_footer(); 

?>
